package id.co.telkomsigma.app.simulator;

import static org.apache.commons.lang.StringEscapeUtils.escapeHtml;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.HttpClientErrorException;

import id.co.telkomsigma.app.constant.WebGuiConstant;
import id.co.telkomsigma.app.controller.GenericController;
import id.co.telkomsigma.app.controller.RESTClient;
import id.co.telkomsigma.app.model.web.security.AuditLog;
import id.co.telkomsigma.app.model.web.user.Role;
import id.co.telkomsigma.app.model.web.user.User;
import id.co.telkomsigma.app.pojo.AckCnfReqPojo;
import id.co.telkomsigma.app.pojo.DataTablesPojo;
import id.co.telkomsigma.app.pojo.HeaderReqPojo;
import id.co.telkomsigma.app.pojo.PayordReqPojo;
import id.co.telkomsigma.app.service.AckCnfService;
import id.co.telkomsigma.app.service.AuditLogService;
import id.co.telkomsigma.app.service.MenuService;
import id.co.telkomsigma.app.service.PayOrdService;
import id.co.telkomsigma.app.service.RoleService;
import id.co.telkomsigma.app.service.UserService;
import id.co.telkomsigma.app.util.FormValidationUtil;
import id.co.telkomsigma.app.util.JsonUtils;

@Controller
public class AckCnfController extends GenericController { 

	//controller untuk injector manual ackcnf
	@Autowired
	private AckCnfService ackcnfService ;
	
	@Autowired
	private RESTClient restClient ;

	private static final String SEND_PAGE_NAME = "simulator/ackcnf/send";
      
	  @RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_ACKCNF_MAPPING, method = RequestMethod.GET)
	  public String showSendPage(final Model model,
					final HttpServletRequest request, final HttpServletResponse response) {
		
		
		  AckCnfReqPojo ackcnf = new AckCnfReqPojo();
		  ackcnf.setValDate(new SimpleDateFormat("yyyyMMdd").format(new Date())); 
		  ackcnf.setExtRef(ackcnfService.generateExtRef());
		  
		model.addAttribute("ackcnf",ackcnf);
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		
		String date = new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date());
     	
     	AuditLog auditLogs = new AuditLog();
     	auditLogs.setActivityType(auditLogs.MANUAL_INJECTOR);
     	auditLogs.setCreatedBy(user.getUsername());
     	auditLogs.setCreatedDate(new Date());
     	auditLogs.setDescription("Buka Halaman Manual Injector ACKCNF");
     	auditLogs.setReffNo(date);
        
        try{
        	auditLogService.insertAuditLog(auditLogs);
        }
        catch(Exception ex){
        	ex.printStackTrace();
        }
        log.debug("Added Audit Log");
     	
    	return getPageContent(SEND_PAGE_NAME);
    }
	
	
	  @RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_ACKCNF_MAPPING, method = RequestMethod.POST)
	    public String doSend(@ModelAttribute("ackcnf") AckCnfReqPojo ackcnf, 
	    		BindingResult errors, final Model model, 
	    		final HttpServletRequest request, final HttpServletResponse response) {	  
	
		try {
			
			 System.out.println("doSend Message ackcnf ackcnf exref " + ackcnf.getExtRef() );
			 System.out.println("ackcnf : " + ackcnf.toString());
			 
			 FormValidationUtil.validatingRequired(errors, "extRef", ackcnf.getExtRef(), getMessage("label.simulator.ackcnf.extref"));
			 //relRef optional
			 FormValidationUtil.validatingRequired(errors, "benAc", ackcnf.getBenAC(), getMessage("label.simulator.ackcnf.benac"));
			 FormValidationUtil.validatingRequired(errors, "memID", ackcnf.getMemID(), getMessage("label.simulator.ackcnf.memid"));
			 FormValidationUtil.validatingRequired(errors, "memAC", ackcnf.getMemAC(), getMessage("label.simulator.ackcnf.memac"));
			 FormValidationUtil.validatingRequired(errors, "curCod", ackcnf.getCurCod(), getMessage("label.simulator.ackcnf.curcod"));
			 FormValidationUtil.validatingRequired(errors, "valDate", ackcnf.getValDate(), getMessage("label.simulator.ackcnf.valdate"));
			 FormValidationUtil.validatingRequired(errors, "cashVal", ackcnf.getCashVal(), getMessage("label.simulator.ackcnf.cashval"));
			 FormValidationUtil.validatingRequired(errors, "status", ackcnf.getStatus(), getMessage("label.simulator.ackcnf.status"));
			 //Reason Conditional 
			 //Notes Optional
			 Boolean responseRest = false;
			
			if (!errors.hasErrors()) {
				Authentication auth = SecurityContextHolder.getContext().getAuthentication();
				User user = (User) auth.getPrincipal();

				String date = new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date());

				AuditLog auditLogs = new AuditLog();
				auditLogs.setActivityType(auditLogs.MANUAL_INJECTOR);
				auditLogs.setDescription("Kirim ACKCNF Injector");
				auditLogs.setCreatedBy(user.getUsername());
				auditLogs.setCreatedDate(new Date());
				auditLogs.setReffNo(date);
				
				//set headernya 
				HeaderReqPojo header = new HeaderReqPojo();
				header.setName("ACK-CNF");
				header.setType("OutgoingMessage");
				header.setStructure("ACKPBRelatedMovement");
				ackcnf.setHeader(header);
				
				responseRest = restClient.sendAckCnf(ackcnf);
				
				if (responseRest)
					model.addAttribute("okMessage", getMessage("form.ok.simulator.send.ackcnf" , ackcnf.getExtRef()));
				else 
					model.addAttribute("errorMessage", getMessage("form.error.simulator.send.ackcnf" , ackcnf.getExtRef()));
				
				

				auditLogService.insertAuditLog(auditLogs);
				log.debug("Added Audit Log");
				
			}
		} catch (Exception ex) {
			log.error(ex);
			model.addAttribute("errorMessage", getMessage("form.error.simulator.send.ackcnf" , ackcnf.getExtRef()));
		}

		return getPageContent(SEND_PAGE_NAME);
	    }
	

}
