package id.co.telkomsigma.app.simulator;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.HttpClientErrorException;

import id.co.telkomsigma.app.constant.WebGuiConstant;
import id.co.telkomsigma.app.controller.GenericController;
import id.co.telkomsigma.app.controller.RESTClient;
import id.co.telkomsigma.app.model.web.parameter.SystemParameter;
import id.co.telkomsigma.app.model.web.security.AuditLog;
import id.co.telkomsigma.app.model.web.user.User;
import id.co.telkomsigma.app.pojo.CnfReqPojo;
import id.co.telkomsigma.app.pojo.PayordReqPojo;
import id.co.telkomsigma.app.service.CnfService;
import id.co.telkomsigma.app.service.KseiWebService;
import id.co.telkomsigma.app.service.PayOrdService;
import id.co.telkomsigma.app.util.FieldValidationUtil;
import id.co.telkomsigma.app.util.FormValidationUtil;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

@Controller
public class CnfController  extends GenericController { 

	@Autowired
	private CnfService cnfService ;
	
	@Autowired
	private RESTClient restClient ;

	private static final String SEND_PAGE_NAME = "simulator/cnf/send";
      
	  @RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_CNF_MAPPING, method = RequestMethod.GET)
	  public String showSendPage(final Model model,
					final HttpServletRequest request, final HttpServletResponse response) {
		
		  
		  CnfReqPojo cnf = new CnfReqPojo();
		  cnf.setValDate(new SimpleDateFormat("yyyyMMdd").format(new Date())); 
		  cnf.setExtRef(cnfService.generateExtRef());
			  
		  System.out.println("cnf exref  " + cnf.getExtRef());
		  
		model.addAttribute("cnf",cnf);
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		
		String date = new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date());
     	
     	AuditLog auditLogs = new AuditLog();
     	auditLogs.setActivityType(auditLogs.MANUAL_INJECTOR);
     	auditLogs.setCreatedBy(user.getUsername());
     	auditLogs.setCreatedDate(new Date());
     	auditLogs.setDescription("Buka Halaman Manual Injector CNF");
     	auditLogs.setReffNo(date);
        
        try{
        	auditLogService.insertAuditLog(auditLogs);
        }
        catch(Exception ex){
        	ex.printStackTrace();
        }
        log.debug("Added Audit Log");
     	
    	return getPageContent(SEND_PAGE_NAME);
    }
	
	
	  @RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_CNF_MAPPING, method = RequestMethod.POST)
	    public String doSend(@ModelAttribute("cnf") CnfReqPojo cnf, 
	    		BindingResult errors, final Model model, 
	    		final HttpServletRequest request, final HttpServletResponse response) {	  
	
		try {
			
			System.out.println("==== CNF POJO TO STRING: " + cnf.toString() );
			 
			FormValidationUtil.validatingRequired(errors, "extref", cnf.getExtRef(), getMessage("label.simulator.cnf.extref"));
			//relref optional
			FormValidationUtil.validatingRequired(errors, "benac", cnf.getBenAC(), getMessage("label.simulator.cnf.benac"));
			FormValidationUtil.validatingRequired(errors, "memid", cnf.getMemID(), getMessage("label.simulator.cnf.memid"));
			FormValidationUtil.validatingRequired(errors, "memac", cnf.getMemAC(), getMessage("label.simulator.cnf.memac"));
			FormValidationUtil.validatingRequired(errors, "valdate", cnf.getValDate(), getMessage("label.simulator.cnf.valdate"));
			FormValidationUtil.validatingRequired(errors, "curcod", cnf.getCurCod(), getMessage("label.simulator.cnf.curcod"));
			FormValidationUtil.validatingRequired(errors, "cashval", cnf.getCashVal(), getMessage("label.simulator.cnf.cashval"));
			
			if (!errors.hasErrors()) {
				
				System.out.println("doSend Message CNF ");
				System.out.println("cnf exref " + cnf.getExtRef() );
				
				Authentication auth = SecurityContextHolder.getContext().getAuthentication();
				User user = (User) auth.getPrincipal();

				String date = new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date());

				AuditLog auditLogs = new AuditLog();
				auditLogs.setActivityType(auditLogs.MANUAL_INJECTOR);
				auditLogs.setDescription("Kirim CNF Injector");
				auditLogs.setCreatedBy(user.getUsername());
				auditLogs.setCreatedDate(new Date());
				auditLogs.setReffNo(date);
				Boolean responseRest = false;
				//call service 
		
				auditLogService.insertAuditLog(auditLogs);
				log.debug("Added Audit Log");
				responseRest = restClient.sendCnf(cnf);
				
				if (responseRest)
					model.addAttribute("okMessage", getMessage("form.ok.simulator.send.cnf" , cnf.getExtRef()));
				else 
					model.addAttribute("errorMessage", getMessage("form.error.simulator.send.cnf" , cnf.getExtRef()));
			}
		} catch (Exception ex) {
			log.error(ex);
			model.addAttribute("errorMessage", getMessage("form.error.simulator.send.cnf", cnf.getExtRef()));
		}

		return getPageContent(SEND_PAGE_NAME);
	    }
	
	
	
	
}
