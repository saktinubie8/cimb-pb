package id.co.telkomsigma.app.controller;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.text.SimpleDateFormat;

import javax.annotation.PostConstruct;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import id.co.telkomsigma.app.model.web.parameter.SystemParameter;
import id.co.telkomsigma.app.pojo.AckCnfReqPojo;
import id.co.telkomsigma.app.pojo.CnfReqPojo;
import id.co.telkomsigma.app.pojo.DcnfReqPojo;
import id.co.telkomsigma.app.pojo.DrefReqPojo;
import id.co.telkomsigma.app.pojo.PayordReqPojo;
import id.co.telkomsigma.app.service.ParameterService;
import id.co.telkomsigma.app.util.RestUtil;

import java.util.Base64;

@Component("restClient")
public class RESTClient {

	protected final Log log = LogFactory.getLog(getClass());
	
	@Autowired
	ParameterService parameterService;
	
	public static final String USERNAMEREST = "usernameRest";
	public static final String PASSWORDREST = "passwordRest";
	
	String IP_ADDR = "localhost";
	//String IP_ADDR_DEV = "192.168.1.103";
	
	private HttpHeaders createHeaders(){
	    HttpHeaders headers =  new HttpHeaders(){
	          {
	        	String authBasic = "";
	      		SystemParameter userRest = new SystemParameter();
	      		userRest = parameterService.getParamByParamName(USERNAMEREST);
	      		
	      		SystemParameter passRest = new SystemParameter();
	      		passRest = parameterService.getParamByParamName(PASSWORDREST);
	      		
	      		authBasic = Base64.getEncoder().encodeToString((userRest.getParamValue()+":"+passRest.getParamValue()).getBytes()); 
	            String authHeader = "Basic " + authBasic;
	            set( "Authorization", authHeader );
	          }
	       };
	       headers.setContentType(MediaType.APPLICATION_JSON);
	       return headers;
	}
	
	public Boolean doResend(String messageId) {
		
		 Boolean responseRest = false;
		// String url = "https://localhost:8080/basic/resend";
		 //keperluan test 
		 String url = "https://"+IP_ADDR+":8080/basic/resend";
		
		 RestTemplate currentRestTemplate = RestUtil.getSecureRestTemplate();
		 HttpHeaders headers = new HttpHeaders();
		 headers = this.createHeaders();
		 headers.setContentType(MediaType.APPLICATION_JSON);
		 HttpEntity<String> entity = new HttpEntity<String>(messageId,headers);
		 System.out.println("header: " + headers);
	
		 try {
				log.debug("SEND MESSAGE ID..." + messageId);
				ResponseEntity<String> resp = currentRestTemplate.postForEntity(url, entity, String.class);
				log.debug("SEND MESSAGE ID... SUCCESS...");
				responseRest =  resp.getStatusCode().is2xxSuccessful();
				System.out.println("resp.getStatusCode() : " + resp.getStatusCode());
				System.out.println("response rest : " + resp.getStatusCode().is2xxSuccessful());
				System.out.println(" ==== sukses resend  " + messageId);
			} catch (HttpClientErrorException e) {
				HttpStatus httpStatus = e.getStatusCode();
				log.error("SEND MESSAGE ID... FAILED..., " + httpStatus.toString() + " : " + httpStatus.getReasonPhrase());
				System.out.println(" ==== gagal resend  " + messageId);
				responseRest =  e.getStatusCode().is2xxSuccessful();
			} catch (Exception e) {
				log.error("SEND MESSAGE ID... FAILED..., "+e.getMessage());
				System.out.println(" ==== gagal resend  " + messageId);
			}
		return responseRest;
	}
	
	public Boolean sendDcnf(DcnfReqPojo dcnf){
		 Boolean responseRest = false;
		 RestTemplate currentRestTemplate = RestUtil.getSecureRestTemplate();
		 //String url = "https://localhost:8080/basic/injector/dcnf";

		 String url = "https://"+IP_ADDR+":8080/basic/injector/dcnf";
		 
		 HttpHeaders headers = new HttpHeaders();
		 headers = this.createHeaders();
		 headers.setContentType(MediaType.APPLICATION_JSON);
		 HttpEntity<DcnfReqPojo> entity = new HttpEntity<DcnfReqPojo>(dcnf,headers);
		 System.out.println("header: " + headers);
	
		 try {
				log.debug("SEND DCNF .." + dcnf.getExtRef());
				ResponseEntity<String> resp = currentRestTemplate.postForEntity(url, entity, String.class);
				log.debug("SEND DCNF... SUCCESS..." + dcnf.getExtRef());
				responseRest =  resp.getStatusCode().is2xxSuccessful();
				System.out.println("resp.getStatusCode() : " + resp.getStatusCode());
				System.out.println("response rest : " + resp.getStatusCode().is2xxSuccessful());
				System.out.println(" ==== sukses SEND DCNF  " + dcnf.getExtRef());
			} catch (HttpClientErrorException e) {
				HttpStatus httpStatus = e.getStatusCode();
				log.error("SENDDCNF... FAILED..., " + httpStatus.toString() + " : " + httpStatus.getReasonPhrase());
				System.out.println(" ==== gagal SEND DCNF  " + dcnf.getExtRef());
				responseRest =  e.getStatusCode().is2xxSuccessful();
			} catch (Exception e) {
				log.error("SEND DCNF... FAILED..., "+e.getMessage());
				System.out.println(" ==== gagal SEND DCNF  " + dcnf.getExtRef());
			}
		 return responseRest;

	}
	
	public Boolean sendCnf(CnfReqPojo cnf){
		 Boolean responseRest = false;
		 RestTemplate currentRestTemplate = RestUtil.getSecureRestTemplate();
		 //String url = "https://localhost:8080/basic/injector/cnf";
		 
		 String url = "https://"+IP_ADDR+":8080/basic/injector/cnf";

		 HttpHeaders headers = new HttpHeaders();
		 headers = this.createHeaders();
		 headers.setContentType(MediaType.APPLICATION_JSON);
		 HttpEntity<CnfReqPojo> entity = new HttpEntity<CnfReqPojo>(cnf,headers);
		 System.out.println("header: " + headers);
	
		 try {
				log.debug("SEND CNF .." + cnf.getExtRef());
				ResponseEntity<String> resp = currentRestTemplate.postForEntity(url, entity, String.class);
				log.debug("SEND CNF... SUCCESS..." + cnf.getExtRef());
				
				responseRest =  resp.getStatusCode().is2xxSuccessful();
				System.out.println("resp.getStatusCode() : " + resp.getStatusCode());
				System.out.println("response rest : " + resp.getStatusCode().is2xxSuccessful());
				System.out.println(" ==== sukses SEND CNF  " + cnf.getExtRef());
			} catch (HttpClientErrorException e) {
				HttpStatus httpStatus = e.getStatusCode();
				log.error("SENDDCNF... FAILED..., " + httpStatus.toString() + " : " + httpStatus.getReasonPhrase());
				System.out.println(" ==== gagal SEND CNF  " + cnf.getExtRef());
				responseRest =  e.getStatusCode().is2xxSuccessful();
			} catch (Exception e) {
				log.error("SEND CNF... FAILED..., "+e.getMessage());
				System.out.println(" ==== gagal SEND CNF  " + cnf.getExtRef());
			}
		 
		 return responseRest;

	}
	
	public Boolean sendDref(DrefReqPojo dref){
		 Boolean responseRest = false;
		 RestTemplate currentRestTemplate = RestUtil.getSecureRestTemplate();
	//	 String url = "https://localhost:8080/basic/injector/dref";
		 
		 String url = "https://"+IP_ADDR+":8080/basic/injector/dref";

		 HttpHeaders headers = new HttpHeaders();
		 headers = this.createHeaders();
		 headers.setContentType(MediaType.APPLICATION_JSON);
		 HttpEntity<DrefReqPojo> entity = new HttpEntity<DrefReqPojo>(dref,headers);
		 System.out.println("header: " + headers);
	
		 try {
				log.debug("SEND DREF .." + dref.getExtRef());
				ResponseEntity<String> resp = currentRestTemplate.postForEntity(url, entity, String.class);
				log.debug("SEND DREF... SUCCESS..." + dref.getExtRef());
				responseRest =  resp.getStatusCode().is2xxSuccessful();
				System.out.println("resp.getStatusCode() : " + resp.getStatusCode());
				System.out.println("response rest : " + resp.getStatusCode().is2xxSuccessful());
				System.out.println(" ==== sukses SEND DREF  " + dref.getExtRef());
			} catch (HttpClientErrorException e) {
				HttpStatus httpStatus = e.getStatusCode();
				log.error("SEND DREF... FAILED..., " + httpStatus.toString() + " : " + httpStatus.getReasonPhrase());
				System.out.println(" ==== gagal SEND DREF  " + dref.getExtRef());
				responseRest =  e.getStatusCode().is2xxSuccessful();
			} catch (Exception e) {
				log.error("SEND DREF... FAILED..., "+e.getMessage());
				System.out.println(" ==== gagal SEND DREF  " + dref.getExtRef());
			}
		 
		 return responseRest;
	}
	
	public Boolean sendPayord(PayordReqPojo payord){
		 Boolean responseRest = false;
		 RestTemplate currentRestTemplate = RestUtil.getSecureRestTemplate();
		// String url = "https://localhost:8080/basic/injector/payord";

		 String url = "https://"+IP_ADDR+":8080/basic/injector/payord";
		 
		 HttpHeaders headers = new HttpHeaders();
		 headers = this.createHeaders();
		 headers.setContentType(MediaType.APPLICATION_JSON);
		 HttpEntity<PayordReqPojo> entity = new HttpEntity<PayordReqPojo>(payord,headers);
		 System.out.println("header: " + headers);
	
		 try {
				log.debug("SEND PAYORD .." + payord.getExtRef());
				ResponseEntity<String> resp = currentRestTemplate.postForEntity(url, entity, String.class);
				log.debug("SEND PAYORD... SUCCESS..." + payord.getExtRef());
				System.out.println("resp.getStatusCode() : " + resp.getStatusCode());
				System.out.println("response rest : " + resp.getStatusCode().is2xxSuccessful());
				responseRest =  resp.getStatusCode().is2xxSuccessful();
				System.out.println(" ==== sukses SEND PAYORD  " + payord.getExtRef());
			} catch (HttpClientErrorException e) {
				HttpStatus httpStatus = e.getStatusCode();
				log.error("SEND PAYORD... FAILED..., " + httpStatus.toString() + " : " + httpStatus.getReasonPhrase());
				System.out.println(" ==== gagal SEND PAYORD  " + payord.getExtRef());
				responseRest =  e.getStatusCode().is2xxSuccessful();
			} catch (Exception e) {
				log.error("SEND PAYORD... FAILED..., "+e.getMessage());
				System.out.println(" ==== gagal SEND PAYORD  " + payord.getExtRef());
			}
		 
		 return responseRest;
	}
	
	
	public Boolean sendAckCnf(AckCnfReqPojo ackcnf){
		 System.out.println(" ==== SEND ACKCNF  " + ackcnf.getExtRef());
	     Boolean responseRest = false;
		 RestTemplate currentRestTemplate = RestUtil.getSecureRestTemplate();
		 //String url = "https://localhost:8080/basic/injector/ack-cnf";
		 
		 String url = "https://"+IP_ADDR+":8080/basic/injector/ack-cnf";

		 HttpHeaders headers = new HttpHeaders();
		 headers = this.createHeaders();
		 headers.setContentType(MediaType.APPLICATION_JSON);
		 HttpEntity<AckCnfReqPojo> entity = new HttpEntity<AckCnfReqPojo>(ackcnf,headers);
		 System.out.println("header: " + headers);
	
		 try {
				log.debug("SEND ACKCNF .." + ackcnf.getExtRef());
				ResponseEntity<String> resp = currentRestTemplate.postForEntity(url, entity, String.class);
				log.debug("SEND ACKCNF... SUCCESS..." + ackcnf.getExtRef());
				responseRest =  resp.getStatusCode().is2xxSuccessful();
				System.out.println(" ==== sukses SEND ACKCNF  " + ackcnf.getExtRef());
			} catch (HttpClientErrorException e) {
				HttpStatus httpStatus = e.getStatusCode();
				log.error("SEND ACKCNF... FAILED..., " + httpStatus.toString() + " : " + httpStatus.getReasonPhrase());
				System.out.println(" ==== gagal SEND ACKCNF  " + ackcnf.getExtRef());
				responseRest =  e.getStatusCode().is2xxSuccessful();
			} catch (Exception e) {
				log.error("SEND ACKCNF... FAILED..., "+e.getMessage());
				System.out.println(" ==== gagal SEND ACKCNF  " + ackcnf.getExtRef());
			}
		 
		 return responseRest;
	}
	
	public void clearCache(){
		 RestTemplate currentRestTemplate = RestUtil.getSecureRestTemplate();
		// String url = "https://localhost:8080/basic/monitoring/clear-cache";
		 
		 String url = "https://"+IP_ADDR+":8080/basic/monitoring/clear-cache";

		 HttpHeaders headers = new HttpHeaders();
		 headers = this.createHeaders();
		 headers.setContentType(MediaType.APPLICATION_JSON);
		 HttpEntity<String> entity = new HttpEntity<String>("",headers);
		 System.out.println("header: " + headers);
	
		 try {
				log.debug("CLEAR CACHE .." );
				ResponseEntity<String> resp = currentRestTemplate.postForEntity(url, entity, String.class);
				log.debug("CLEAR CACHE .. SUCCESS..." );
				System.out.println(" CLEAR CACHE .. SUCCESS...");
			} catch (HttpClientErrorException e) {
				HttpStatus httpStatus = e.getStatusCode();
				log.error("CLEAR CACHE .. FAILED..."  + httpStatus.toString() + " : " + httpStatus.getReasonPhrase());
				System.out.println(" ==== FAILED CLEAR CACHE  " );
				//responseRest =  e.getStatusCode().is2xxSuccessful();
			} catch (Exception e) {
				log.error("CLEAR CACHE .. FAILED...  "+e.getMessage());
				System.out.println(" CLEAR CACHE .. FAILED...  " );
			}
		
	}

	public Boolean checkConnITM(){
		
		 Boolean responseRest = false;
		 RestTemplate currentRestTemplate = RestUtil.getSecureRestTemplate();
		// String url = "https://localhost:8080/basic/monitoring/check-socket-itm";
		 
		 String url = "https://"+IP_ADDR+":8080/basic/monitoring/check-socket-itm";

		 HttpHeaders headers = new HttpHeaders();
		 headers = this.createHeaders();
		 headers.setContentType(MediaType.APPLICATION_JSON);
		 HttpEntity<String> entity = new HttpEntity<String>("",headers);
		 System.out.println("header: " + headers);
	
		 try {
				log.debug("SEND CHECK CONNECTION ITM.."  );
				ResponseEntity<String> resp = currentRestTemplate.postForEntity(url, entity, String.class);
				log.debug("SEND CHECK CONNECTION ITM... SUCCESS..." );
				responseRest =  resp.getStatusCode().is2xxSuccessful();
				System.out.println(" SEND CHECK CONNECTION ITM... SUCCESS..." );
			} catch (HttpClientErrorException e) {
				HttpStatus httpStatus = e.getStatusCode();
				log.error("SEND CHECK CONNECTION ITM... FAILED..." + httpStatus.toString() + " : " + httpStatus.getReasonPhrase());
				System.out.println(" SEND CHECK CONNECTION ITM... FAILED..." );
				responseRest =  e.getStatusCode().is2xxSuccessful();
			} catch (Exception e) {
				log.error("SEND CHECK CONNECTION ITM... FAILED... "+e.getMessage());
				System.out.println(" SEND CHECK CONNECTION ITM... FAILED...");
			}
		// responseRest = true;
		 return responseRest;
	}
	
	public Boolean checkConnKSEI(){
		
		 Boolean responseRest = false;
		 RestTemplate currentRestTemplate = RestUtil.getSecureRestTemplate();
		// String url = "https://localhost:8080/basic/monitoring/check-socket-ksei";
		 
		 String url = "https://"+IP_ADDR+":8080/basic/monitoring/check-socket-ksei";

		 HttpHeaders headers = new HttpHeaders();
		 headers = this.createHeaders();
		 headers.setContentType(MediaType.APPLICATION_JSON);
		 HttpEntity<String> entity = new HttpEntity<String>("",headers);
		 System.out.println("header: " + headers);
	
		 try {
				log.debug("SEND CHECK CONNECTION KSEI.."  );
				ResponseEntity<String> resp = currentRestTemplate.postForEntity(url, entity, String.class);
				log.debug("SEND CHECK CONNECTION KSEI... SUCCESS..." );
				responseRest =  resp.getStatusCode().is2xxSuccessful();
				System.out.println(" SEND CHECK CONNECTION KSEI... SUCCESS..." );
			} catch (HttpClientErrorException e) {
				HttpStatus httpStatus = e.getStatusCode();
				log.error("SEND CHECK CONNECTION KSEI... FAILED..." + httpStatus.toString() + " : " + httpStatus.getReasonPhrase());
				System.out.println(" SEND CHECK CONNECTION KSEI... FAILED..." );
				responseRest =  e.getStatusCode().is2xxSuccessful();
			} catch (Exception e) {
				log.error("SEND CHECK CONNECTION KSEI... FAILED... "+e.getMessage());
				System.out.println(" SEND CHECK CONNECTION KSEI... FAILED...");
			}
		 
		 return responseRest;
	}
	
	
	public Boolean startITM(){
		
		 Boolean responseRest = false;
		 RestTemplate currentRestTemplate = RestUtil.getSecureRestTemplate();
		// String url = "https://localhost:8080/basic/monitoring/check-socket-ksei";
		 
		 String url = "https://"+IP_ADDR+":8080/basic/monitoring/start-itm";

		 HttpHeaders headers = new HttpHeaders();
		 headers = this.createHeaders();
		 headers.setContentType(MediaType.APPLICATION_JSON);
		 HttpEntity<String> entity = new HttpEntity<String>("",headers);
		 System.out.println("header: " + headers);
	
		 try {
				log.debug("SEND START ITM.."  );
				ResponseEntity<String> resp = currentRestTemplate.postForEntity(url, entity, String.class);
				log.debug("SEND START ITM... SUCCESS..." );
				responseRest =  resp.getStatusCode().is2xxSuccessful();
				System.out.println(" SEND START ITM... SUCCESS..." );
			} catch (HttpClientErrorException e) {
				HttpStatus httpStatus = e.getStatusCode();
				log.error("SEND START ITM... FAILED..." + httpStatus.toString() + " : " + httpStatus.getReasonPhrase());
				System.out.println(" SEND START ITM... FAILED..." );
				responseRest =  e.getStatusCode().is2xxSuccessful();
			} catch (Exception e) {
				log.error("SEND START ITM... FAILED... "+e.getMessage());
				System.out.println(" SEND START ITM... FAILED...");
			}
		 
		 
		 //keperluan test
		 //responseRest = true;
		 //end keperluan test
		 return responseRest;
	}
	
	
	public Boolean stopITM(){
		
		 Boolean responseRest = false;
		 RestTemplate currentRestTemplate = RestUtil.getSecureRestTemplate();
		 
		 String url = "https://"+IP_ADDR+":8080/basic/monitoring/stop-itm";

		 HttpHeaders headers = new HttpHeaders();
		 headers = this.createHeaders();
		 headers.setContentType(MediaType.APPLICATION_JSON);
		 HttpEntity<String> entity = new HttpEntity<String>("",headers);
		 System.out.println("header: " + headers);
	
		 try {
				log.debug("SEND STOP ITM.."  );
				ResponseEntity<String> resp = currentRestTemplate.postForEntity(url, entity, String.class);
				log.debug("SEND STOP ITM... SUCCESS..." );
				responseRest =  resp.getStatusCode().is2xxSuccessful();
				System.out.println(" SEND STOP ITM... SUCCESS..." );
			} catch (HttpClientErrorException e) {
				HttpStatus httpStatus = e.getStatusCode();
				log.error("SEND STOP ITM... FAILED..." + httpStatus.toString() + " : " + httpStatus.getReasonPhrase());
				System.out.println(" SEND STOP ITM... FAILED..." );
				responseRest =  e.getStatusCode().is2xxSuccessful();
			} catch (Exception e) {
				log.error("SEND STOP ITM... FAILED... "+e.getMessage());
				System.out.println(" SEND STOP ITM... FAILED...");
			}
		 
		 System.out.println("  ");
		 //fixing untuk tombol stop koneksi ITM
		 if(responseRest) //true apabila sukses mengirimkan stop ke ITM
			 responseRest = false ; //sehingga perlu diubah untuk proses cek didepan, harus false untuk bisa menampilkan button START 
		 if(!responseRest) //false apabila gagal mengirimkan stop ke ITM 
			 responseRest = true; //sehingga perlu diubah untuk proses cek di deoan, harus true agar tetap bisa menampilkan button STOP 
		 
		 return responseRest;
	}
	
	/* belum ada api nya
	public Boolean startKSEI(){
		
		 Boolean responseRest = false;
		 RestTemplate currentRestTemplate = RestUtil.getSecureRestTemplate();
		 
		 String url = "https://"+IP_ADDR+":8080/basic/monitoring/stop-itm";

		 HttpHeaders headers = new HttpHeaders();
		 headers = this.createHeaders();
		 headers.setContentType(MediaType.APPLICATION_JSON);
		 HttpEntity<String> entity = new HttpEntity<String>("",headers);
		 System.out.println("header: " + headers);
	
		 try {
				log.debug("SEND STOP ITM.."  );
				ResponseEntity<String> resp = currentRestTemplate.postForEntity(url, entity, String.class);
				log.debug("SEND STOP ITM... SUCCESS..." );
				responseRest =  resp.getStatusCode().is2xxSuccessful();
				System.out.println(" SEND STOP ITM... SUCCESS..." );
			} catch (HttpClientErrorException e) {
				HttpStatus httpStatus = e.getStatusCode();
				log.error("SEND STOP ITM... FAILED..." + httpStatus.toString() + " : " + httpStatus.getReasonPhrase());
				System.out.println(" SEND STOP ITM... FAILED..." );
				responseRest =  e.getStatusCode().is2xxSuccessful();
			} catch (Exception e) {
				log.error("SEND STOP ITM... FAILED... "+e.getMessage());
				System.out.println(" SEND STOP ITM... FAILED...");
			}
		 
		 return responseRest;
	}
	*/
	
	public Boolean signon(){
		
		 Boolean responseRest = false;
		 RestTemplate currentRestTemplate = RestUtil.getSecureRestTemplate();
		 
		 String url = "https://"+IP_ADDR+":8080/basic/monitoring/signon";

		 HttpHeaders headers = new HttpHeaders();
		 headers = this.createHeaders();
		 headers.setContentType(MediaType.APPLICATION_JSON);
		 HttpEntity<String> entity = new HttpEntity<String>("",headers);
		 System.out.println("header: " + headers);
	
		 try {
				log.debug("SEND SIGNON ITM.."  );
				ResponseEntity<String> resp = currentRestTemplate.postForEntity(url, entity, String.class);
				log.debug("SEND SIGNON ITM... SUCCESS..." );
				responseRest =  resp.getStatusCode().is2xxSuccessful();
				System.out.println(" SEND SIGNON ITM... SUCCESS..." );
			} catch (HttpClientErrorException e) {
				HttpStatus httpStatus = e.getStatusCode();
				log.error("SEND SIGNON ITM... FAILED..." + httpStatus.toString() + " : " + httpStatus.getReasonPhrase());
				System.out.println(" SEND SIGNON ITM... FAILED..." );
				responseRest =  e.getStatusCode().is2xxSuccessful();
			} catch (Exception e) {
				log.error("SEND SIGNON ITM... FAILED... "+e.getMessage());
				System.out.println(" SEND SIGNON ITM... FAILED...");
			}
		 
		 return responseRest;
	}
	
	public Boolean signoff(){
		
		 Boolean responseRest = false;
		 RestTemplate currentRestTemplate = RestUtil.getSecureRestTemplate();
		 
		 String url = "https://"+IP_ADDR+":8080/basic/monitoring/signoff";

		 HttpHeaders headers = new HttpHeaders();
		 headers = this.createHeaders();
		 headers.setContentType(MediaType.APPLICATION_JSON);
		 HttpEntity<String> entity = new HttpEntity<String>("",headers);
		 System.out.println("header: " + headers);
	
		 try {
				log.debug("SEND SIGNOFF ITM.."  );
				ResponseEntity<String> resp = currentRestTemplate.postForEntity(url, entity, String.class);
				log.debug("SEND SIGNOFF ITM... SUCCESS..." );
				responseRest =  resp.getStatusCode().is2xxSuccessful();
				System.out.println(" SEND SIGNOFF ITM... SUCCESS..." );
			} catch (HttpClientErrorException e) {
				HttpStatus httpStatus = e.getStatusCode();
				log.error("SEND SIGNOFF ITM... FAILED..." + httpStatus.toString() + " : " + httpStatus.getReasonPhrase());
				System.out.println(" SEND SIGNOFF ITM... FAILED..." );
				responseRest =  e.getStatusCode().is2xxSuccessful();
			} catch (Exception e) {
				log.error("SEND SIGNOFF ITM... FAILED... "+e.getMessage());
				System.out.println(" SEND SIGNOFF ITM... FAILED...");
			}
		 
		 return responseRest;
	}
	
	
	public Boolean echo(){
		
		 Boolean responseRest = false;
		 RestTemplate currentRestTemplate = RestUtil.getSecureRestTemplate();
		 
		 String url = "https://"+IP_ADDR+":8080/basic/monitoring/echo";

		 HttpHeaders headers = new HttpHeaders();
		 headers = this.createHeaders();
		 headers.setContentType(MediaType.APPLICATION_JSON);
		 HttpEntity<String> entity = new HttpEntity<String>("",headers);
		 System.out.println("header: " + headers);
	
		 try {
				log.debug("SEND ECHO ITM.."  );
				ResponseEntity<String> resp = currentRestTemplate.postForEntity(url, entity, String.class);
				log.debug("SEND ECHO ITM... SUCCESS..." );
				responseRest =  resp.getStatusCode().is2xxSuccessful();
				System.out.println(" SEND ECHO ITM... SUCCESS..." );
			} catch (HttpClientErrorException e) {
				HttpStatus httpStatus = e.getStatusCode();
				log.error("SEND ECHO ITM... FAILED..." + httpStatus.toString() + " : " + httpStatus.getReasonPhrase());
				System.out.println(" SEND ECHO ITM... FAILED..." );
				responseRest =  e.getStatusCode().is2xxSuccessful();
			} catch (Exception e) {
				log.error("SEND ECHO ITM... FAILED... "+e.getMessage());
				System.out.println(" SEND ECHO ITM... FAILED...");
			}
		 
		 return responseRest;
	}
	
}
