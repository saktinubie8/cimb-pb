package id.co.telkomsigma.app.authhandler;

import id.co.telkomsigma.app.constant.WebGuiConstant;
import id.co.telkomsigma.app.controller.GenericController;
import id.co.telkomsigma.app.model.web.menu.Menu;
import id.co.telkomsigma.app.model.web.parameter.SystemParameter;
import id.co.telkomsigma.app.model.web.security.AuditLog;
import id.co.telkomsigma.app.model.web.user.Role;
import id.co.telkomsigma.app.model.web.user.User;
import id.co.telkomsigma.app.service.AuditLogService;
import id.co.telkomsigma.app.service.MenuService;
import id.co.telkomsigma.app.service.ParameterService;
import id.co.telkomsigma.app.service.RoleService;
import id.co.telkomsigma.app.service.UserService;
import id.co.telkomsigma.app.util.UserUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.ldap.userdetails.LdapUserDetails;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component("authenticationSuccessHandler")
public class AuthenticationSuccessHandler extends GenericController implements org.springframework.security.web.authentication.AuthenticationSuccessHandler {
    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Autowired
    private RoleService roleService;

    @Autowired
    private MenuService menuService;

    @Autowired
    private ParameterService paramService;

    @Autowired
    private AuditLogService auditLogService;

    @Autowired
    private UserService userServiceWithCache;

    @Autowired
    private SessionRegistry sessionRegistry;
	
	/*@Post
	private PasswordEncoder passwordEncoder;*/

//    private List<SessionInformation> getActiveSessions() {
//      List<SessionInformation> activeSessions = new ArrayList<>();
//      for(Object principal : sessionRegistry.getAllPrincipals()) {
//        activeSessions.addAll(sessionRegistry.getAllSessions(principal, false));
//      }
//      return activeSessions;
//    }

//    private Boolean userIsOnline(User user) {
//	    Iterator iterator = getActiveSessions().iterator();
//    	int count = 0;
//    	while(iterator.hasNext()) {
//	    	SessionInformation sessionData = (SessionInformation)iterator.next();
//	    	User userData = (User)sessionData.getPrincipal();
//	    	if(userData.getUsername().equals(user.getUsername())) {
//	    		count++;
//	    		if(count>1)
//	    			return true;
//	    	}
//	    }
//	    return false;
//    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication auth) throws IOException, ServletException {
        if (response.isCommitted()) {
            return;
        }
        log.info("Username : " + auth.getName());
        User user = (User) userServiceWithCache.loadUserByUsername(auth.getName());
        if (user != null) {
            Set<Role> rolesOwnedByUser = user.getRoles();
            List<Role> allRoles = roleService.getRoles();
            List<Menu> header = new ArrayList<Menu>();
            List<Menu> menu = new ArrayList<Menu>();

            Set<Long> checkHeader = new HashSet<>();
            Set<Long> checkMenu = new HashSet<>();

	      /*Boolean match = true;
    	    Long expiryDays= 0L;
    	    match = userServiceWithCache.checkDefaultPassword(user);
    	    expiryDays = userServiceWithCache.checkExpiryDays(user);
    	    log.debug("=====   diff : " + expiryDays);*/

            SystemParameter param = paramService.getParamByParamName("expiryDays");

            for (final Role role : rolesOwnedByUser) {
                for (Role role2 : allRoles) {
                    if (role2.getAuthority().equals(role.getAuthority())) {
                        List<Menu> menuByRole = new ArrayList();
                        menuByRole = menuService.getMenuByRole(role2.getRoleId());

                        Collections.sort(menuByRole);
                        for (Menu menus : menuByRole) {
                            if (menus.getMenuPath().contains("LABEL")) {

                            } else {
                                if (!checkMenu.contains(menus.getMenuId())) {
                                    menu.add(menus);
                                    checkMenu.add(menus.getMenuId());

                                    if (menus.getMenuParentId() != null) {
                                        Menu parentMenu = menuService.getMenuByMenuId(menus.getMenuParentId());
                                        if (!checkHeader.contains(parentMenu.getMenuId())) {
                                            header.add(parentMenu);
                                            checkHeader.add(parentMenu.getMenuId());
                                        }
                                    }

                                }
                            }
                        }
                    }

                }
                request.getSession(true).setAttribute("headers", header);
                request.getSession(true).setAttribute("menus", menu);
            }
            log.debug("Logged in using " + user.getUserId() + " : " + user.getUsername() + " : " + request.getSession().getId());

            String date = new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date());

            AuditLog auditLog = new AuditLog();
            auditLog.setActivityType(1);
            auditLog.setCreatedBy(user.getUsername());
            auditLog.setCreatedDate(new Date());
            auditLog.setDescription("Login success");
            auditLog.setReffNo(date);

            auditLogService.insertAuditLog(auditLog);
            log.debug("Added Audit Log");
            List<String> grantedRoles = UserUtil.checkGrantedRoles(auth);
            if (UserUtil.isSystemUser(grantedRoles)) {
                // reject user system from login
                new SecurityContextLogoutHandler().logout(request, response, auth);
                redirectStrategy.sendRedirect(request, response, "/login?error=true");
            } else {
                String currSessionId = request.getSession().getId();
                for (Object principal : sessionRegistry.getAllPrincipals()) {
                    for (SessionInformation sessionInfo : sessionRegistry.getAllSessions(principal, true)) {
                        LdapUserDetails userData = (LdapUserDetails)sessionInfo.getPrincipal();
                        log.info(userData.toString());
                        if (userData.getUsername().equals(user.getUsername()) && !currSessionId.equals(sessionInfo.getSessionId())) {
                            log.info("Force Signout previous user login for user " + userData.toString() + " of session ID " + sessionInfo.getSessionId());
                            sessionInfo.expireNow();
                        }
                    }
                }
                /*
	    		Boolean match = true;
	    		Long expiryDays= 0L;
	    		match = userServiceWithCache.checkDefaultPassword(user);
	    		expiryDays = userServiceWithCache.checkExpiryDays(user);
	    		log.debug("=====   diff : " + expiryDays);
                if (match){
	    	      	  log.debug("password user : " + user.getPassword());
	    	       	  redirectStrategy.sendRedirect(request, response, WebGuiConstant.AFTER_LOGIN_PATH_PROFILE_PAGE);
	            }else {
	    	       // if cek hari password sudah 90 hari
	    	          if(expiryDays>90L){
	    	     	   		redirectStrategy.sendRedirect(request, response, WebGuiConstant.AFTER_LOGIN_PATH_PROFILE_PAGE);
	    	     	  }
	    	     	  else {
	    	     	   		redirectStrategy.sendRedirect(request, response, WebGuiConstant.AFTER_LOGIN_PATH_HOME_PAGE);
	    	       	 }
	            }*/
                redirectStrategy.sendRedirect(request, response, WebGuiConstant.AFTER_LOGIN_PATH_HOME_PAGE);
            }
        }else{
            log.info("User tidak ditemukan di Aplikasi CIMB-PB");
            new SecurityContextLogoutHandler().logout(request, response, auth);
            redirectStrategy.sendRedirect(request, response, "/login?error=true");
        }


    	/*
    	 * MASIH DISPUTE
    	//ambil parameter dengan user tidak pernah login dan sudah expired masih menggunakan password default
    	SystemParameter paramExpDef =  paramService.getParamByParamName("expiryDaysDefaultPwd");
    	*/
      /* if (match) {
            log.debug("password user : " + user.getPassword());
            redirectStrategy.sendRedirect(request, response, WebGuiConstant.AFTER_LOGIN_PATH_PROFILE_PAGE);
        } else {
            if (expiryDays > Long.parseLong(param.getParamValue())) {
                redirectStrategy.sendRedirect(request, response, WebGuiConstant.AFTER_LOGIN_PATH_PROFILE_PAGE);
            } else {*/



        /*        if (!"0".equals(user.getCustomProp(WebGuiConstant.PARAMETER_USER_CURRENT_FAILED_ATTEMPT))) {
                    user.setCustomProp(WebGuiConstant.PARAMETER_USER_CURRENT_FAILED_ATTEMPT, "0");
                    user.getCustomProp(WebGuiConstant.PARAMETER_USER_CURRENT_FAILED_ATTEMPT);
                    userServiceWithCache.updateUser(user);
                }
*/

        //  }
        //  }


    }
}