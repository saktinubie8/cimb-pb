package id.co.telkomsigma.app.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import id.co.telkomsigma.app.constant.WebGuiConstant;
import id.co.telkomsigma.app.model.web.menu.Menu;
import id.co.telkomsigma.app.model.web.security.AuditLog;
import id.co.telkomsigma.app.model.web.user.Role;
import id.co.telkomsigma.app.model.web.user.User;
import id.co.telkomsigma.app.pojo.DataTablesPojo;
import id.co.telkomsigma.app.service.AuditLogService;
import id.co.telkomsigma.app.service.MenuService;
import id.co.telkomsigma.app.service.RoleService;
import id.co.telkomsigma.app.service.UserService;
import id.co.telkomsigma.app.util.FormValidationUtil;
import id.co.telkomsigma.app.util.JsonUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;

import static org.apache.commons.lang.StringEscapeUtils.escapeHtml;

@Controller
public class ManageRoleController extends GenericController {

	// ----------------------------------------- LIST ROLE SECTION
	// ----------------------------------------- //

	private static final String LIST_PAGE_NAME = "manage/role/list";
	private static final String LIST_COMMAND = "redirect:"
			+ WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_ROLE;

	@Autowired
	private RoleService roleService;

	@Autowired
	private UserService userService;

	@Autowired
	private MenuService menuService;

	@Autowired
	private AuditLogService auditLogService;

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_ROLE_MAPPING, method = RequestMethod.GET)
	public String showListPage(final Model model,
			final HttpServletRequest request, final HttpServletResponse response) {
		List<Role> roles = roleService.getRoles();
		model.addAttribute("roles", roles);
		
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		User user = (User) auth.getPrincipal();
		
		String date = new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date());
		
        AuditLog auditLog = new AuditLog();
        auditLog.setActivityType(auditLog.VIEW_PAGE);
        auditLog.setCreatedBy(user.getUsername());
        auditLog.setCreatedDate(new Date());
        auditLog.setDescription("Buka Halaman Kelola Role");
        auditLog.setReffNo(date);
        
        try{
        	auditLogService.insertAuditLog(auditLog);
        }
        catch(Exception e){
        	e.printStackTrace();
        }
        log.debug("Added Audit Log");
				
		return getPageContent(LIST_PAGE_NAME);
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_ROLE_GET_LIST_MAPPING, method = RequestMethod.GET)
	public void doSearch(final HttpServletRequest request,
			final HttpServletResponse response) throws IOException {
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User userAuth = (User) auth.getPrincipal();
		
		//karena role hanya bisa 1, jadi default diambil yang index pertama dari Set<Role>
		Role roleAuth = userAuth.getRoles().iterator().next();
		
		System.out.print("Role = " + roleAuth.getAuthority());
		
		if (isAjaxRequest(request)) {
			setResponseAsJson(response);

			DataTablesPojo dataTablesPojo = new DataTablesPojo();
			try {
				dataTablesPojo.setDraw(Integer.parseInt(request
						.getParameter("draw")));
			} catch (NumberFormatException e) {
				dataTablesPojo.setDraw(0);
			}

			final int orderableColumnCount = 2;
			final StringBuffer sbOrder = new StringBuffer();
			LinkedHashMap<String, String> columnMap = new LinkedHashMap<String, String>() {
				{
					put("0", "authority");
					put("1", "description");
				}
			};

			int offset = 0;
			int limit = 50;
            String orderColumn = "";
            String orderDir = "";
			try {
				offset = Integer.parseInt(request.getParameter("start"));
			} catch (NumberFormatException e) {
			}
			try {
				limit = Integer.parseInt(request.getParameter("length"));
			} catch (NumberFormatException e) {
			}

			for (int i = 0; i < orderableColumnCount; i++) {
				orderColumn = request.getParameter("order[" + i
						+ "][column]");
				if (orderColumn == null || orderColumn.equalsIgnoreCase("")) {
					break;
				} else {
					orderColumn = columnMap.get(orderColumn);
					if (orderColumn == null) {
						break;
					}
					orderDir = request.getParameter("order[" + i
							+ "][dir]");
					if (orderDir == null || !orderDir.equalsIgnoreCase("desc")) {
						orderDir = "asc";
					}
					sbOrder.append(orderColumn);
					sbOrder.append("-");
					sbOrder.append(orderDir);
					sbOrder.append("+");
				}
			}

			final String authority = request.getParameter("q");


			try {
				final int finalOffset = offset;
				final int finalLimit = limit;

				Page<Role> roleListPojo = roleService.search(authority,columnMap,finalOffset,finalLimit,orderColumn,orderDir);

				List<String[]> data = new ArrayList<>();

				for (Role role : roleListPojo) {
					
					String url = "";
					if(!roleAuth.getAuthority().toUpperCase().equals("ADMIN")){
						url = getMessage("message.error.role.not.admin");
					}
					else {
						url = " <a href=\""
								+ getServletContext().getContextPath()
								+ WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_ROLE_ADD_REQUEST
										.replace("{roleId}",
												"" + role.getRoleId())
								+ "\" data-tooltip='true' data-placement='bottom' title='"
								+ getMessage("label.update")+"'><i class='fa fa-pencil fa-1x'></i></a>" ;
					}
					
					data.add(new String[] {
							escapeHtml(role.getAuthority().toUpperCase()),
							escapeHtml(role.getDescription()),
							url
						    ,escapeHtml("")
					
					});}

				dataTablesPojo.setData(data);
				dataTablesPojo.setRecordsFiltered(roleListPojo.getContent().size());
				dataTablesPojo.setRecordsTotal(Integer.parseInt(""+roleListPojo.getTotalElements()));
				
				
			} catch (HttpClientErrorException e) {
				log.error(e.getStatusCode().toString(), e);
				dataTablesPojo
						.setError(getMessage("message.datatables.error.fetch.role"));
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				dataTablesPojo
						.setError(getMessage("message.datatables.error.fetch.role"));
			} finally {
				response.getWriter().write(
						new JsonUtils().toJson(dataTablesPojo));
			}
		} else {
			response.setStatus(HttpServletResponse.SC_FOUND);
		}

	}

	// ----------------------------------------- ADD/EDIT ROLE SECTION
	// ----------------------------------------- //

	private static final String ADD_PAGE_NAME = "manage/role/add";

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_ROLE_ADD_MAPPING, method = { RequestMethod.GET })
	public String showAddPage(
			@RequestParam(value = "roleId", required = false) final Long roleId,
			final Model model, final HttpServletRequest request,
			final HttpServletResponse response) {

		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		User user = (User) auth.getPrincipal();
	
        AuditLog auditLog = new AuditLog();
        auditLog.setActivityType(auditLog.VIEW_PAGE);
        auditLog.setCreatedBy(user.getUsername());
        auditLog.setCreatedDate(new Date());
        auditLog.setDescription("Buka Halaman Ubah atau Tambah Role ");
        auditLog.setReffNo(new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date()));
        
        try{
        	auditLogService.insertAuditLog(auditLog);
        }
        catch(Exception e){
        	e.printStackTrace();
        }
		
		
		List<Menu> menus = menuService.getAllMenus();
		model.addAttribute("allMenus", menus);

		if (roleId != null) {
				Role role = roleService.loadUserByRoleId(roleId);

				for (Menu menu : role.getMenus()) {
					if(!menu.getMenuPath().contains("LABEL"))
						role.getStrMenus().add(menu.getMenuId() + "");
				}
				role.setRoleId(roleId);
				model.addAttribute("role", role);
		} else
			model.addAttribute("role", new Role());

		return getPageContent(ADD_PAGE_NAME);
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_ROLE_ADD_MAPPING, method = { RequestMethod.POST })
	public String doAdd(
			@RequestParam(value = "roleId", required = false) final Long roleId,
			@ModelAttribute("role") Role role, BindingResult errors,
			final Model model, final HttpServletRequest request,
			final HttpServletResponse response) {
		  String pageReturn = ADD_PAGE_NAME;
		// validating role
		if (roleId == null)
			FormValidationUtil.validatingAuthority(errors, "authority",
					role.getAuthority(), getMessage("label.role.name"),
					Role.MAX_LENGTH_AUTHORITY, Role.MIN_LENGTH_AUTHORITY);

		// validating description
		FormValidationUtil.validatingRoleDescription(errors, "description",
				role.getDescription(), getMessage("label.role.description"),
				Role.MAX_LENGTH_DESCRIPTION);

		// validating MENU
		FormValidationUtil.validatingMenus(errors, "menus", role.getStrMenus(),
				getMessage("label.role.menu"));

		List<Menu> allMenus = menuService.getAllMenus();

		if (!errors.hasErrors()) {
			Authentication auth = SecurityContextHolder.getContext()
					.getAuthentication();
			User admin = (User) auth.getPrincipal();

			Long userId = admin.getUserId();
			
			//add auditLog
			
			AuditLog auditLog = new AuditLog();
	        auditLog.setActivityType(auditLog.SAVE_OR_UPDATE);
	        auditLog.setCreatedBy(admin.getUsername());
	        auditLog.setCreatedDate(new Date());
	        auditLog.setReffNo(new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date()));
	        

			role.setUpdatedBy(admin);
			role.setUpdatedDate(new Date());
			for (String strMenuId : role.getStrMenus()) {
				Menu menu = new Menu();
				menu.setMenuId(Long.parseLong(strMenuId));
				role.addMenu(menu);
			}

			if (roleId == null) {// for add
				role.setUserIdCreated(userId);
				role.setCreatedBy(admin);
				role.setCreatedDate(new Date());
				try {
					roleService.createRole(role);
					model.addAttribute("role", new Role());
					model.addAttribute("okMessage",
							getMessage("form.ok.add.role"));
			        auditLog.setDescription("Tambah Role Berhasil");
			        try{
			        	auditLogService.insertAuditLog(auditLog);
			        }
			        catch(Exception e){
			        	e.printStackTrace();
			        }

				} catch (Exception e) {
					log.error(e.getMessage(), e);
					model.addAttribute("errorMessage",
							getMessage("form.error.add.user"));
			        auditLog.setDescription("Tambah Role Gagal ");
			        try{
			        	auditLogService.insertAuditLog(auditLog);
			        }
			        catch(Exception ex){
			        	ex.printStackTrace();
			        }

				}
				pageReturn = LIST_PAGE_NAME;
			} else {
				// for edit
				try {
					Role fromDb = roleService.loadUserByRoleId(roleId);
					role.setAuthority(fromDb.getAuthority());
					role.setUpdatedBy(admin);
					role.setUpdatedDate(new Date());
					role.setUserIdUpdated(userId);
					
					roleService.updateRole(role);
					model.addAttribute("okMessage",
							getMessage("form.ok.edit.role"));
			        auditLog.setDescription("Ubah Role " +role.getAuthority() + " Sukses ");
			        try{
			        	auditLogService.insertAuditLog(auditLog);
			        }
			        catch(Exception e){
			        	e.printStackTrace();
			        }
				} catch (HttpClientErrorException e) {

					log.error(e.getStatusCode().toString(), e);
					model.addAttribute("errorMessage",
							getMessage("form.error.edit.role"));
					auditLog.setDescription("Ubah Role " +role.getAuthority() + " Gagal ");
				     try{
				        	auditLogService.insertAuditLog(auditLog);
				        }
				        catch(Exception ex){
				        	ex.printStackTrace();
				        }
				} catch (Exception e) {
					log.error(e.getMessage(), e);
					model.addAttribute("errorMessage",
							getMessage("form.error.edit.role"));
					auditLog.setDescription("Ubah Role " +role.getAuthority() + " Gagal ");
				     try{
				        	auditLogService.insertAuditLog(auditLog);
				        }
				        catch(Exception ex){
				        	ex.printStackTrace();
				        }
				}
				pageReturn = ADD_PAGE_NAME;
			}
		}
		// cek kalau pas edit, ada error, kewenangan tidak kosong
		if (roleId != null) {
			Role fromDb;
			try {
				fromDb = roleService.loadUserByRoleId(roleId);
				role.setAuthority(fromDb.getAuthority());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		model.addAttribute("allMenus", allMenus);
		return getPageContent(pageReturn);
	}

	/* @RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_ROLE_DELETE_MAPPING, method = RequestMethod.GET)
	 public String doDelete(final Model model, final HttpServletRequest request, final HttpServletResponse response, final Long endpointId) {	     	
			
			if(endpointId!=null)
			{
				RestEndpoint fromDb = restService.findRestEndpointById(endpointId);
				if(fromDb!=null)
				{
					try{
					     	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
							User user = (User) auth.getPrincipal();
							
							if(fromDb.getGroupChilds()!=null && fromDb.getGroupChilds().size()>0){
								restService.deleteRestEndpointAndChild(fromDb);
							}
							else {
								restService.deleteRestEndpoint(endpointId);
							}
							
							model.addAttribute("okMessage",getMessage("form.ok.hapus.endpoint",fromDb.getName()));
							String date = new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date());
					     	
					     	AuditLog auditLogs = new AuditLog();
					     	auditLogs.setActivityType(auditLogs.SAVE_OR_UPDATE);
					     	auditLogs.setCreatedBy(user.getUsername());
					     	auditLogs.setCreatedDate(new Date());
					     	auditLogs.setDescription("Delete data REST Endpoint");
					     	auditLogs.setReffNo(date);		        
				        	auditLogService.insertAuditLog(auditLogs);
				        	log.debug("Added Audit Log");
				        }
				        catch(Exception ex){
				        	ex.printStackTrace();
				        	log.error(ex);
							model.addAttribute("errorMessage",getMessage("form.error.delete.endpoint"));
				        }	
					}
				}
				else
				{
					model.addAttribute("errorMessage",getMessage("form.error.delete.endpoint.notfound"));
				}		        
	    	return getPageContent(LIST_PAGE_NAME);
	  }
*/
	
}
