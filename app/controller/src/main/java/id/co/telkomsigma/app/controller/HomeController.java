package id.co.telkomsigma.app.controller;

import id.co.telkomsigma.app.constant.WebGuiConstant;
import id.co.telkomsigma.app.controller.GenericController;
import id.co.telkomsigma.app.model.web.parameter.SystemParameter;
import id.co.telkomsigma.app.model.web.user.User;
import id.co.telkomsigma.app.pojo.DataTablesPojo;
import id.co.telkomsigma.app.service.ParameterService;
import id.co.telkomsigma.app.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import static org.apache.commons.lang.StringEscapeUtils.escapeHtml;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by daniel on 4/23/15.
 */
@Controller("homeController")
public class HomeController extends GenericController {
	private static final String HOME_PAGE_NAME = "home";
	
    @Autowired
    private ParameterService parameterService;
    
    @Autowired
    private UserService userServiceWithCache;

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_HOME_PAGE, method = { RequestMethod.GET,
			RequestMethod.POST })
	public String showHomepageDashboard(final Model model, final HttpServletRequest request,
			final HttpServletResponse response) {
		
		SystemParameter paramDays = new SystemParameter();
        paramDays = parameterService.getParamByParamName("expiryDays");
        System.out.println("paramDays : " + paramDays.toString());
        
	    Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) userServiceWithCache.loadUserByUsername(auth.getName());
        Long expiryDaysUser = 0L;
	    expiryDaysUser = userServiceWithCache.checkExpiryDays(user);
        
	    
	    Long expDay =  Long.parseLong(paramDays.getParamValue());
	    System.out.println("expDay : " + paramDays.getParamValue());
	    System.out.println("expiryDays : " + expiryDaysUser);	 
	    
	    Long diff = 0L;
	    
	    
	    if (expiryDaysUser < Long.parseLong(paramDays.getParamValue())) {
	    	//cek selisih
	    	diff = expDay - expiryDaysUser ;
	    	System.out.println("diff : " + diff);
	    	
	    	if(diff > 7L) {
	    		model.addAttribute("okMessage", getMessage("form.warning.change.password"));
	    	}
	    	if(diff == 7L) {
	    		model.addAttribute("okMessage", getMessage("form.warning.change.password.7"));
	    	}
	    	else if(diff==15L) {
	    		model.addAttribute("okMessage", getMessage("form.warning.change.password.15"));
	    	}
	    	else if(diff == 30L) {
	    		model.addAttribute("okMessage", getMessage("form.warning.change.password.30"));
	    	}
	    	
			/*
			 * if (diff <= 7L || diff == 15L ||diff == 30L) {
			 * model.addAttribute("okMessage", getMessage("form.warning.change.password"));
			 * }
			 */
	    }
		return getPageContent(HOME_PAGE_NAME);
	}

}
