package id.co.telkomsigma.app.controller;

import id.co.telkomsigma.app.constant.WebGuiConstant;
import id.co.telkomsigma.app.controller.GenericController;
import id.co.telkomsigma.app.model.web.parameter.SystemParameter;
import id.co.telkomsigma.app.model.web.security.AuditLog;
import id.co.telkomsigma.app.model.web.user.Role;
import id.co.telkomsigma.app.model.web.user.User;
import id.co.telkomsigma.app.pojo.DataTablesPojo;
import id.co.telkomsigma.app.service.AuditLogService;
import id.co.telkomsigma.app.service.MenuService;
import id.co.telkomsigma.app.service.ParameterService;
import id.co.telkomsigma.app.util.FormValidationUtil;
import id.co.telkomsigma.app.util.JsonUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.apache.commons.lang.StringEscapeUtils.escapeHtml;

/**
 * Created by daniel on 4/24/15.
 */
@Controller
public class ManageParamController extends GenericController {
    // ----------------------------------------- LIST PARAMETER SECTION ----------------------------------------- //
    private static final String LIST_PAGE_NAME = "manage/parameter/list";
    private static final String LIST_COMMAND = "redirect:" + WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_PARAM;
    
    private static final Long MIN_EXP_DAYS = 90L;
    private static final Long MIN_LENGTH_PWD = 8L;
    private static final Long MIN_HIST_CHANGE_PWD = 18L;

    @Autowired
    private ParameterService paramService;

    @Autowired
    private AuditLogService auditLogService;

    @Autowired
    private RESTClient restClient;

    @RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_PARAM_MAPPING, method = RequestMethod.GET)
    public String showListPage(final Model model, final HttpServletRequest request, final HttpServletResponse response) {
        List<SystemParameter> params = paramService.getParameter();
        List<String> paramGroup = new ArrayList<String>();

        Authentication auth = SecurityContextHolder.getContext()
                .getAuthentication();
        User user = (User) auth.getPrincipal();

        AuditLog auditLog = new AuditLog();
        auditLog.setActivityType(auditLog.VIEW_PAGE);
        auditLog.setCreatedBy(user.getUsername());
        auditLog.setCreatedDate(new Date());
        auditLog.setDescription("Buka Halaman Kelola Parameter");
        auditLog.setReffNo(new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date()));

        try {
            auditLogService.insertAuditLog(auditLog);
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.debug("Added Audit Log");


        Iterator iterator = params.iterator();
        while (iterator.hasNext()) {
            SystemParameter systemParameter = (SystemParameter) iterator.next();
            paramGroup.add(systemParameter.getParamGroup());
        }

        Set<String> grupParam = new HashSet<String>(paramGroup);

        model.addAttribute("params", params);
        model.addAttribute("paramGroup", grupParam);
        return getPageContent(LIST_PAGE_NAME);
    }

    @RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_PARAM_GET_LIST_MAPPING, method = RequestMethod.GET)
    public void doSearch(final Model model, final HttpServletRequest request, final HttpServletResponse response) throws IOException {

    	
    	Enumeration<String> parameterNames = request.getParameterNames();
		String orderBy = null;
		String sortBy = null;
		
		while (parameterNames.hasMoreElements()) {

			String paramName = parameterNames.nextElement();
			//System.out.println("paramname : " + paramName);
			// String paramValues = request.getParameter(paramName);
			if (paramName.contains("dir")) {
				sortBy = request.getParameter(paramName);
			//	System.out.println("sort by : " + sortBy);
			} else if (paramName.contains("order") && paramName.contains("column")) {
				orderBy = request.getParameter(paramName);
				System.out.println("order by : " + orderBy);
				
				switch (orderBy) {
				case "0":
					orderBy = "paramGroup";
				//	System.out.println("order by paramGroup : " + orderBy);
					break;
				case "1":
					orderBy = "paramName";
				//	System.out.println("order by paramName : " + orderBy);
					break;
				case "2":
					orderBy = "paramDescription";
				//	System.out.println("order by paramDescription : " + orderBy);
					break;
				case "3":
					orderBy = "paramValue";
				//	System.out.println("order by paramValue : " + orderBy);
					break;
				default:
					orderBy = "paramName";
				}
			}
		}
    	
    	
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User userAuth = (User) auth.getPrincipal();

        //karena role hanya bisa 1, jadi default diambil yang index pertama dari Set<Role>
        Role roleAuth = userAuth.getRoles().iterator().next();

        if (isAjaxRequest(request)) {
            setResponseAsJson(response);

            DataTablesPojo dataTablesPojo = new DataTablesPojo();
            try {
                dataTablesPojo.setDraw(Integer.parseInt(request.getParameter("draw")));
            } catch (NumberFormatException e) {
                dataTablesPojo.setDraw(0);
            }

            final int orderableColumnCount = 4;
            final StringBuffer sbOrder = new StringBuffer();
            LinkedHashMap<String, String> columnMap = new LinkedHashMap<String, String>() {
                {
                    put("0", "paramGroup");
                    put("1", "paramName");
                    put("2", "paramDescription");
                    put("3", "paramValue");
                }
            };

            int offset = 0;
            int limit = 50;
            String orderColumn = "";
            String orderDir = "";
            try {
                offset = Integer.parseInt(request.getParameter("start"));
            } catch (NumberFormatException e) {
            }
            try {
                limit = Integer.parseInt(request.getParameter("length"));
            } catch (NumberFormatException e) {
            }

            for (int i = 0; i < orderableColumnCount; i++) {
                orderColumn = request.getParameter("order[" + i + "][column]");
                if (orderColumn == null || orderColumn.equalsIgnoreCase("")) {
                    break;
                } else {
                    orderColumn = columnMap.get(orderColumn);
                    if (orderColumn == null) {
                        break;
                    }
                    orderDir = request.getParameter("order[" + i + "][dir]");
                    if (orderDir == null || !orderDir.equalsIgnoreCase("desc")) {
                        orderDir = "asc";
                    }
                    sbOrder.append(orderColumn);
                    sbOrder.append("-");
                    sbOrder.append(orderDir);
                    sbOrder.append("+");
                }
            }

            final String keyword = request.getParameter("q");
            final String paramGroup = request.getParameter("a");

            try {
                final int finalOffset = offset;
                final int finalLimit = limit;

                Page<SystemParameter> paramListPojo = paramService.searchParam(keyword, paramGroup, columnMap, finalOffset, finalLimit, orderColumn, orderDir);

                List<String[]> data = new ArrayList<>();


                for (SystemParameter param : paramListPojo) {

                    String url = "";
                    if (!roleAuth.getAuthority().toUpperCase().equals("ADMIN")) {
                        url = getMessage("message.error.role.not.admin");
                    } else {
                        url = url + "<a href=\"#\" data-toggle=\"modal\" data-target=\"#buttonModal\" data-event=\"edit\" "
                                + "data-value=\"" + param.getParamValue() + "\" "
                                + "data-username=\"" + param.getParamName() + "\" "
                                + "data-group=\"" + param.getParamGroup() + "\" "
                                + "data-description=\"" + param.getParamDescription() + "\" "
                                + "data-link=\"" + getServletContext().getContextPath() + WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_PARAM_EDIT_REQUEST.replace("{paramId}", "" + param.getParameterId()) + "\" data-id=\"" + param.getParameterId() 
                                + "\">"
                                		+ "<i class='fa fa-pencil fa-1x'><div class='action' id='action-edit'>edit</div></i></a>";
                    }
                    
                    String value = param.getParamValue();
                    if(param.getParamName().equals(WebGuiConstant.PARAMETER_PASSWORD_REST))
                    	value = "******";

                    data.add(new String[]{
                            escapeHtml(param.getParamGroup()),
                            escapeHtml(param.getParamName()),
                            escapeHtml(param.getParamDescription()),
                            value,
                            url});

                }

                dataTablesPojo.setData(data);
                dataTablesPojo.setRecordsFiltered(paramListPojo.getContent().size());
                dataTablesPojo.setRecordsTotal(Integer.parseInt("" + paramListPojo.getTotalElements()));

            } catch (HttpClientErrorException e) {
                log.error(e.getStatusCode().toString(), e);
                dataTablesPojo.setError(getMessage("message.datatables.error.fetch.user"));
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                dataTablesPojo.setError(getMessage("message.datatables.error.fetch.user"));
            } finally {
                response.getWriter().write(new JsonUtils().toJson(dataTablesPojo));

            }
        } else {
            response.setStatus(HttpServletResponse.SC_FOUND);
        }
    }

    @RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_PARAM_EDIT_MAPPING, method = {RequestMethod.GET})
    public String editValue(@RequestParam(value = "paramId", required = true) final Long paramId, 
    		@RequestParam(value = "paramValue", required = false) String paramValue, 
    		@ModelAttribute("params") SystemParameter param, BindingResult errors, final Model model, 
    		final HttpServletRequest request, final HttpServletResponse response) {

    	System.out.println("param value from UI : " + paramValue);
    	System.out.println("param from UI : " + param.getParamValue());
        try {
            Authentication auth = SecurityContextHolder.getContext()
                    .getAuthentication();
            User user = (User) auth.getPrincipal();

            AuditLog auditLog = new AuditLog();
            auditLog.setActivityType(auditLog.SAVE_OR_UPDATE);
            auditLog.setCreatedBy(user.getUsername());
            auditLog.setCreatedDate(new Date());
            auditLog.setReffNo(new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date()));

            try {
                SystemParameter editParam = paramService.loadParamByParamId(paramId);

                System.out.println("param name : " + editParam.getParamName());
                System.out.println("param value from DB: " + editParam.getParamValue());
                
                System.out.println("param type : " + editParam.getParamType());
                //validasi inputan
	        		/*param_type 1 = numeric
	        		param_type 0 = string		*/

                if (editParam.getParamType() == '1') {
                    if (!paramValue.matches("[0-9]+")) {
                        log.debug(paramValue + " is invalid number");
                        FormValidationUtil.validatingNumber(errors, "paramValue", paramValue, getMessage("label.param.value"));
                        model.addAttribute("errorMessage", getMessage("field.error.param.invalid.value", editParam.getParamName()));
                    }
                }
                
                //cek expiry days
                if (editParam.getParamName().equals("expiryDays")) {
                	System.out.println(editParam.getParamName() + " expiryDays ");
                    if (Long.parseLong(paramValue) < MIN_EXP_DAYS) {
                        System.out.println(paramValue + " less than 90 days");
                        FormValidationUtil.validateMinExpiryDays(errors,  "paramValue", getMessage("label.param.value"));
                        model.addAttribute("errorMessage", getMessage("field.error.min.expiry.days", editParam.getParamName()));
                    }
                }
                //min change hist pwd
                if (editParam.getParamName().equals("maxHistChangePassword")) {
                    if (Long.parseLong(paramValue) < MIN_HIST_CHANGE_PWD) {
                        System.out.println(paramValue + " maxHistChangePassword less than 18 x");
                        FormValidationUtil.validateMinHistPwd(errors,  "paramValue", getMessage("label.param.value"));
                        model.addAttribute("errorMessage", getMessage("field.error.min.hist.change.pwd", editParam.getParamName()));
                    }
                }
                
                //min change hist pwd
              /*  if (editParam.getParamName().contentEquals("maxLengthPass")) {
                    if (Long.parseLong(paramValue) < MIN_LENGTH_PWD) {
                        System.out.println(paramValue + " maxLengthPass less than 8 char");
                        
                    }
                }*/
                
                if (WebGuiConstant.PARAMETER_DEFAULT_PASSWORD.equals(editParam.getParamName())) {
                    SystemParameter paramMaxLengthPass =
                            paramService.getParamByParamName(WebGuiConstant.PARAMETER_MAX_LENGTH_PASSWORD);
                    Short sObj2 = Short.valueOf(paramMaxLengthPass.getParamValue());

                    Short minLength = new Short("8");
                    //System.out.println("max length : "+sObj2);
                    //System.out.println("value : "+paramValue);
                   
                    
                    //validasi min length pwd
                   System.out.println("Validasi  minLength Pwd less than 8 char");
	              // FormValidationUtil.validateMinLengthPwd(errors,  "paramValue", getMessage("label.param.value"));
	               FormValidationUtil.validatingPasswordMinLengthPass(errors, "paramValue", paramValue, getMessage("label.password"), minLength);
	               if(errors.hasErrors()){
	            	   System.out.println("Has Error min validasi length");
	            	   model.addAttribute("errorMessage", getMessage("field.error.min.length.pwd", editParam.getParamName()));
	               }
                    
                    //validasi max length pwd
	               else {
		               System.out.println("Validasi  maxLength Pwd less than " + paramMaxLengthPass.getParamValue());
	                    FormValidationUtil.validatingPasswordMaxLength(errors, "paramValue", paramValue, getMessage("label.password"), sObj2);
	                    if(errors.hasErrors()){
	                    	System.out.println("Has Error max validasi length");
	                        model.addAttribute("errorMessage", getMessage("field.error.param.invalid.value",
	                                editParam.getParamName()) + "Max length password must be " + paramMaxLengthPass.getParamValue());
	                    }
                    }
                }

                if (!errors.hasErrors()) {
                    try {

                        paramService.updateParam(editParam, paramValue);

                        List<SystemParameter> params = paramService.getParameter();
                        List<String> paramGroup = new ArrayList<String>();

                        Iterator iterator = params.iterator();
                        while (iterator.hasNext()) {
                            SystemParameter systemParameter = (SystemParameter) iterator.next();
                            paramGroup.add(systemParameter.getParamGroup());
                        }

                        Set<String> grupParam = new HashSet<String>(paramGroup);

                        model.addAttribute("paramGroup", grupParam);
                        model.addAttribute("okMessage", getMessage("form.ok.edit.param"));

                        //clear cache
                        restClient.clearCache();
                        //end clear cache

                        auditLog.setDescription("Ubah Parameter " + editParam.getParamName() + " Berhasil");
                        try {
                            auditLogService.insertAuditLog(auditLog);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } catch (HttpClientErrorException e) {
                        log.error(e.getStatusCode().toString(), e);
                        auditLog.setDescription("Ubah Parameter " + editParam.getParamName() + " Gagal");
                        try {
                            auditLogService.insertAuditLog(auditLog);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                        model.addAttribute("errorMessage", getMessage("field.error.param.not.found", param.getParamName()));
                        auditLog.setDescription("Ubah Parameter " + editParam.getParamName() + " Gagal");
                        try {
                            auditLogService.insertAuditLog(auditLog);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            } catch (HttpClientErrorException e) {
                log.error(e.getStatusCode().toString(), e);
                auditLog.setDescription("Ubah Parameter " + param.getParamName() + " Gagal");
                try {
                    auditLogService.insertAuditLog(auditLog);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                model.addAttribute("errorMessage", getMessage("field.error.param.not.found", param.getParamName()));
                auditLog.setDescription("Ubah Parameter " + param.getParamName() + " Gagal");
                try {
                    auditLogService.insertAuditLog(auditLog);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

        } catch (UsernameNotFoundException e) {
            log.error(e.getMessage(), e);
            model.addAttribute("errorMessage", getMessage("form.error.edit.param"));
        }
        return getPageContent(LIST_PAGE_NAME);
    }
}