package id.co.telkomsigma.app.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by daniel on 3/31/15.
 */
@Controller
public class LoginController extends GenericController {

	@RequestMapping("/login")
	public String index(final Model model, final HttpServletRequest request, final HttpServletResponse response) {
		// HttpSession session = request.getSession();
		// session.invalidate();
		// new SecurityContextLogoutHandler().logout(request, response,
		// SecurityContextHolder.getContext().getAuthentication()); // concern
		// you

		// get All Role For SpringSecurity Authorization Setting
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		return "content/login";
	}

}