package id.co.telkomsigma.app.controller;

import id.co.telkomsigma.app.constant.WebGuiConstant;
import id.co.telkomsigma.app.controller.GenericController;
import id.co.telkomsigma.app.model.web.security.AuditLog;
import id.co.telkomsigma.app.model.web.user.User;
import id.co.telkomsigma.app.pojo.DataTablesPojo;
import id.co.telkomsigma.app.pojo.MonitoringPojo;
import id.co.telkomsigma.app.service.AuditLogService;
import id.co.telkomsigma.app.util.FormValidationUtil;
import id.co.telkomsigma.app.util.JsonUtils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.apache.commons.lang.StringEscapeUtils.escapeHtml;


/**
 * Created by Dzulfiqar on 11/13/15.
 */
@Controller
public class MonitoringController extends GenericController {
    // ----------------------------------------- LIST PARAMETER SECTION ----------------------------------------- //
    private static final String LIST_PAGE_NAME = "manage/monitoring/list";
    private static final String LIST_COMMAND = "redirect:" + WebGuiConstant.AFTER_LOGIN_PATH_AUDIT_LOG;

    @Autowired
    private AuditLogService auditLogService;

    @Autowired
    private RESTClient restClient;


    @RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_MONITORING_MAPPING, method = RequestMethod.GET)
    public String showListPage(final Model model, final HttpServletRequest request, final HttpServletResponse response) {
        List<MonitoringPojo> monitoringPojo = new ArrayList<MonitoringPojo>();

       	System.out.println("start1 : ");
        model.addAttribute("monitoringPojo", monitoringPojo);

        Authentication auth = SecurityContextHolder.getContext()
                .getAuthentication();
        User user = (User) auth.getPrincipal();

        String date = new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date());

        AuditLog auditLogs = new AuditLog();
        auditLogs.setActivityType(auditLogs.VIEW_PAGE);
        auditLogs.setCreatedBy(user.getUsername());
        auditLogs.setCreatedDate(new Date());
        auditLogs.setDescription("Buka Halaman Monitoring Connection");
        auditLogs.setReffNo(date);

        try {
            auditLogService.insertAuditLog(auditLogs);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        log.debug("Added Audit Log");

        return getPageContent(LIST_PAGE_NAME);
    }

    @RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_MONITORING_GET_LIST_MAPPING, method = RequestMethod.GET)
    public void doSearch(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
    	
    	System.out.println("start2 : ");
    	
        if (isAjaxRequest(request)) {
            setResponseAsJson(response);

            DataTablesPojo dataTablesPojo = new DataTablesPojo();
            try {
                dataTablesPojo.setDraw(Integer.parseInt(request.getParameter("draw")));
            } catch (NumberFormatException e) {
                dataTablesPojo.setDraw(0);
            }

            Enumeration<String> parameterNames = request.getParameterNames();
            String orderBy = null;
            String sortBy = null;
            while (parameterNames.hasMoreElements()) {

                String paramName = parameterNames.nextElement();
                //String paramValues = request.getParameter(paramName);
                if(paramName.contains("dir")) {
                    sortBy = request.getParameter(paramName);
                }else if(paramName.contains("order") && paramName.contains("column")) {
                    orderBy = request.getParameter(paramName);
                    switch (orderBy)
                    {
                        case "0" : orderBy = "connection"; break;
                        case "1" : orderBy = "status"; break;
                        case "2" : orderBy = "lastCheck"; break;
                        default : orderBy = "id";
                    }
                }

            }

            final int orderableColumnCount = 4;
            final StringBuffer sbOrder = new StringBuffer();
            LinkedHashMap<String, String> columnMap = new LinkedHashMap<String, String>() {
                {
                    put("0", "connection");
                    put("1", "status");
                    put("2", "lastCheck");
                    put("3", "action");
                }
            };

            int offset = 0;
            int limit = 50;
            String orderColumn = "";
            String orderDir = "";
            try {
                offset = Integer.parseInt(request.getParameter("start"));
            } catch (NumberFormatException e) {
            }
            try {
                limit = Integer.parseInt(request.getParameter("length"));
            } catch (NumberFormatException e) {
            }

            for (int i = 0; i < orderableColumnCount; i++) {
                orderColumn = request.getParameter("order[" + i + "][column]");
                if (orderColumn == null || orderColumn.equalsIgnoreCase("")) {
                    break;
                } else {
                    orderColumn = columnMap.get(orderColumn);
                    if (orderColumn == null) {
                        break;
                    }
                    orderDir = request.getParameter("order[" + i + "][dir]");
                    if (orderDir == null || !orderDir.equalsIgnoreCase("desc")) {
                        orderDir = "asc";
                    }
                    sbOrder.append(orderColumn);
                    sbOrder.append("-");
                    sbOrder.append(orderDir);
                    sbOrder.append("+");
                }
            }

            Log log = LogFactory.getLog(this.getClass());

            try {
                final int finalOffset = offset;
                final int finalLimit = limit;
                List<String[]> data = new ArrayList<>();
                Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                Integer type = 1;
                //type = 1 ITM
                //type 2 = KSEI

                for (int i = 1; i <= 2; i++) {

                    String connection = "";
                    String status = "Not Connected"; //status connected ambil dari service
                    Boolean cekStatus = false;
                    String action = ""; // buat button

                    if (type == 1) {
                        //ITM
                        //ambil service status ITM
                        connection = "ITM";
                        
                        cekStatus = restClient.checkConnITM();
                        //cekStatus = true;
                        //cek status diset true buat test
                        //cekStatus = true;

                        //cek action
                        //jika ITM akan ada , START, STOP, SIGN ON , SIGN OFF, ECHO
                        //START --> enable jika DISCONNECT
                        //STOP, SIGN ON, SIGN OFF, ECHO --> enable jika status CONNECTED

                        if (cekStatus) {
                            status = "Connected";
                            //button yang ada STOP, SIGN ON, SIGN OFF, ECHO
                            action = action + " <a href=\"" + getServletContext().getContextPath()
                                    + WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_MONITORING_STOP_ITM_REQUEST.replace("{id}",
                                    "" + type)
                                    + "\" class='btn btn-danger'  data-tooltip='true' data-placement='bottom' title='" + getMessage("label.monitoring.stop")
                                    + "'>STOP</a>&nbsp&nbsp";

                            action = action + " <a href=\"" + getServletContext().getContextPath()
                                    + WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_MONITORING_SIGNON_ITM_REQUEST.replace("{id}",
                                    "" + type)
                                    + "\" class='btn btn-primary'  data-tooltip='true' data-placement='bottom' title='" + getMessage("label.monitoring.signon")
                                    + "'>SIGN ON</a>&nbsp&nbsp";

                            action = action + " <a href=\"" + getServletContext().getContextPath()
                                    + WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_MONITORING_SIGNOFF_ITM_REQUEST.replace("{id}",
                                    "" + type)
                                    + "\" class='btn btn-primary'  data-tooltip='true' data-placement='bottom' title='" + getMessage("label.monitoring.signoff")
                                    + "'>SIGN OFF</a>&nbsp&nbsp";

                            action = action + " <a href=\"" + getServletContext().getContextPath()
                                    + WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_MONITORING_ECHO_ITM_REQUEST.replace("{id}",
                                    "" + type)
                                    + "\" class='btn btn-primary'  data-tooltip='true' data-placement='bottom' title='" + getMessage("label.monitoring.echo")
                                    + "'>ECHO</a>&nbsp&nbsp";
                        } else {
                            //button yang ada START
                            action = action + " <a href=\"" + getServletContext().getContextPath()
                                    + WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_MONITORING_START_ITM_REQUEST.replace("{id}",
                                    "" + type)
                                    + "\" class='btn btn-danger'  data-tooltip='true' data-placement='bottom' title='" + getMessage("label.monitoring.start")
                                    + "'>START</a>&nbsp&nbsp";
                        }

                    }

                    if (type == 2) {
                        //KSEI
                        //ambil service status KSEI
                        connection = "KSEI";
                        cekStatus = restClient.checkConnKSEI();
                        if (cekStatus) {
                            status = "Connected";
                            //cek action
                            //jika KSEI hanya ada button START dan STOP
                            //START --> jika status DISCONNECT
                            //STOP --> jika status CONNECTED

                            action = action + " <a href=\"" + getServletContext().getContextPath()
                                    + WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_MONITORING_STOP_KSEI_REQUEST.replace("{id}",
                                    "" + type)
                                    + "\" class='btn btn-danger'  data-tooltip='true' data-placement='bottom' title='" + getMessage("label.monitoring.stop")
                                    + "'>STOP</a>&nbsp&nbsp";

                        } else {
                            action = action + " <a href=\"" + getServletContext().getContextPath()
                                    + WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_MONITORING_START_KSEI_REQUEST.replace("{id}",
                                    "" + type)
                                    + "\" class='btn btn-danger'  data-tooltip='true' data-placement='bottom' title='" + getMessage("label.monitoring.start")
                                    + "'>START</a>&nbsp&nbsp";
                        }
                    }
                    type++;

                    System.out.println("Status : " + status);

                    data.add(new String[]{
                            escapeHtml(connection),
                            escapeHtml(status),
                            escapeHtml(formatter.format(new Date()).toString()),
                            action
                    });
                }

                dataTablesPojo.setData(data);
                dataTablesPojo.setRecordsFiltered(2);
                dataTablesPojo.setRecordsTotal(2);

            } catch (HttpClientErrorException e) {
                log.error(e.getStatusCode().toString(), e);
                dataTablesPojo.setError(getMessage("message.datatables.error.fetch.user"));
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                dataTablesPojo.setError(getMessage("message.datatables.error.fetch.user"));
            } finally {
                response.getWriter().write(new JsonUtils().toJson(dataTablesPojo));

            }
        } else {
            response.setStatus(HttpServletResponse.SC_FOUND);
        }
    }


    @RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_MONITORING_START_ITM_MAPPING, method = {RequestMethod.GET})
    public String startConITM(
            @ModelAttribute("id") Integer id, BindingResult errors, final Model model,
            final HttpServletRequest request, final HttpServletResponse response) {

        // audit log
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User admin = (User) auth.getPrincipal();

        AuditLog auditLog = new AuditLog();
        auditLog.setActivityType(auditLog.SAVE_OR_UPDATE);
        auditLog.setCreatedBy(admin.getUsername());
        auditLog.setCreatedDate(new Date());
        auditLog.setReffNo(new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date()));

        try {
            System.out.println("Start Connection ITM : ");

            Boolean resp = false;
            resp = restClient.startITM();

            if (resp)
                model.addAttribute("okMessage", getMessage("field.monitoring.start.itm.ok"));
            else
                model.addAttribute("errorMessage", getMessage("field.monitoring.start.itm.error"));


        } catch (Exception ex) {
            ex.printStackTrace();
            model.addAttribute("errorMessage", getMessage("field.monitoring.start.itm.error"));
        }
        return getPageContent(LIST_PAGE_NAME);
    }

    @RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_MONITORING_STOP_ITM_MAPPING, method = {RequestMethod.GET})
    public String stopConITM(
            @ModelAttribute("id") Integer id, BindingResult errors, final Model model,
            final HttpServletRequest request, final HttpServletResponse response) {

        // audit log
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User admin = (User) auth.getPrincipal();

        AuditLog auditLog = new AuditLog();
        auditLog.setActivityType(auditLog.SAVE_OR_UPDATE);
        auditLog.setCreatedBy(admin.getUsername());
        auditLog.setCreatedDate(new Date());
        auditLog.setReffNo(new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date()));

        try {

            System.out.println("Stop Connection ITM : ");
            Boolean resp = false;
            resp = restClient.stopITM();

            if (resp)
                model.addAttribute("okMessage", getMessage("field.monitoring.stop.itm.ok"));
            else
                model.addAttribute("errorMessage", getMessage("field.monitoring.stop.itm.error"));


        } catch (Exception ex) {
            ex.printStackTrace();
            model.addAttribute("errorMessage", getMessage("field.monitoring.stop.itm.error"));
        }
        return getPageContent(LIST_PAGE_NAME);
    }


    @RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_MONITORING_SIGNON_ITM_MAPPING, method = {RequestMethod.GET})
    public String signOn(
            @ModelAttribute("id") Integer id, BindingResult errors, final Model model,
            final HttpServletRequest request, final HttpServletResponse response) {

        // audit log
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User admin = (User) auth.getPrincipal();

        AuditLog auditLog = new AuditLog();
        auditLog.setActivityType(auditLog.SAVE_OR_UPDATE);
        auditLog.setCreatedBy(admin.getUsername());
        auditLog.setCreatedDate(new Date());
        auditLog.setReffNo(new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date()));

        try {

            System.out.println("Send Sign On ITM : ");
            Boolean resp = false;
            resp = restClient.signon();

            if (resp)
                model.addAttribute("okMessage", getMessage("field.monitoring.signon.itm.ok"));
            else
                model.addAttribute("errorMessage", getMessage("field.monitoring.signon.itm.error"));


        } catch (Exception ex) {
            ex.printStackTrace();
            model.addAttribute("errorMessage", getMessage("field.monitoring.signon.itm.error"));
        }
        return getPageContent(LIST_PAGE_NAME);
    }

    @RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_MONITORING_SIGNOFF_ITM_MAPPING, method = {RequestMethod.GET})
    public String signOff(
            @ModelAttribute("id") Integer id, BindingResult errors, final Model model,
            final HttpServletRequest request, final HttpServletResponse response) {

        // audit log
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User admin = (User) auth.getPrincipal();

        AuditLog auditLog = new AuditLog();
        auditLog.setActivityType(auditLog.SAVE_OR_UPDATE);
        auditLog.setCreatedBy(admin.getUsername());
        auditLog.setCreatedDate(new Date());
        auditLog.setReffNo(new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date()));

        try {

            System.out.println("Send Sign Off ITM : ");
            Boolean resp = false;
            resp = restClient.signoff();

            if (resp)
                model.addAttribute("okMessage", getMessage("field.monitoring.signoff.itm.ok"));
            else
                model.addAttribute("errorMessage", getMessage("field.monitoring.signoff.itm.error"));


        } catch (Exception ex) {
            ex.printStackTrace();
            model.addAttribute("errorMessage", getMessage("field.monitoring.signoff.itm.error"));
        }
        return getPageContent(LIST_PAGE_NAME);
    }

    @RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_MONITORING_ECHO_ITM_MAPPING, method = {RequestMethod.GET})
    public String echo(
            @ModelAttribute("id") Integer id, BindingResult errors, final Model model,
            final HttpServletRequest request, final HttpServletResponse response) {

        // audit log
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User admin = (User) auth.getPrincipal();

        AuditLog auditLog = new AuditLog();
        auditLog.setActivityType(auditLog.SAVE_OR_UPDATE);
        auditLog.setCreatedBy(admin.getUsername());
        auditLog.setCreatedDate(new Date());
        auditLog.setReffNo(new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date()));

        try {

            System.out.println("Send ECHO ITM : ");
            Boolean resp = false;
            resp = restClient.echo();

            if (resp)
                model.addAttribute("okMessage", getMessage("field.monitoring.echo.itm.ok"));
            else
                model.addAttribute("errorMessage", getMessage("field.monitoring.echo.itm.error"));


        } catch (Exception ex) {
            ex.printStackTrace();
            model.addAttribute("errorMessage", getMessage("field.monitoring.echo.itm.error"));
        }
        return getPageContent(LIST_PAGE_NAME);
    }
    
    /*
    @RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_MONITORING_START_KSEI_MAPPING, method = { RequestMethod.GET })
   	public String startKsei(
   			@ModelAttribute("id") Integer id, BindingResult errors, final Model model,
   			final HttpServletRequest request, final HttpServletResponse response) {

   		// audit log
   		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
   		User admin = (User) auth.getPrincipal();

   		AuditLog auditLog = new AuditLog();
   		auditLog.setActivityType(auditLog.SAVE_OR_UPDATE);
   		auditLog.setCreatedBy(admin.getUsername());
   		auditLog.setCreatedDate(new Date());
   		auditLog.setReffNo(new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date()));

   		try {
   			
   			System.out.println("Send START KSEI : " );
   			Boolean resp = false;
   			resp = restClient.();
   	
   			if(resp)
   				model.addAttribute("okMessage", getMessage("field.monitoring.start.ksei.ok"));
   			else
   				model.addAttribute("errorMessage", getMessage("field.monitoring.echo.itm.error"));

   							
   		} catch (Exception ex) {
   				ex.printStackTrace();
   				model.addAttribute("errorMessage", getMessage("field.monitoring.echo.itm.error"));
   		}
   		return getPageContent(LIST_PAGE_NAME);
   	}
    */

}