
package id.co.telkomsigma.app.logs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.util.HtmlUtils;

import id.co.telkomsigma.app.constant.WebGuiConstant;
import id.co.telkomsigma.app.controller.GenericController;
import id.co.telkomsigma.app.controller.RESTClient;
import id.co.telkomsigma.app.model.web.logs.MessageLogs;
import id.co.telkomsigma.app.model.web.logs.TransactionLogs;
import id.co.telkomsigma.app.model.web.parameter.SystemParameter;
import id.co.telkomsigma.app.model.web.security.AuditLog;
import id.co.telkomsigma.app.model.web.user.User;
import id.co.telkomsigma.app.pojo.DataTablesPojo;
import id.co.telkomsigma.app.service.LogService;
import id.co.telkomsigma.app.service.TransactionLogsService;
import id.co.telkomsigma.app.util.DateUtil;
import id.co.telkomsigma.app.util.FormValidationUtil;
import id.co.telkomsigma.app.util.JsonUtils;

import static org.apache.commons.lang.StringEscapeUtils.escapeHtml;

//controller ini sebagai controller Transaction Logs dan COntroller Message Logs
@Controller
public class MessageLogsController extends GenericController {

	@Autowired // autowired ke message logs service
	private LogService logService;

	@Autowired
	private TransactionLogsService trxLogService;

	@Autowired
	private RESTClient restClient;

	@Autowired
	JsonUtils jsonUtils;

	private static final String LIST_PAGE_NAME = "log/messagelogs/list"; // ambil list transaction logs
	private static final String LIST_COMMAND = "redirect:" + WebGuiConstant.AFTER_LOGIN_PATH_LOG;

	/**
	 * Method ambil semua TRANSACTION LOGS show list Page
	 **/
	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_LOG_MAPPING, method = RequestMethod.GET)
	public String showListPage(final Model model, final HttpServletRequest request, final HttpServletResponse response,
			@RequestParam(value = "dataSearch", required = false) String dataSearch) {

		// ambil diawal pake limit dulu
		int first = 0;
		int max = 50;

		System.out.println("dataSearch " + dataSearch);
		List<TransactionLogs> trxLogs = trxLogService.getTransactionLogs(first, max);

		model.addAttribute("trxLogs", trxLogs);
		model.addAttribute("dataSearch", dataSearch);

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();

		String date = new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date());

		AuditLog auditLogs = new AuditLog();
		auditLogs.setActivityType(auditLogs.VIEW_PAGE);
		auditLogs.setCreatedBy(user.getUsername());
		auditLogs.setCreatedDate(new Date());
		auditLogs.setDescription("Buka Halaman Log");
		auditLogs.setReffNo(date);

		try {
			auditLogService.insertAuditLog(auditLogs);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		log.debug("Added Audit Log");
		
		Enumeration<String> parameterNames = request.getParameterNames();
		while (parameterNames.hasMoreElements()) {
			String paramName = parameterNames.nextElement();
			String paramValues = request.getParameter(paramName);
			
			
		}


		return getPageContent(LIST_PAGE_NAME);
	}

	/**
	 * Method ambil semua TRANSACTION LOGS show list Page BASED ON FILTER
	 **/
	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_LOG_GET_LIST_MAPPING, method = RequestMethod.GET)
	public void doSearch(final HttpServletRequest request, final HttpServletResponse response) throws IOException {

		Enumeration<String> parameterNames = request.getParameterNames();
		String orderBy = null;
		String sortBy = null;
		
		while (parameterNames.hasMoreElements()) {
			//System.out.println("parameterNames : " + parameterNames);
			String paramName = parameterNames.nextElement();
			String paramValues = request.getParameter(paramName);
			
			System.out.println("paramname : " + paramName + " paramValues : " + paramValues);
			
			if ((paramName.contains("draw")) && (paramValues.equals("1"))) {
				System.out.println("Default Pencarian , order by Created Date DESC ");
				orderBy = "createdDate";
				sortBy = "desc";
				break;
			}
			else if (paramName.contains("dir")) {
				
				sortBy = request.getParameter(paramName);
				System.out.println("paramValue : " + paramValues);
				System.out.println("sort by : " + sortBy);
				
			} else if (paramName.contains("order") && paramName.contains("column")) {
				orderBy = request.getParameter(paramName);
				//System.out.println("order by : " + orderBy);
								
				switch (orderBy) {
				case "0":
					orderBy = "createdDate";
					//	System.out.println("order by createdDate : " + orderBy + " sortBy : " + sortBy);
					break;
				case "1":
					orderBy = "id";
				//	System.out.println("order by id : " + orderBy);
					break;
				case "2":
					orderBy = "transactionType";
				//	System.out.println("order by transactionType : " + orderBy);
					break;
				case "3":
					orderBy = "originator";
				//	System.out.println("order by originator : " + orderBy);
					break;
				case "4":
					orderBy = "destination";
				//	System.out.println("order by destination : " + orderBy);
					break;
				case "5":
					orderBy = "cashValue";
				//	System.out.println("order by cashValue : " + orderBy);
					break;
				case "6":
					orderBy = "curCod";
				//	System.out.println("order by curCod : " + orderBy);
					break;
				case "7":
					orderBy = "extRef";
				//	System.out.println("order by extRef : " + orderBy);
					break;
				default:
					orderBy = "createdDate";
				}
			}
		}
		
		
		if (isAjaxRequest(request)) {
			setResponseAsJson(response);

			DataTablesPojo dataTablesPojo = new DataTablesPojo();
			try {
				dataTablesPojo.setDraw(Integer.parseInt(request.getParameter("draw")));
			} catch (NumberFormatException e) {
				dataTablesPojo.setDraw(0);
			}

			final int orderableColumnCount = 10;
			final StringBuffer sbOrder = new StringBuffer();
			LinkedHashMap<String, String> columnMap = new LinkedHashMap<String, String>() {
				{
					put("0", "createdDate");
					put("1", "id");
					put("2", "transactionType");
					put("3", "originator");
					put("4", "destination");
					put("5", "cashValue");
					put("6", "curCod");
					put("7", "extRef");
				}
			};

			int offset = 0;
			int limit = 50;
			try {
				offset = Integer.parseInt(request.getParameter("start"));
			} catch (NumberFormatException e) {
			}
			try {
				limit = Integer.parseInt(request.getParameter("length"));
			} catch (NumberFormatException e) {
			}

			for (int i = 0; i < orderableColumnCount; i++) {
				String orderColumn = request.getParameter("order[" + i + "][column]");
				if (orderColumn == null || orderColumn.equalsIgnoreCase("")) {
					break;
				} else {
					orderColumn = columnMap.get(orderColumn);
					if (orderColumn == null) {
						break;
					}
					String orderDir = request.getParameter("order[" + i + "][dir]");
					/*
					 * if (orderDir == null || !orderDir.equalsIgnoreCase("desc")) { orderDir =
					 * "asc"; }
					 */
					if (orderDir == null)
						orderDir = "desc";
					
					sbOrder.append(orderColumn);
					sbOrder.append("-");
					sbOrder.append(orderDir);
					sbOrder.append("+");
				}
			}

			Log log = LogFactory.getLog(this.getClass());
			String keyword = request.getParameter("q");
			// log.debug("keyword " + keyword);

			String trxName = request.getParameter("a");
			String sender = request.getParameter("e");
			String destination = request.getParameter("g");
			String rangeDate = request.getParameter("d");
			Map<String, Object> parameterList = new HashMap<String, Object>();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			SimpleDateFormat formatter2 = new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat formatter3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

			String startDate = null;
			String endDate = null;
			
			Date std = new Date();
			Date ed = new Date();
			
			/*
			Date date = new Date();  
            Timestamp ts=new Timestamp(date.getTime());  
           
            Timestamp std = new Timestamp(date.getTime());  
            Timestamp ed = new Timestamp(date.getTime());  
            
            System.out.println(ts);     
             */
			
			System.out.println("keyword : " + keyword);
			System.out.println("trxName : " + trxName);
			System.out.println("rangeDate : " + rangeDate);
			System.out.println("sender : " + sender);
			System.out.println("destination : " + destination);

			if (rangeDate == "")
				rangeDate = null;

			if (rangeDate != null) {
				String split[] = rangeDate.split("-");
				startDate = split[0].trim();
				endDate = split[1].trim();
				
			     
				try {
					
					std = formatter2.parse(startDate);  
					ed = DateUtil.addDays(formatter2.parse(endDate), 1);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				System.out.println("startDate : " + startDate + " std : " + std);
				System.out.println("endDate : " + endDate + " ed : " + ed);
			}

			if (keyword != null && keyword.trim().length() > 0) {
				keyword = "%" + keyword + "%";
				parameterList.put("keyword", keyword);
			}

			if (trxName != null && trxName.trim().length() > 0) {
				//trxName = "%" + trxName + "%";
				trxName =  trxName.toUpperCase();
				parameterList.put("trxName", trxName);
			}

			if (startDate != null && endDate != null) {
				parameterList.put("std", std);
				parameterList.put("ed", ed);
			}

			if (sender != null && sender.trim().length() > 0) {
				//sender = "%" + sender + "%";
				sender = sender.toUpperCase();
				parameterList.put("sender", sender);
			}

			if (destination != null && destination.trim().length() > 0) {
				//destination = "%" + destination + "%";
				destination = destination.toUpperCase();
				parameterList.put("destination", destination);
			}

			try {
				final int finalOffset = offset;
				final int finalLimit = limit;

				/*
				Page<TransactionLogs> trxLogsList = trxLogService.searchTransactionLogs(parameterList, finalOffset,
						finalLimit);
				*/
				/*
				 * Map<String,Object> parameterList, LinkedHashMap<String, String> sbOrder, int
				 * finalOffset, int finalLimit, String orderBy, String sortBy
				 */
				Page<TransactionLogs> trxLogsList = trxLogService.searchTransactionLogs(parameterList, columnMap, finalOffset,
						finalLimit, orderBy, sortBy);
				
				System.out.println("==jumlah record : " + trxLogsList.getTotalElements());

				List<String[]> data = new ArrayList<>();
				String resend = "RESEND";
				String detail = "VIEW DETAIL";

				for (TransactionLogs trx : trxLogsList) {

					/*
					 * String flow = "OUTGOING"; if (message.getIsRequest()) flow = "INCOMING";
					 */

					data.add(new String[] { 
							escapeHtml(formatter3.format(trx.getCreatedDate())),
							escapeHtml(trx.getId().toString()), 
							escapeHtml(trx.getTransactionType()),
							escapeHtml(trx.getOriginator()), 
							escapeHtml(trx.getDestination()),
							escapeHtml(trx.getCashValue().toString()), 
							escapeHtml(trx.getCurCod()),
							escapeHtml(trx.getExtRef().toString()), 
							"<a href=\"" + getServletContext().getContextPath()
									+ WebGuiConstant.AFTER_LOGIN_PATH_LOG_DETAIL_REQUEST.replace("{trxId}",
											"" + trx.getId())
									+ "\">"
									+ "<i class='fa fa-eye'><div class='action' id='action-eye'>view detail</div></i></a>", });
				}

				dataTablesPojo.setData(data);
				dataTablesPojo.setRecordsFiltered(trxLogsList.getTotalElements());
				dataTablesPojo.setRecordsTotal(trxLogsList.getTotalElements());

			} catch (HttpClientErrorException e) {
				log.error(e.getStatusCode().toString(), e);
				dataTablesPojo.setError(getMessage("message.datatables.error.fetch.log.message"));
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				dataTablesPojo.setError(getMessage("message.datatables.error.fetch.log.message"));
			} finally {
				response.getWriter().write(new JsonUtils().toJson(dataTablesPojo));

			}
		} else {
			response.setStatus(HttpServletResponse.SC_FOUND);
		}
	}

	private static final String DETAILS_PAGE_NAME = "log/messagelogs/details/list"; // get message logs, based on
																					// transaction logs id

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_LOG_DETAIL_MAPPING, method = { RequestMethod.GET })
	public String showDetail(final HttpServletRequest request, final Model model, final HttpServletResponse response,
			@RequestParam(value = "trxId", required = true) String trxId,
			@RequestParam(value = "dataSearch", required = false) String dataSearch) {

		model.addAttribute("trxId", trxId);
		model.addAttribute("dataSearch", dataSearch);

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();

		String date = new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date());

		AuditLog auditLogs = new AuditLog();
		auditLogs.setActivityType(auditLogs.VIEW_PAGE);
		auditLogs.setCreatedBy(user.getUsername());
		auditLogs.setCreatedDate(new Date());
		auditLogs.setDescription("Buka Halaman Log");
		auditLogs.setReffNo(date);

		try {
			auditLogService.insertAuditLog(auditLogs);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		log.debug("Added Audit Log");

		return getPageContent(DETAILS_PAGE_NAME);
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_LOG_GET_LIST_DETAILS_BY_GROUP_ID_MAPPING, method = RequestMethod.GET)
	public void doSearchDetails(final HttpServletRequest request, final Model model, final HttpServletResponse response)
			throws IOException {

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		if (isAjaxRequest(request)) {
			setResponseAsJson(response);

			DataTablesPojo dataTablesPojo = new DataTablesPojo();
			try {
				dataTablesPojo.setDraw(Integer.parseInt(request.getParameter("draw")));
			} catch (NumberFormatException e) {
				dataTablesPojo.setDraw(0);
			}

			final int orderableColumnCount = 2;
			final StringBuffer sbOrder = new StringBuffer();
			LinkedHashMap<String, String> columnMap = new LinkedHashMap<String, String>() {
				{
					put("0", "raw");
				}
			};

			int offset = 0;
			int limit = 50;
			try {
				offset = Integer.parseInt(request.getParameter("start"));
			} catch (NumberFormatException e) {
			}
			try {
				limit = Integer.parseInt(request.getParameter("length"));
			} catch (NumberFormatException e) {
			}

			for (int i = 0; i < orderableColumnCount; i++) {
				String orderColumn = request.getParameter("order[" + i + "][column]");
				if (orderColumn == null || orderColumn.equalsIgnoreCase("")) {
					break;
				} else {
					orderColumn = columnMap.get(orderColumn);
					if (orderColumn == null) {
						break;
					}
					String orderDir = request.getParameter("order[" + i + "][dir]");
					if (orderDir == null || !orderDir.equalsIgnoreCase("desc")) {
						orderDir = "asc";
					}
					sbOrder.append(orderColumn);
					sbOrder.append("-");
					sbOrder.append(orderDir);
					sbOrder.append("+");
				}
			}
			String trxId = request.getParameter("trxId");

			try {
				final int finalOffset = offset;
				final int finalLimit = limit;

				System.out.println("trxId id : " + trxId);

				Page<MessageLogs> messageLogsList = logService.getMessageLogsByTrxId(trxId, finalOffset, finalLimit);

				List<String[]> data = new ArrayList<>();

				
				//DateFormat outputformat = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
				for (MessageLogs message : messageLogsList) {
					
					/*
					 * System.out.println("created date : " + message.getCreatedDate()); String
					 * output = ""; output = outputformat.format( message.getCreatedDate());
					 * System.out.println("output 24H : " + output);
					 */
					

					String req = "";
					if (message.getTrxType().equalsIgnoreCase("ACK-CNF") || message.getTrxType().equalsIgnoreCase("PAYORD"))
						req = "INCOMING";
					else 
						req = "OUTGOING";
					

					// formatted message :
					String formatMsg = "";
					String frmtMsg = "";
					StringBuilder result = new StringBuilder();

					result.append(System.getProperty("line.separator"));
					
					Map<String, Object> maps = jsonUtils.jsonToMapOfObject(message.getFormattedData());
					// loop maps
					for (Map.Entry<String, Object> formatDt : maps.entrySet()) {
						System.out.println("Key : " + formatDt.getKey() + " Value : " + formatDt.getValue());
						formatMsg = " " + formatDt.getKey() + " : " + formatDt.getValue() + " ";
						result.append(formatMsg);
						result.append("\n");
						System.out.println(" ======= result : " + result);
						frmtMsg = result.toString();
						System.out.println(" ======= fmtr : " + frmtMsg.toString());
					}

					// cek pengecekan message yang boleh diresend
					String url = "";
					if (message.getOriginator().equalsIgnoreCase("PB") && message.getIsRequest()) {

						url = url
								+ "<a href=\"#\" data-toggle=\"modal\" data-target=\"#buttonModal\" data-event=\"resend\" "
								+ "data-id=\"" + message.getId().toString() + "\" " + "data-sender=\""
								+ message.getOriginator() + "\" " + "data-destination=\"" + message.getDestination()
								+ "\" " + "data-value=\"" + frmtMsg + "\" " + "data-link=\""
								+ getServletContext().getContextPath()
								+ WebGuiConstant.AFTER_LOGIN_PATH_LOG_DETAIL_RESEND_REQUEST.replace("{messageId}",
										"" + message.getId().toString())
								+ "\" data-id=\"" + message.getId().toString()
								+ "\"><i class='fa fa-pencil fa-1x'><div class='action' id='action-resend'>resend</div></i></a>";
					} else {
						url = url
								+ "<a href=\"#\" data-toggle=\"modal\" data-target=\"#buttonModal\" data-event=\"view\" "
								+ "data-id=\"" + message.getId().toString() + "\" " + "data-sender=\""
								+ message.getOriginator() + "\" " + "data-destination=\"" + message.getDestination()
								+ "\" " + "data-value=\"" + frmtMsg + "\" " + "data-link=\""
								+ getServletContext().getContextPath()
								+ WebGuiConstant.AFTER_LOGIN_PATH_LOG_DETAIL_RESEND_REQUEST.replace("{messageId}",
										"" + message.getId().toString())
								+ "\" data-id=\"" + message.getId().toString()
								+ "\"><i class='fa fa-pencil fa-1x'><div class='action' id='action-view'>view</div></i></a>";
					}

					data.add(new String[] { escapeHtml(message.getTransactionId().toString()),
							escapeHtml(message.getId().toString()),
							escapeHtml(formatter.format(message.getCreatedDate())), 
							escapeHtml(message.getTrxType()),
							escapeHtml(message.getOriginator()), escapeHtml(message.getDestination()), escapeHtml(req),
							// frmtMsg,
							url });
				}

				dataTablesPojo.setData(data);

			} catch (HttpClientErrorException e) {
				log.error(e.getStatusCode().toString(), e);
				dataTablesPojo.setError(getMessage("message.datatables.error.fetch.messageLogs"));
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				dataTablesPojo.setError(getMessage("message.datatables.error.fetch.messageLogs"));
			} finally {
				response.getWriter().write(new JsonUtils().toJson(dataTablesPojo));
			}
		} else {
			response.setStatus(HttpServletResponse.SC_FOUND);
		}

	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_LOG_DETAIL_RESEND_MAPPING, method = {RequestMethod.GET})
    public String doResend (@ModelAttribute("messageId") final String messageId,
    		BindingResult errors, 
    		final Model model, 
    		final HttpServletRequest request, final HttpServletResponse response) {
    	
    	try{
    	
    		System.out.println("test do Resend yaaaa :" + messageId);
    		
	    	Authentication auth = SecurityContextHolder.getContext()
					.getAuthentication();
			User user = (User) auth.getPrincipal();
			
			
	        AuditLog auditLog = new AuditLog();
	        auditLog.setActivityType(auditLog.SAVE_OR_UPDATE);
	        auditLog.setCreatedBy(user.getUsername());
	        auditLog.setCreatedDate(new Date());
	        auditLog.setReffNo(new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date()));
	        
	        Boolean responseRest =  false;
	       
	        HashMap<String,String> map = new HashMap<String, String>();
	        map.put("messageId", messageId);
	        System.out.println("messageId === " + jsonUtils.toJson(map));
	                
	        responseRest= restClient.doResend(jsonUtils.toJson(map));
	        
	        if(responseRest)
	        	model.addAttribute("okMessage", getMessage("form.ok.resend.message.detail", messageId));
	        else 
	        	model.addAttribute("errorMessage", getMessage("form.error.resend.message.detail", messageId));
        
    	}
    	catch(Exception e) {
    		e.printStackTrace();
    		model.addAttribute("errorMessage", getMessage("form.error.resend.message.detail", messageId));
    	}
    	
    	return getPageContent(LIST_PAGE_NAME);
    }
	
	
	
}
