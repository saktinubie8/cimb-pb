package id.co.telkomsigma.app.simulator;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.HttpClientErrorException;

import id.co.telkomsigma.app.constant.WebGuiConstant;
import id.co.telkomsigma.app.controller.GenericController;
import id.co.telkomsigma.app.controller.RESTClient;
import id.co.telkomsigma.app.model.web.security.AuditLog;
import id.co.telkomsigma.app.model.web.user.User;
import id.co.telkomsigma.app.pojo.DrefReqPojo;
import id.co.telkomsigma.app.service.DrefService;
import id.co.telkomsigma.app.service.KseiWebService;
import id.co.telkomsigma.app.util.FormValidationUtil;

@Controller
public class DrefController extends GenericController { 
	//controller untuk injector manual dref
	
	@Autowired
	private DrefService drefService ;
	
	@Autowired
	private RESTClient restClient ;

	private static final String SEND_PAGE_NAME = "simulator/dref/send";
      
	  @RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_DREF_MAPPING, method = RequestMethod.GET)
	  public String showSendPage(final Model model,
					final HttpServletRequest request, final HttpServletResponse response) {
		
		  
		  DrefReqPojo dref = new DrefReqPojo();
		  dref.setValDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
		  dref.setExtRef(drefService.generateExtRef());
		  
		  System.out.println("dref exref  " + dref.getExtRef());
		  
		model.addAttribute("dref",dref);
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		
		String date = new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date());
     	
     	AuditLog auditLogs = new AuditLog();
     	auditLogs.setActivityType(auditLogs.MANUAL_INJECTOR);
     	auditLogs.setCreatedBy(user.getUsername());
     	auditLogs.setCreatedDate(new Date());
     	auditLogs.setDescription("Buka Halaman Manual Injector DREF");
     	auditLogs.setReffNo(date);
        
        try{
        	auditLogService.insertAuditLog(auditLogs);
        }
        catch(Exception ex){
        	ex.printStackTrace();
        }
        log.debug("Added Audit Log");
     	
    	return getPageContent(SEND_PAGE_NAME);
    }
	
	
	  @RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_DREF_MAPPING, method = RequestMethod.POST)
	    public String doSend(@ModelAttribute("dref") DrefReqPojo dref, 
	    		BindingResult errors, final Model model, 
	    		final HttpServletRequest request, final HttpServletResponse response) {	  
	
		try {
			
			 System.out.println("doSend Message DREF ");
			 System.out.println("DREF exref " + dref.getExtRef() );
			 System.out.println("DREF :  " + dref.toString());
			 
			 Boolean responseRest = false;
			
			FormValidationUtil.validatingRequired(errors, "memAC", dref.getMemAC(), getMessage("label.simulator.dref.memac"));
			FormValidationUtil.validatingRequired(errors, "extRef", dref.getExtRef(), getMessage("label.simulator.dref.extref"));
			FormValidationUtil.validatingRequired(errors, "relRef", dref.getRelRef(), getMessage("label.simulator.dref.relref"));
			FormValidationUtil.validatingRequired(errors, "curCod", dref.getCurCod(), getMessage("label.simulator.dref.curcod"));
			FormValidationUtil.validatingRequired(errors, "benAC", dref.getBenAC(), getMessage("label.simulator.dref.benac"));
			FormValidationUtil.validatingRequired(errors, "valDate", dref.getValDate(), getMessage("label.simulator.dref.valdate"));
			FormValidationUtil.validatingRequired(errors, "cashVal", dref.getCashVal(), getMessage("label.simulator.dref.cashval"));
			//notes optional
			//memid optional
			
			//if (!errors.hasErrors()) {
				Authentication auth = SecurityContextHolder.getContext().getAuthentication();
				User user = (User) auth.getPrincipal();

				String date = new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date());

				AuditLog auditLogs = new AuditLog();
				auditLogs.setActivityType(auditLogs.MANUAL_INJECTOR);
				auditLogs.setDescription("Kirim DREF Injector");
				auditLogs.setCreatedBy(user.getUsername());
				auditLogs.setCreatedDate(new Date());
				auditLogs.setReffNo(date);
				
				auditLogService.insertAuditLog(auditLogs);
				log.debug("Added Audit Log");
				
				responseRest = restClient.sendDref(dref);
				
				if (responseRest)
					model.addAttribute("okMessage", getMessage("form.ok.simulator.send.dref" , dref.getExtRef()));
				else 
					model.addAttribute("errorMessage", getMessage("form.error.simulator.send.dref" , dref.getExtRef()));
		//	}
		} catch (Exception ex) {
			log.error(ex);
			model.addAttribute("errorMessage", getMessage("form.error.simulator.send.dref", dref.getExtRef()));
		}

		return getPageContent(SEND_PAGE_NAME);
	    }
}
