package id.co.telkomsigma.app.simulator;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import id.co.telkomsigma.app.constant.WebGuiConstant;
import id.co.telkomsigma.app.controller.GenericController;
import id.co.telkomsigma.app.controller.RESTClient;
import id.co.telkomsigma.app.model.web.security.AuditLog;
import id.co.telkomsigma.app.model.web.user.User;
import id.co.telkomsigma.app.pojo.HeaderReqPojo;
import id.co.telkomsigma.app.pojo.PayordReqPojo;
import id.co.telkomsigma.app.service.PayOrdService;
import id.co.telkomsigma.app.util.FormValidationUtil;

@Controller("payordController")
public class PayOrdController extends GenericController { 
	
	@Autowired
	private PayOrdService payordService ;
	
	@Autowired
	private RESTClient restClient ;


	private static final String SEND_PAGE_NAME = "simulator/payord/send";
      
	  @RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_PAYORD_MAPPING, method = RequestMethod.GET)
	  public String showSendPage(final Model model,
					final HttpServletRequest request, final HttpServletResponse response) {
		
		  
		  PayordReqPojo payord = new PayordReqPojo();
		  payord.setValDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
		  payord.setExtRef(payordService.generateExtRef());
		  
		  System.out.println("payord exref  " + payord.getExtRef());
		  
		model.addAttribute("payord",payord);
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		
		String date = new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date());
     	
     	AuditLog auditLogs = new AuditLog();
     	auditLogs.setActivityType(auditLogs.MANUAL_INJECTOR);
     	auditLogs.setCreatedBy(user.getUsername());
     	auditLogs.setCreatedDate(new Date());
     	auditLogs.setDescription("Buka Halaman Manual Injector PAYORD");
     	auditLogs.setReffNo(date);
        
        try{
        	auditLogService.insertAuditLog(auditLogs);
        }
        catch(Exception ex){
        	ex.printStackTrace();
        }
        log.debug("Added Audit Log");
     	
    	return getPageContent(SEND_PAGE_NAME);
    }
	
	
	  @RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_PAYORD_MAPPING, method = RequestMethod.POST)
	    public String doSend(@ModelAttribute("payord") PayordReqPojo payord, 
	    		BindingResult errors, final Model model, 
	    		final HttpServletRequest request, final HttpServletResponse response) {	  
	
		try {
			
			 System.out.println("doSend Message Payord ");
			 System.out.println("payord exref " + payord.getExtRef() );
			 System.out.println("payord " + payord.toString() );
			
			FormValidationUtil.validatingRequired(errors, "memAC", payord.getMemAC(), getMessage("label.simulator.payord.memac"));
			FormValidationUtil.validatingRequired(errors, "extRef", payord.getExtRef(), getMessage("label.simulator.payord.extref"));
			FormValidationUtil.validatingRequired(errors, "curCod", payord.getCurCod(), getMessage("label.simulator.payord.curcod"));
			FormValidationUtil.validatingRequired(errors, "benAC", payord.getBenAC(), getMessage("label.simulator.payord.benac"));
			FormValidationUtil.validatingRequired(errors, "valDate", payord.getExtRef(), getMessage("label.simulator.payord.valdate"));
			FormValidationUtil.validatingRequired(errors, "cashVal", payord.getCashVal(), getMessage("label.simulator.payord.cashval"));
			//note optional
			//memid optional
			
			Boolean responseRest = false;
			
			
		//	if (!errors.hasErrors()) {
				Authentication auth = SecurityContextHolder.getContext().getAuthentication();
				User user = (User) auth.getPrincipal();

				String date = new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date());

				AuditLog auditLogs = new AuditLog();
				auditLogs.setActivityType(auditLogs.MANUAL_INJECTOR);
				auditLogs.setDescription("Kirim PAYORD Injector");
				auditLogs.setCreatedBy(user.getUsername());
				auditLogs.setCreatedDate(new Date());
				auditLogs.setReffNo(date);
				
				
				//set headernya 
				HeaderReqPojo header = new HeaderReqPojo();
				header.setName("PAYORD");
				header.setType("OutgoingMessage");
				header.setStructure("PBMovement");
				payord.setHeader(header);
				
				responseRest = restClient.sendPayord(payord);
				
				if (responseRest)
					model.addAttribute("okMessage", getMessage("form.ok.simulator.send.payord" , payord.getExtRef()));
				else 
					model.addAttribute("errorMessage", getMessage("form.error.simulator.send.payord" , payord.getExtRef()));
	
				auditLogService.insertAuditLog(auditLogs);
				log.debug("Added Audit Log");
		//	}
		} catch (Exception ex) {
			log.error(ex);
			model.addAttribute("errorMessage", getMessage("form.error.simulator.send.payord", payord.getExtRef()));
		}

		return getPageContent(SEND_PAGE_NAME);
	    }
}
