package id.co.telkomsigma.app.simulator;

import javax.servlet.http.HttpServletResponse;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.HttpClientErrorException;

import id.co.telkomsigma.app.constant.WebGuiConstant;
import id.co.telkomsigma.app.controller.GenericController;
import id.co.telkomsigma.app.controller.RESTClient;
import id.co.telkomsigma.app.model.web.security.AuditLog;
import id.co.telkomsigma.app.model.web.user.User;
import id.co.telkomsigma.app.pojo.DcnfReqPojo;
import id.co.telkomsigma.app.service.DcnfService;
import id.co.telkomsigma.app.service.KseiWebService;
import id.co.telkomsigma.app.util.FormValidationUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;

@Controller
public class DcnfController extends GenericController { 

	@Autowired
	private DcnfService dcnfService ;
	
	@Autowired
	private RESTClient restClient;


	private static final String SEND_PAGE_NAME = "simulator/dcnf/send";
      
	  @RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_DCNF_MAPPING, method = RequestMethod.GET)
	  public String showSendPage(final Model model,
					final HttpServletRequest request, final HttpServletResponse response) {
		
		  
		  DcnfReqPojo dcnf = new DcnfReqPojo();
		  dcnf.setValDate(new SimpleDateFormat("yyyyMMdd").format(new Date()));
		  dcnf.setExtRef(dcnfService.generateExtRef());
		  
		  System.out.println("dcnf exref  " + dcnf.getExtRef());
		  
		model.addAttribute("dcnf",dcnf);
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		
		String date = new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date());
     	
     	AuditLog auditLogs = new AuditLog();
     	auditLogs.setActivityType(auditLogs.MANUAL_INJECTOR);
     	auditLogs.setCreatedBy(user.getUsername());
     	auditLogs.setCreatedDate(new Date());
     	auditLogs.setDescription("Buka Halaman Manual Injector DCNF");
     	auditLogs.setReffNo(date);
        
        try{
        	auditLogService.insertAuditLog(auditLogs);
        }
        catch(Exception ex){
        	ex.printStackTrace();
        }
        log.debug("Added Audit Log");
     	
    	return getPageContent(SEND_PAGE_NAME);
    }
	
	
	  @RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_DCNF_MAPPING, method = RequestMethod.POST)
	    public String doSend(@ModelAttribute("dcnf") DcnfReqPojo dcnf, 
	    		BindingResult errors, final Model model, 
	    		final HttpServletRequest request, final HttpServletResponse response) {	  
	
		try {
			
			 System.out.println("doSend Message dcnf ");
			 System.out.println("dcnf dcnf " + dcnf.getExtRef() );
			 Boolean responseRest =  false;
			 System.out.println("dcnf dcnf " + dcnf.toString() );
			 
			FormValidationUtil.validatingRequired(errors, "memAC", dcnf.getMemAC(), getMessage("label.simulator.dcnf.memac"));
			FormValidationUtil.validatingRequired(errors, "extref", dcnf.getExtRef(), getMessage("label.simulator.dcnf.extref"));
			FormValidationUtil.validatingRequired(errors, "relRef", dcnf.getRelRef(), getMessage("label.simulator.dcnf.relref"));
			FormValidationUtil.validatingRequired(errors, "curcod", dcnf.getCurCod(), getMessage("label.simulator.dcnf.curcod"));
			FormValidationUtil.validatingRequired(errors, "benac", dcnf.getBenAC(), getMessage("label.simulator.dcnf.benac"));
			FormValidationUtil.validatingRequired(errors, "valdate", dcnf.getValDate(), getMessage("label.simulator.dcnf.valdate"));
			FormValidationUtil.validatingRequired(errors, "cashVal", dcnf.getCashVal(), getMessage("label.simulator.dcnf.cashval"));
			//notes optional
			//memid optional

			
			
			if (!errors.hasErrors()) {
				Authentication auth = SecurityContextHolder.getContext().getAuthentication();
				User user = (User) auth.getPrincipal();

				String date = new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date());

				AuditLog auditLogs = new AuditLog();
				auditLogs.setActivityType(auditLogs.MANUAL_INJECTOR);
				auditLogs.setDescription("Kirim DCNF Injector");
				auditLogs.setCreatedBy(user.getUsername());
				auditLogs.setCreatedDate(new Date());
				auditLogs.setReffNo(date);
	
				auditLogService.insertAuditLog(auditLogs);
				log.debug("Added Audit Log");
				
				responseRest = restClient.sendDcnf(dcnf);
				
				if (responseRest)
					model.addAttribute("okMessage", getMessage("form.ok.simulator.send.dcnf", dcnf.getExtRef()));
				else 
					model.addAttribute("errorMessage", getMessage("form.error.simulator.send.dcnf", dcnf.getExtRef()));
				
			}
		} catch (Exception ex) {
			log.error(ex);
			model.addAttribute("errorMessage", getMessage("form.error.simulator.send.dcnf", dcnf.getExtRef()));
		}

		return getPageContent(SEND_PAGE_NAME);
	    }
	
}
