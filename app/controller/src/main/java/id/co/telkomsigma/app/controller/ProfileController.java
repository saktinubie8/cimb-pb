package id.co.telkomsigma.app.controller;


import id.co.telkomsigma.app.constant.WebGuiConstant;
import id.co.telkomsigma.app.controller.GenericController;
import id.co.telkomsigma.app.model.web.parameter.SystemParameter;
import id.co.telkomsigma.app.model.web.security.AuditLog;
import id.co.telkomsigma.app.model.web.user.User;
import id.co.telkomsigma.app.service.AuditLogService;
import id.co.telkomsigma.app.service.ParameterService;
import id.co.telkomsigma.app.service.UserService;
import id.co.telkomsigma.app.util.FieldValidationUtil;
import id.co.telkomsigma.app.util.FormValidationUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by daniel on 4/23/15.
 */
@Controller("userProfileController")
public class ProfileController extends GenericController {
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserService userServiceWithCache;

    @Autowired
    private AuditLogService auditLogService;

    @Autowired
    private ParameterService parameterService;

    private static final String PROFILE_PAGE_NAME = "profile";

       
    private static String EXPIRY_DAYS = "expiryDays"; 

    private static String DEFAULT_PASSWORD = "DefaultPassword";

    @RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_PROFILE_PAGE, method = {RequestMethod.GET})
    public String showChangeProfilePage(final Model model, final HttpServletRequest request, final HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();


        User user = (User) auth.getPrincipal();
        AuditLog auditLog = new AuditLog();
        auditLog.setActivityType(auditLog.VIEW_PAGE);
        auditLog.setCreatedBy(user.getUsername());
        auditLog.setCreatedDate(new Date());
        auditLog.setDescription("Buka Halaman Profile");
        auditLog.setReffNo(new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date()));

        SystemParameter paramDefaultPwd = new SystemParameter();
        paramDefaultPwd = parameterService.getParamByParamName(DEFAULT_PASSWORD);
        //cek matches default password
        user.setMatchDefault(passwordEncoder.matches(paramDefaultPwd.getParamValue(), user.getPassword()));

        SystemParameter paramDays = new SystemParameter();
        paramDays = parameterService.getParamByParamName(EXPIRY_DAYS);

        //cek expiry days
        Long expiryDays = 0L;
        expiryDays = userServiceWithCache.checkExpiryDays(user);

        if (expiryDays > Long.parseLong(paramDays.getParamValue()))
            user.setExpiryDays(true);
        else
            user.setExpiryDays(false);

        try {
            auditLogService.insertAuditLog(auditLog);
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.debug("Added Audit Log");

        model.addAttribute("user", (User) auth.getPrincipal());
        return getPageContent(PROFILE_PAGE_NAME);
    }

    
    @RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_PROFILE_PAGE, method = {RequestMethod.POST})
    public String doChangeProfile(@ModelAttribute("user") User user, BindingResult errors, final Model model, final HttpServletRequest request, final HttpServletResponse response) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User originalUser = (User) auth.getPrincipal();
        user.setUserId(originalUser.getUserId());
        Boolean match = true;
        SystemParameter param = parameterService.getParamByParamName(WebGuiConstant.PARAMETER_DEFAULT_PASSWORD);
        
        //validating password bukan default 0000000
        Boolean matchDefault = true;
        matchDefault = passwordEncoder.matches(param.getParamValue(), originalUser.getPassword());
        user.setMatchDefault(matchDefault);
        SystemParameter paramMaxLengthPass =
                parameterService.getParamByParamName(WebGuiConstant.PARAMETER_MAX_LENGTH_PASSWORD);
        Short sObj2 = Short.valueOf(paramMaxLengthPass.getParamValue());
        
        //ambil general password
        SystemParameter genPwdObj =
                parameterService.getParamByParamName(WebGuiConstant.PARAMETER_GENERAL_PASSWORD);
        String genPass = genPwdObj.getParamValue();
        
        /*
        SystemParameter paramMaxChangePass =
                parameterService.getParamByParamName(WebGuiConstant.PARAMETER_MAX_CHANGE_PASSWORD);
        log.debug("User Change Password Count : "+originalUser.getChangePasswordCount());
        log.debug("Param Max Change Password : "+paramMaxChangePass.getParamValue());
        
        
        if(originalUser.getChangePasswordCount() > Integer.parseInt(paramMaxChangePass.getParamValue())){
            log.debug("Overlimit change password");
            FormValidationUtil.changePassOverLimit(errors,"password", getMessage("label.password"));
        }
        */
        
        if (matchDefault) {
        	System.out.println("password default ");
            //cekvalidasi reuse Pwd
            if (!user.getPassword().equalsIgnoreCase("")) {
                if (!FieldValidationUtil.isBlank(user.getPasswordOld())) {
                    //validasi perubahan password , menggunakan password yang sama apabila sudah pernah diganti 4x
                    Boolean find = false;
                    log.debug("check reuse password");
                    System.out.println("check reuse password");
                    find = userServiceWithCache.checkReusePassword(user);
                    if (find) {
                        FormValidationUtil.validatingReusePassword(errors, "password", getMessage("label.password"));
 
                    }
                }
            }
            //FormValidationUtil.validatingDefaultPassword(errors, "password", user.getPassword(), getMessage("label.password"), "passwordConfirm", user.getPasswordConfirm(), getMessage("label.password.confirm"), User.MAX_LENGTH_PASSWORD, User.MIN_LENGTH_PASSWORD);
            match = passwordEncoder.matches(user.getPasswordOld(), originalUser.getPassword());
            
            FormValidationUtil.validatingOldPassword(errors, "passwordOld", user.getPasswordOld(), getMessage("label.password.old"), match, getMessage("label.password.old"), sObj2, User.MIN_LENGTH_PASSWORD);
            FormValidationUtil.validatingFormatPassword(errors, "password", user.getPassword(), getMessage("label.password"));
            FormValidationUtil.validatingPassword(errors, "password", user.getPassword(), getMessage("label.password"), "passwordConfirm", user.getPasswordConfirm(), getMessage("label.password.confirm"), sObj2, User.MIN_LENGTH_PASSWORD);
            FormValidationUtil.validatingGeneralPassword(errors, "password", user.getPassword(),genPass, getMessage("label.password"));
        }
        //validating expire days password > 90 hari
        
        Long expiryDays = 0L;
        expiryDays = userServiceWithCache.checkExpiryDays(originalUser);
        SystemParameter paramDays = parameterService.getParamByParamName(EXPIRY_DAYS);
        if (expiryDays > Long.parseLong(paramDays.getParamValue())) {
        	System.out.println("password sudah expired ");
            //cekvalidasi reuse Pwd
            if (!user.getPassword().equalsIgnoreCase("")) {
                if (!FieldValidationUtil.isBlank(user.getPasswordOld())) {
                    //validasi perubahan password , menggunakan password yang sama apabila sudah pernah diganti 4x
                    Boolean find = false;
                    System.out.println("check reuse password");
                    find = userServiceWithCache.checkReusePassword(user);
                    System.out.println("find " + find);
                    if (find) {
                        FormValidationUtil.validatingReusePassword(errors, "password", getMessage("label.password"));
                        //dilarang menggunakan password yang sama, sebelum diganti 4x
                    }
                }
            }
            //FormValidationUtil.validatingDefaultPassword(errors, "password", user.getPassword(), getMessage("label.password"), "passwordConfirm", user.getPasswordConfirm(), getMessage("label.password.confirm"), User.MAX_LENGTH_PASSWORD, User.MIN_LENGTH_PASSWORD);
            user.setExpiryDays(true);
            match = passwordEncoder.matches(user.getPasswordOld(), originalUser.getPassword());
            FormValidationUtil.validatingOldPassword(errors, "passwordOld", user.getPasswordOld(), getMessage("label.password.old"), match, getMessage("label.password.old"), sObj2, User.MIN_LENGTH_PASSWORD);
            FormValidationUtil.validatingFormatPassword(errors, "password", user.getPassword(), getMessage("label.password"));
            FormValidationUtil.validatingPassword(errors, "password", user.getPassword(), getMessage("label.password"), "passwordConfirm", user.getPasswordConfirm(), getMessage("label.password.confirm"), sObj2, User.MIN_LENGTH_PASSWORD);
            FormValidationUtil.validatingGeneralPassword(errors, "password", user.getPassword(),genPass, getMessage("label.password"));
        }
        System.out.println("matched Default : " + matchDefault);
        
        System.out.println("== expiry days : " + expiryDays);
        
        
        
        Boolean cek = !matchDefault && expiryDays < Long.parseLong(paramDays.getParamValue());
        
        System.out.println("!matchDefault && expiryDays < Long.parseLong(paramDays.getParamValue())  " + cek);
        
        if (!matchDefault && expiryDays < Long.parseLong(paramDays.getParamValue())) {
        	
        	System.out.println("password bukan default dan blm expired ");
            // validating name
            FormValidationUtil.validatingName(errors, "name", user.getName(), getMessage("label.name"), User.MAX_LENGTH_NAME);
            // validating address
            FormValidationUtil.validatingAddress(errors, "address", user.getAddress(), getMessage("label.address"), User.MAX_LENGTH_ADDRESS);
            // validating fax
            FormValidationUtil.validatingPhoneMobile(errors, "fax", user.getPhoneMobile(), getMessage("label.fax"), User.MAX_LENGTH_PHONE_MOBILE, User.MIN_LENGTH_PHONE_MOBILE);
            // validating phone
            FormValidationUtil.validatingPhoneMobile(errors, "phone", user.getPhoneMobile(), getMessage("label.phone"), User.MAX_LENGTH_PHONE_MOBILE, User.MIN_LENGTH_PHONE_MOBILE);
            // validating phone mobile
            FormValidationUtil.validatingPhoneMobile(errors, "phoneMobile", user.getPhoneMobile(), getMessage("label.phone.mobile"), User.MAX_LENGTH_PHONE_MOBILE, User.MIN_LENGTH_PHONE_MOBILE);
            if (!user.getPassword().equalsIgnoreCase("")) {
                // validating password if password is not empty
                //Boolean match = true;
                if (!FieldValidationUtil.isBlank(user.getPasswordOld())) {
                    //validasi perubahan password , menggunakan password yang sama apabila sudah pernah diganti 4x
                    Boolean find = false;
                    System.out.println("check reuse password");
                    
                    find = userServiceWithCache.checkReusePassword(user);
                    if (find) {
                    	 //dilarang menggunakan password yang sama, sesuai histori berapa kalinya, min 18, max parameterize 
                        FormValidationUtil.validatingReusePassword(errors, "password", getMessage("label.password"));
                       
                    } else {
                    	//cek old password 
                        match = passwordEncoder.matches(user.getPasswordOld(), originalUser.getPassword());
                        FormValidationUtil.validatingOldPassword(errors, "passwordOld", user.getPasswordOld(), getMessage("label.password.old"), match, getMessage("label.password.old"), sObj2, User.MIN_LENGTH_PASSWORD);
                        //cek format password
                        FormValidationUtil.validatingFormatPassword(errors, "password", user.getPassword(), getMessage("label.password"));
                        //cek panjang password 
                        FormValidationUtil.validatingPassword(errors, "password", user.getPassword(), getMessage("label.password"), "passwordConfirm", user.getPasswordConfirm(), getMessage("label.password.confirm"), sObj2, User.MIN_LENGTH_PASSWORD);
                        //cek password tidak boleh pwd yang general
                        FormValidationUtil.validatingGeneralPassword(errors, "password", user.getPassword(),genPass, getMessage("label.password"));
                    }
                }
            }
        }
        user.setUsername(originalUser.getUsername());
        user.setEmail(originalUser.getEmail());
        AuditLog auditLog = new AuditLog();
        auditLog.setActivityType(auditLog.SAVE_OR_UPDATE);
        auditLog.setCreatedBy(user.getUsername());
        auditLog.setCreatedDate(new Date());
        auditLog.setReffNo(new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date()));
        if (!errors.hasErrors()) {
            userServiceWithCache.updateProfile(user);
            originalUser.setName(user.getName());
            originalUser.setAddress(user.getAddress());
            originalUser.setPhoneMobile(user.getPhoneMobile());
            originalUser.setPhone(user.getPhone());
            originalUser.setExtension(user.getExtension());
            originalUser.setFax(user.getFax());
            originalUser.setJobTitle(user.getJobTitle());
            if (!user.getPassword().equalsIgnoreCase("")) {
                originalUser.setPassword(user.getPassword());
            }
            Authentication authentication = new UsernamePasswordAuthenticationToken(originalUser, originalUser.getPassword(), originalUser.getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(authentication);
            model.addAttribute("okMessage", getMessage("form.ok.change.profile"));
            auditLog.setDescription("Edit Profile berhasil");
            try {
                auditLogService.insertAuditLog(auditLog);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return getPageContent(PROFILE_PAGE_NAME);
    }

}