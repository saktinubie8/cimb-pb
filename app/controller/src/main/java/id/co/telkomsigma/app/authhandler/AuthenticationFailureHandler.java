package id.co.telkomsigma.app.authhandler;

import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.stereotype.Component;

import id.co.telkomsigma.app.constant.WebGuiConstant;
import id.co.telkomsigma.app.model.web.parameter.SystemParameter;
import id.co.telkomsigma.app.model.web.security.AuditLog;
import id.co.telkomsigma.app.model.web.user.User;
import id.co.telkomsigma.app.service.AuditLogService;
import id.co.telkomsigma.app.service.ParameterService;
import id.co.telkomsigma.app.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component("authenticationFailureHandler")
public class AuthenticationFailureHandler implements org.springframework.security.web.authentication.AuthenticationFailureHandler {
	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
	
	@Autowired
	private UserService userServiceWithCache;
	
	@Autowired
	private ParameterService paramService;
	
	@Autowired
	private AuditLogService auditLogService;
	
	private final String MAX_FAILED_ATTEMPT = "maxFinalAttempt";
	
	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException authExc) throws IOException, ServletException {
        String username = request.getParameter("username");

        Log log = LogFactory.getLog(this.getClass());
        log.debug("Login fail with: " + username + " " + authExc.getMessage());
        if (authExc instanceof LockedException) {
            redirectStrategy.sendRedirect(request, response, "/login?error=locked");
        } else if (authExc instanceof DisabledException) {
            redirectStrategy.sendRedirect(request, response, "/login?error=disabled");
        } else if (authExc instanceof CredentialsExpiredException) {
            redirectStrategy.sendRedirect(request, response, "/login?error=expired-credential");
        } else if (authExc instanceof AccountExpiredException) {
            redirectStrategy.sendRedirect(request, response, "/login?error=expired-account");
        } else if (authExc instanceof BadCredentialsException) {
            try{
        	UserDetails userDetail = userServiceWithCache.loadUserByUsername(username);
        	if(userDetail!=null)
        	{
        		User user = (User) userDetail;
	        	user.setPassword(null);
	        	//String failedAttempt = user.getCustomProp(WebGuiConstant.PARAMETER_USER_CURRENT_FAILED_ATTEMPT);
	        	Long failedAttempt = user.getFailedLogin();	        	
	        	//if(failedAttempt!=null)
	        	/*if(failedAttempt==0L)
	        	{*/
	        		//Integer currentAttempt = Integer.parseInt(failedAttempt);
	        		Integer currentAttempt = failedAttempt.intValue();
	        		//Integer maxAttempt = Integer.parseInt(paramService.loadParamByParamName(WebGuiConstant.PARAMETER_NAME_MAX_FAILED_ATTEMPT, WebGuiConstant.PARAMETER_MAX_FAILED_ATTEMPT_DEFAULT_VALUE));
	        		SystemParameter param = paramService.getParamByParamName(MAX_FAILED_ATTEMPT);
	        		Integer maxAttempt = Integer.parseInt(param.getParamValue());
	        		currentAttempt++;
	        		if(currentAttempt>=maxAttempt)
	        		{
	        			//jika melebihi batas maksimal --> ke lock
	        			user.setAccountNonLocked(false);
	        			//Integer lockDurationInMinutes = Integer.parseInt(paramService.loadParamByParamName(WebGuiConstant.PARAMETER_NAME_LOCK_DURATION, WebGuiConstant.PARAMETER_LOCK_DURATION_DEFAULT_VALUE));
	        			//Date lockUntil = DateUtils.addMinutes(new Date(), lockDurationInMinutes);     			
	        			//user.setCustomProp(WebGuiConstant.PARAMETER_USER_CURRENT_LOCK_UNTIL, ""+lockUntil.getTime());
	        			userServiceWithCache.failedAttempt(user, currentAttempt);
	        		}
	        		else
	        		{
	        			//belum maksimal, hanya update
	        			user.setCustomProp(WebGuiConstant.PARAMETER_USER_CURRENT_FAILED_ATTEMPT, ""+currentAttempt);
	        			userServiceWithCache.failedAttempt(user, currentAttempt);
	        			
	        		}
	
	        /*	}
	        	else
	        	{
	        		user.setCustomProp(WebGuiConstant.PARAMETER_USER_CURRENT_FAILED_ATTEMPT, "1");
	        		userServiceWithCache.failedAttempt(user, currentAttempt);
	        	}*/
				userServiceWithCache.clearUserCache();
				String date = new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date());
		        
		        AuditLog auditLog = new AuditLog();
		        auditLog.setActivityType(1);
		        auditLog.setCreatedBy(user.getUsername());
		        auditLog.setCreatedDate(new Date());
		        auditLog.setDescription("Login failed bad credential");
		        auditLog.setReffNo(date);
		        
		        auditLogService.insertAuditLog(auditLog);
		        log.debug("Added Audit Log");
        	}
        	/*else 
        		redirectStrategy.sendRedirect(request, response, "/login?error=invalid-username");*/
            }catch (Exception e){}
			//redirectStrategy.sendRedirect(request, response, "/login?error=bad-credential");
        	redirectStrategy.sendRedirect(request, response, "/login?error=invalid-username-password");
        } else {
            redirectStrategy.sendRedirect(request, response, "/login?error");
        }
	}
	
}