package id.co.telkomsigma.app.authhandler;

import id.co.telkomsigma.app.model.web.menu.Menu;
import id.co.telkomsigma.app.model.web.user.Role;
import id.co.telkomsigma.app.model.web.user.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

public class MenuValidation implements HandlerInterceptor{
	private static final Log log = LogFactory.getLog(MenuValidation.class);
	
	@Override
	public void afterCompletion(HttpServletRequest arg0,
			HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1,
			Object arg2, ModelAndView arg3) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
			Object arg2) throws Exception {
//		boolean retVal = false;
//		
//		if("/home/list".equals(request.getPathInfo()) || "/home/list".equals(request.getServletPath()) || "/home".equals(request.getServletPath()) || "/home/profile".equals(request.getServletPath()) || "/home".equals(request.getPathInfo()) || "/home/profile".equals(request.getPathInfo()))
//			return true;
//		
//		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//		User user = (User)auth.getPrincipal();
//		
//		for(Role role : user.getRoles())
//		{
//			for(Menu menu : role.getMenus())
//			{
//				if(request.getServletPath().indexOf(menu.getMenuPath())>=0 || request.getPathInfo().indexOf(menu.getMenuPath())>=0)
//				{
//					retVal=true;
//					return retVal;
//				}
//				
//			}
//		}		
//		log.warn("Role owned for User "+user.getUsername()+" is not allowed to access '"+request.getServletPath()+"' or '"+request.getPathInfo()+"'! System will Force Logout the user.");
//		response.sendRedirect(request.getContextPath()+"/logout");
//		return retVal;
		return true;
	}

}
