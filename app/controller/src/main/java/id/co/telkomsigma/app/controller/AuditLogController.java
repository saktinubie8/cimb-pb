package id.co.telkomsigma.app.controller;

import id.co.telkomsigma.app.constant.WebGuiConstant;
import id.co.telkomsigma.app.model.web.security.AuditLog;
import id.co.telkomsigma.app.model.web.user.User;
import id.co.telkomsigma.app.pojo.DataTablesPojo;
import id.co.telkomsigma.app.service.AuditLogService;
import id.co.telkomsigma.app.util.DateUtil;
import id.co.telkomsigma.app.util.JsonUtils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.HttpClientErrorException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.apache.commons.lang.StringEscapeUtils.escapeHtml;

/**
 * Created by Dzulfiqar on 11/13/15.
 */
@Controller
public class AuditLogController extends GenericController {

    // ----------------------------------------- LIST PARAMETER SECTION ----------------------------------------- //
    private static final String LIST_PAGE_NAME = "auditLog/list";
    private static final String LIST_COMMAND = "redirect:" + WebGuiConstant.AFTER_LOGIN_PATH_AUDIT_LOG;

    @Autowired
    private AuditLogService auditLogService;

    @RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_AUDIT_LOG_MAPPING, method = RequestMethod.GET)
    public String showListPage(final Model model, final HttpServletRequest request, final HttpServletResponse response) {
        List<AuditLog> auditLog = auditLogService.getLogs();
        model.addAttribute("auditLog", auditLog);

        Authentication auth = SecurityContextHolder.getContext()
                .getAuthentication();
        User user = (User) auth.getPrincipal();

        String date = new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date());

        AuditLog auditLogs = new AuditLog();
        auditLogs.setActivityType(auditLogs.VIEW_PAGE);
        auditLogs.setCreatedBy(user.getUsername());
        auditLogs.setCreatedDate(new Date());
        auditLogs.setDescription("Buka Halaman AuditLog");
        auditLogs.setReffNo(date);

        try {
            auditLogService.insertAuditLog(auditLogs);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        log.debug("Added Audit Log");

        return getPageContent(LIST_PAGE_NAME);
    }

    @RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_AUDIT_LOG_GET_LIST_MAPPING, method = RequestMethod.GET)
    public void doSearch(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        if (isAjaxRequest(request)) {
            setResponseAsJson(response);

            DataTablesPojo dataTablesPojo = new DataTablesPojo();
            try {
                dataTablesPojo.setDraw(Integer.parseInt(request.getParameter("draw")));
            } catch (NumberFormatException e) {
                dataTablesPojo.setDraw(0);
            }

            final int orderableColumnCount = 5;
            final StringBuffer sbOrder = new StringBuffer();
            LinkedHashMap<String, String> columnMap = new LinkedHashMap<String, String>() {
                {
                	put("0", "createdDate");
                    put("1", "activity_type");
                    put("2", "description");
                    put("3", "createdBy");
                    put("4", "reffNo");
                }
            };

            int offset = 0;
            int limit = 50;
            Enumeration<String> parameterNames = request.getParameterNames();
            String orderBy = null;
            String sortBy = null;
            
            while (parameterNames.hasMoreElements()) {

                String paramName = parameterNames.nextElement();
                String paramValues = request.getParameter(paramName);
                
                if ((paramName.contains("draw")) && (paramValues.equals("1"))) {
    				System.out.println("Default Pencarian , order by Created Date DESC ");
    				orderBy = "createdDate";
    				sortBy = "desc";
    				break;
    			}
                else if (paramName.contains("dir")) {
                    sortBy = request.getParameter(paramName);
                } else if (paramName.contains("order") && paramName.contains("column")) {
                    orderBy = request.getParameter(paramName);
                    switch (orderBy) {
                    	case "0":
	                        orderBy = "createdDate";
	                        break;
                        case "1":
                            orderBy = "activityType";
                            break;
                        case "2":
                            orderBy = "description";
                            break;
                        case "3":
                            orderBy = "createdBy";
                            break;
                        case "4":
                            orderBy = "reffNo";
                            break;
                        default:
                            orderBy = "createdDate";
                    }
                }

            }
            try {
                offset = Integer.parseInt(request.getParameter("start"));
            } catch (NumberFormatException e) {
            }
            try {
                limit = Integer.parseInt(request.getParameter("length"));
            } catch (NumberFormatException e) {
            }

            for (int i = 0; i < orderableColumnCount; i++) {
                String orderColumn = request.getParameter("order[" + i + "][column]");
                if (orderColumn == null || orderColumn.equalsIgnoreCase("")) {
                    break;
                } else {
                    orderColumn = columnMap.get(orderColumn);
                    if (orderColumn == null) {
                        break;
                    }
                    String orderDir = request.getParameter("order[" + i + "][dir]");
                    if (orderDir == null || !orderDir.equalsIgnoreCase("desc")) {
                        orderDir = "asc";
                    }
                    sbOrder.append(orderColumn);
                    sbOrder.append("-");
                    sbOrder.append(orderDir);
                    sbOrder.append("+");
                }
            }

            Log log = LogFactory.getLog(this.getClass());
            String keyword = request.getParameter("q");
            System.out.println("===keyword " + keyword);
            String rangeDate = request.getParameter("c");

            if (rangeDate == "")
                rangeDate = null;

            System.out.println("rangeDate : " + rangeDate);

            //SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            SimpleDateFormat formatter2 = new SimpleDateFormat("dd/MM/yyyy");
            Map<String, Object> parameterList = new HashMap<String, Object>();
            String startDate = null;
            String endDate = null;
            Date std = new Date();
            Date ed = new Date();

            //Date std 
            if (rangeDate != null) {
                System.out.println("Range Date != null" + rangeDate);
                String split[] = rangeDate.split("-");
                startDate = split[0].trim();
                endDate = split[1].trim();
                try {
                    std = formatter2.parse(startDate);
                    ed = DateUtil.addDays(formatter2.parse(endDate), 1);
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

            System.out.println("startDate : " + startDate + " std : " + std);
            System.out.println("endDate : " + endDate + " ed : " + ed);

            System.out.println("===range date after mapp" + rangeDate);
            String type = request.getParameter("a");
            System.out.println("===type from list " + type);
            Integer activityType = 0;
            if (type == "")
                type = null;
            if (type != null)
                activityType = Integer.parseInt(type);

            System.out.println("===activityType " + activityType);

            if (keyword != null && keyword.trim().length() > 0) {
                keyword = "%" + keyword + "%";
                parameterList.put("keyword", keyword);
            }

            if (activityType != 0) {
                //trxName = "%" + trxName + "%";
                //activityType =  activityType;
                parameterList.put("activityType", activityType);
            }

            if (startDate != null && endDate != null) {
                System.out.println("std : " + std + " ed : " + ed);
                parameterList.put("std", std);
                parameterList.put("ed", ed);
            }

            try {
                final int finalOffset = offset;
                final int finalLimit = limit;

                Page<AuditLog> auditLogPojo = auditLogService.searchAuditLogs(parameterList, columnMap, finalOffset,
                        finalLimit, orderBy, sortBy);

                List<String[]> data = new ArrayList<>();
                Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Format formatter3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                for (AuditLog audit : auditLogPojo) {
                    System.out.println(audit.getReffNo());
                    int tipeAktifitas = audit.getActivityType();
                    String namaAktifitas = new String();
                    if (tipeAktifitas == 1)
                        namaAktifitas = "Login/Logout Web";
                    else if (tipeAktifitas == 2)
                        namaAktifitas = "Buka Halaman";
                    else if (tipeAktifitas == 3)
                        namaAktifitas = "Tambah atau Ubah data";
                    else if (tipeAktifitas == 4)
                        namaAktifitas = "Hapus Cache";
                    else if (tipeAktifitas == 5)
                        namaAktifitas = "Monitoring Koneksi";
                    else if (tipeAktifitas == 6)
                        namaAktifitas = "Store dan Forward";
                    else if (tipeAktifitas == 7)
                        namaAktifitas = "Manual Injector";
                    data.add(new String[]{
                    		escapeHtml(formatter3.format(audit.getCreatedDate()).toString()),
                            escapeHtml(namaAktifitas),
                            escapeHtml(audit.getDescription()),
                            escapeHtml(audit.getCreatedBy()),
                            escapeHtml(audit.getReffNo())});
                }

                dataTablesPojo.setData(data);
                dataTablesPojo.setRecordsFiltered(auditLogPojo.getTotalElements());
                dataTablesPojo.setRecordsTotal(Integer.parseInt("" + auditLogPojo.getTotalElements()));

            } catch (HttpClientErrorException e) {
                log.error(e.getStatusCode().toString(), e);
                dataTablesPojo.setError(getMessage("message.datatables.error.fetch.user"));
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                dataTablesPojo.setError(getMessage("message.datatables.error.fetch.user"));
            } finally {
                response.getWriter().write(new JsonUtils().toJson(dataTablesPojo));

            }
        } else {
            response.setStatus(HttpServletResponse.SC_FOUND);
        }
    }

}