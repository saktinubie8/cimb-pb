package id.co.telkomsigma.app.logs;

import static org.apache.commons.lang.StringEscapeUtils.escapeHtml;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;

import id.co.telkomsigma.app.constant.WebGuiConstant;
import id.co.telkomsigma.app.controller.GenericController;
import id.co.telkomsigma.app.model.web.logs.MessageLogs;
import id.co.telkomsigma.app.model.web.logs.StoreAndForward;
import id.co.telkomsigma.app.model.web.security.AuditLog;
import id.co.telkomsigma.app.model.web.user.Role;
import id.co.telkomsigma.app.model.web.user.User;
import id.co.telkomsigma.app.pojo.DataTablesPojo;
import id.co.telkomsigma.app.service.StoreAndForwardService;
import id.co.telkomsigma.app.util.DateUtil;
import id.co.telkomsigma.app.util.FormValidationUtil;
import id.co.telkomsigma.app.util.JsonUtils;

@Controller
public class StoreAndForwardController extends GenericController {

	@Autowired
	StoreAndForwardService snfService;

	@Autowired
	JsonUtils jsonUtils;

	private static final String LIST_PAGE_NAME = "log/storeandforward/list";
	// get data store and forward

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_LOG_STORE_AND_FORWARD_MAPPING, method = RequestMethod.GET)
	public String showListPage(final Model model, final HttpServletRequest request,
			final HttpServletResponse response) {

		List<StoreAndForward> storeAndForward = new ArrayList<StoreAndForward>();
		storeAndForward = snfService.getStoreAndForward();

		model.addAttribute("storeAndForward", storeAndForward);

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();

		AuditLog auditLog = new AuditLog();
		auditLog.setActivityType(auditLog.VIEW_PAGE);
		auditLog.setCreatedBy(user.getUsername());
		auditLog.setCreatedDate(new Date());
		auditLog.setDescription("Buka Halaman List Store And Forward");
		auditLog.setReffNo(new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date()));

		try {
			auditLogService.insertAuditLog(auditLog);
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.debug("Added Audit Log");

		return getPageContent(LIST_PAGE_NAME);
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_LOG_STORE_AND_FORWARD_GET_LIST_MAPPING, method = RequestMethod.GET)
	public void doSearch(final HttpServletRequest request, final HttpServletResponse response) throws IOException {

		Enumeration<String> parameterNames = request.getParameterNames();
		String orderBy = null;
		String sortBy = null;
		while (parameterNames.hasMoreElements()) {

			String paramName = parameterNames.nextElement();
			 String paramValues = request.getParameter(paramName);
			 
			 if ((paramName.contains("draw")) && (paramValues.equals("1"))) {
 				System.out.println("Default Pencarian , order by Created Date DESC ");
 				orderBy = "createdDate";
 				sortBy = "desc";
 				break;
 			}
             else if (paramName.contains("dir")) {
				sortBy = request.getParameter(paramName);
			} else if (paramName.contains("order") && paramName.contains("column")) {
				orderBy = request.getParameter(paramName);
				switch (orderBy) {
				case "0":
					orderBy = "createdDate";
					break;
				case "1":
					orderBy = "transactionId";
					break;
				case "2":
					orderBy = "destination";
					break;
				case "3":
					orderBy = "lastRetry";
					break;
				default:
					orderBy = "createdDate";
				}
			}

		}
		if (isAjaxRequest(request)) {
			setResponseAsJson(response);

			DataTablesPojo dataTablesPojo = new DataTablesPojo();
			try {
				dataTablesPojo.setDraw(Integer.parseInt(request.getParameter("draw")));
			} catch (NumberFormatException e) {
				dataTablesPojo.setDraw(0);
			}

			final int orderableColumnCount = 5;
			final StringBuffer sbOrder = new StringBuffer();
			LinkedHashMap<String, String> columnMap = new LinkedHashMap<String, String>() {
				{
					put("0", "transactionId");
					put("1", "createdDate");
					put("2", "destination");
					put("3", "lastRetry");
				}
			};

			int offset = 0;
			int limit = 50;
			try {
				offset = Integer.parseInt(request.getParameter("start"));
			} catch (NumberFormatException e) {
			}
			try {
				limit = Integer.parseInt(request.getParameter("length"));
			} catch (NumberFormatException e) {
			}

			for (int i = 0; i < orderableColumnCount; i++) {
				String orderColumn = request.getParameter("order[" + i + "][column]");
				if (orderColumn == null || orderColumn.equalsIgnoreCase("")) {
					break;
				} else {
					orderColumn = columnMap.get(orderColumn);
					if (orderColumn == null) {
						break;
					}
					String orderDir = request.getParameter("order[" + i + "][dir]");
					if (orderDir == null || !orderDir.equalsIgnoreCase("desc")) {
						orderDir = "asc";
					}
					sbOrder.append(orderColumn);
					sbOrder.append("-");
					sbOrder.append(orderDir);
					sbOrder.append("+");
				}
			}

			Log log = LogFactory.getLog(this.getClass());
			String keyword = request.getParameter("q");
			log.debug("keyword " + keyword);
			String rangeDate = request.getParameter("d");
			Map<String, Object> parameterList = new HashMap<String, Object>();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			SimpleDateFormat formatter2 = new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat formatter3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String destination = request.getParameter("g");

			String startDate = null;
			String endDate = null;
			Date std = new Date();
			Date ed = new Date();

			System.out.println("keyword : " + keyword);
			System.out.println("rangeDate : " + rangeDate);
			System.out.println("destination : " + destination);

			if (rangeDate == "")
				rangeDate = null;

			if (rangeDate != null) {
				String split[] = rangeDate.split("-");
				startDate = split[0].trim();
				endDate = split[1].trim();
				try {
					std = formatter2.parse(startDate);
					ed = DateUtil.addDays(formatter2.parse(endDate), 1);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				System.out.println("startDate : " + startDate + " std : " + std);
				System.out.println("endDate : " + endDate + " ed : " + ed);
			}

			if (keyword != null && keyword.trim().length() > 0) {
				keyword = "%" + keyword + "%";
				parameterList.put("keyword", keyword);
			}

			if (startDate != null && endDate != null) {
				parameterList.put("std", std);
				parameterList.put("ed", ed);
			}
			if (destination != null && destination.trim().length() > 0) {
				destination = "%" + destination + "%";
				parameterList.put("destination", destination);
			}

			try {
				final int finalOffset = offset;
				final int finalLimit = limit;

				Page<StoreAndForward> snfList = snfService.searchStoreAndForward(parameterList, finalOffset,
						finalLimit, orderBy, sortBy);

				System.out.println("==jumlah record : " + snfList.getTotalElements());

				List<String[]> data = new ArrayList<>();
				String resend = "RESEND";
				String detail = "VIEW DETAIL";

				for (StoreAndForward snf : snfList) {

					String lastRetry = "";

					if (null != snf.getLastRetry())
						lastRetry = formatter.format(snf.getLastRetry());

					// formatted message :
					String formatMsg = "";
					String frmtMsg = "";
					StringBuilder result = new StringBuilder();

					result.append(System.getProperty("line.separator"));
					Map<String, Object> maps = jsonUtils.jsonToMapOfObject(snf.getRawRequest());
					// loop maps
					for (Map.Entry<String, Object> formatDt : maps.entrySet()) {
						System.out.println("Key : " + formatDt.getKey() + " Value : " + formatDt.getValue());
						formatMsg = " " + formatDt.getKey() + " : " + formatDt.getValue() + " ";
						result.append(formatMsg);
						result.append("\n");
						// frmtMsg = frmtMsg.concat(formatMsg).app("\n");
						frmtMsg = result.toString();
						System.out.println(" ======= fmtr : " + frmtMsg);
					}

					String url = "";

					url = url
							+ "<a href=\"#\" data-toggle=\"modal\" data-target=\"#buttonModal\" data-event=\"delete\" "
							+ "data-id=\"" + snf.getId().toString() + "\" " 
							+ "data-tid=\"" + snf.getTransactionId().toString() + "\" "
							+ "data-raw=\"" + frmtMsg + "\" "
							+ "data-link=\"" + getServletContext().getContextPath()
							+ WebGuiConstant.AFTER_LOGIN_PATH_LOG_STORE_AND_FORWARD_DELETE_REQUEST.replace("{id}",
									"" + snf.getId().toString())
							+ "\" data-id=\"" + snf.getId().toString()
							+ "\" data-tooltip='true' data-placement='bottom' title='" + getMessage("label.delete")
							+ "'><i class='fa fa-trash fa-1x'>"
							+ "<div class='action' id='action-delete'>delete</div></i></a>&nbsp&nbsp";


				
					data.add(new String[] { 
							escapeHtml(formatter3.format(snf.getCreatedDate())), 
							escapeHtml(snf.getTransactionId().toString()),
							escapeHtml(snf.getDestination()),
							escapeHtml(snf.getLastError()), escapeHtml(lastRetry),
							escapeHtml(snf.getRetryCount().toString()), url
					});
				}

				dataTablesPojo.setData(data);
				dataTablesPojo.setRecordsFiltered(snfList.getTotalElements());
				dataTablesPojo.setRecordsTotal(snfList.getTotalElements());
				/*
				 * dataTablesPojo.setRecordsTotal(Integer.parseInt("" +
				 * messageLogsList.getTotalElements()));
				 */

			} catch (HttpClientErrorException e) {
				log.error(e.getStatusCode().toString(), e);
				dataTablesPojo.setError(getMessage("message.datatables.error.fetch.log.message"));
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				dataTablesPojo.setError(getMessage("message.datatables.error.fetch.log.message"));
			} finally {
				response.getWriter().write(new JsonUtils().toJson(dataTablesPojo));

			}
		} else {
			response.setStatus(HttpServletResponse.SC_FOUND);
		}
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_LOG_STORE_AND_FORWARD_DELETE_MAPPING, method = {
			RequestMethod.GET })
	public String deleteUser(@RequestParam(value = "id", required = true) String id,
			@ModelAttribute("snf") StoreAndForward snf, BindingResult errors, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) {

		// audit log
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User admin = (User) auth.getPrincipal();

		AuditLog auditLog = new AuditLog();
		auditLog.setActivityType(auditLog.SAVE_OR_UPDATE);
		auditLog.setCreatedBy(admin.getUsername());
		auditLog.setCreatedDate(new Date());
		auditLog.setReffNo(new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date()));

		try {
			// if (!errors.hasErrors()) {

			try {
				System.out.println("snf id : " + id);
				if (null != id)
					snf = snfService.findSnfById(id);

				if (snf == null) {
					model.addAttribute("errorMessage", getMessage("field.error.snf.not.found", snf.getId().toString()));
					auditLog.setDescription("Hapus" + snf.getId().toString() + " gagal");
				} else {
					snfService.deleteSnf(snf);
					model.addAttribute("okMessage", getMessage("form.ok.delete.snf", snf.getTransactionId().toString()));
					auditLog.setDescription("Hapus Record Store And Forward " + snf.getId().toString() + " berhasil");
					try {
						auditLogService.insertAuditLog(auditLog);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			} catch (HttpClientErrorException e) {
				log.error(e.getStatusCode().toString(), e);
				auditLog.setDescription("Hapus Record Store And Forward " + snf.getId().toString() + " gagal");
				try {
					auditLogService.insertAuditLog(auditLog);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				model.addAttribute("errorMessage", getMessage("field.error.snf.not.found", snf.getId().toString()));
				auditLog.setDescription("Hapus" + snf.getId().toString() + " gagal");
				try {
					auditLogService.insertAuditLog(auditLog);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
			// }
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			model.addAttribute("errorMessage", getMessage("form.error.delete.snf"));
			auditLog.setDescription("Hapus" + snf.getId().toString() + " gagal");
			try {
				auditLogService.insertAuditLog(auditLog);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return getPageContent(LIST_PAGE_NAME);
	}

}
