package id.co.telkomsigma.app.web.dao;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;

import id.co.telkomsigma.app.model.web.logs.MessageLogs;
import org.springframework.data.domain.Pageable;

//message logs dao
public interface LogDao extends JpaRepository<MessageLogs,UUID> {
	
	MessageLogs findById(UUID messageId);
	
	Page<MessageLogs> findById(UUID messageId, Pageable pageable);
	
	Page<MessageLogs> findByTransactionIdOrderByCreatedDateDesc(UUID trxId, Pageable pageable);
	
}
