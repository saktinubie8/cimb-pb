package id.co.telkomsigma.app.web.dao;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import id.co.telkomsigma.app.model.web.security.AuditLog;

public interface AuditLogDao extends JpaRepository<AuditLog, String> {

    int countByDescriptionLikeIgnoreCase(String keyword);

    int countByDescriptionLikeIgnoreCaseAndActivityTypeAndCreatedDateBetween(String keyword, int activityType, Date startDate, Date endDate);

    int countByDescriptionLikeIgnoreCaseAndCreatedDateBetween(String keyword, Date startDate, Date endDate);

    int countByDescriptionLikeIgnoreCaseAndActivityTypeOrderByCreatedDateDesc(String keyword, int activityType);

    Page<AuditLog> findByDescriptionLikeOrCreatedByLikeIgnoreCaseOrderByCreatedDateDesc(String keyword, String keyword1, Pageable pageable);

    Page<AuditLog> findByDescriptionLikeOrCreatedByLikeIgnoreCaseAndActivityTypeAndCreatedDateBetweenOrderByCreatedDateDesc(String keyword, String keyword2, int activityType, Date startDate, Date endDate, Pageable pageable);

    Page<AuditLog> findByDescriptionLikeOrCreatedByLikeIgnoreCaseAndCreatedDateBetweenOrderByCreatedDateDesc(String keyword, String keyword2 ,Date startDate, Date endDate, Pageable pageable);

    Page<AuditLog> findByDescriptionLikeOrCreatedByLikeIgnoreCaseAndActivityTypeOrderByCreatedDateDesc(String keyword, String keyword2, int activityType, Pageable pageable);
    
   // Page<AuditLog> findByDescriptionLikeOrCreatedByLikeIgnoreCaseAndActivityTypeOrderByCreatedDateDesc(String keyword1, String keyword2, int activityType,Pageable pageable )

    Page<AuditLog> findByActivityTypeOrderByCreatedDateDesc(int ActivityType ,Pageable pageable);
    
    Page<AuditLog> findByCreatedDateBetweenOrderByCreatedDateDesc(Date startDate, Date endDate, Pageable pageable);
    
    Page<AuditLog>  findByCreatedDateBetweenAndActivityTypeOrderByCreatedDateDesc(Date startDate, Date endDate,  int activityType, Pageable pageable);
}