package id.co.telkomsigma.app.web.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.telkomsigma.app.model.web.menu.Menu;

public interface MenuDao extends JpaRepository<Menu, String> {    
    /**
     * Get certain role menuID
     * @return String of Menu ID 
     */
    @Query("SELECT r.menus FROM Role r WHERE r.id = :authority") 
    List<Menu> getListMenu(@Param("authority") Long roleId);
    
    /**
     * Get list menu by role id on function
     * @param authority
     * @return list of Menu object
     */
    List<Menu> findByMenuIdIn(Long[] listMenuId);
    
    /**
     * Get all menu
     * @return list of Menu object
     */
    List<Menu> findByMenuParentIdNotNullOrderByMenuName();
    
    Menu findByMenuId(Long menuId);
}
