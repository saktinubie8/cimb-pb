package id.co.telkomsigma.app.web.dao;

import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import id.co.telkomsigma.app.model.web.logs.TransactionLogs;

public interface TransactionLogsDao  extends JpaRepository<TransactionLogs,UUID>{

	
	TransactionLogs findById(UUID trxId);
	
	Page<TransactionLogs> findById(UUID trxId, Pageable pageable);
}
