package id.co.telkomsigma.app.web.dao;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.telkomsigma.app.model.web.logs.StoreAndForward;

public interface StoreAndForwardDao  extends JpaRepository<StoreAndForward, UUID> {		 

	StoreAndForward findById(UUID id);
	
}
