
package id.co.telkomsigma.app.web.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.telkomsigma.app.model.web.user.User;

import java.util.List;

import javax.transaction.Transactional;

/**
 * Created by daniel on 3/31/15.
 */
public interface UserDao extends JpaRepository<User, String> {

    /**
     * Count user by username / email address
     * @param keyword
     * @return number of user
     */
    int countByUsernameLikeOrEmailLikeOrNameLikeAllIgnoreCase(String username,String email,String name);

    /**
     * Get user by username / email address with given role
     * @param keyword
     * @param authority
     * @return number of user
     */
    @Query("SELECT COUNT(DISTINCT u) FROM User u INNER JOIN u.roles r WHERE (LOWER(u.username) LIKE :keyword OR LOWER(u.email) LIKE :keyword OR LOWER(u.name) LIKE :keyword) AND r.authority = :authority")
    int countUser(@Param("keyword") String keyword, @Param("authority") String authority);

    /**
     * Get single user by email
     * @param email
     * @return single User object
     */
    User findByEmailIgnoreCase(String email);

    /**
     * Get single user by username
     * @param username
     * @return single User object
     */   
    User findByUsername(String username);
    
    
    User findByUserId(Long userId);

    /**
     * Get single user by username based on specific role
     * @param username
     * @param authority
     * @return single User object
     */
    @Query("SELECT new User(u) FROM User u INNER JOIN u.roles r WHERE LOWER(u.username) = :username AND r.authority = :authority")
    User getUserByUsername(@Param("username") String username, @Param("authority") String authority);

    /**
     * Get user by username / email address from offset as much limit defined in pageable param
     * @param keyword
     * @param pageable
     * @return list of User object
     */
    Page<User> findByUsernameLikeOrEmailLikeOrNameLikeAllIgnoreCase(String username,String email,String name, Pageable pageable);

    /**
     * Get user by username / email address with given role from offset as much limit defined in pageable param
     * @param keyword
     * @param authority
     * @param pageable
     * @return list of User object
     */
    //@Query("SELECT DISTINCT u FROM User u INNER JOIN u.roles r WHERE (LOWER(u.username) = :keyword OR LOWER(u.email) LIKE :keyword) AND r.authority = :authorityC")
    @Query("SELECT DISTINCT u FROM User u INNER JOIN u.roles r WHERE (LOWER(u.username) LIKE :keyword OR LOWER(u.email) LIKE :keyword OR LOWER(u.name) LIKE :keyword) AND r.authority = :authority")
    Page<User> getUsers(@Param("keyword") String keyword, @Param("authority") String authority, Pageable pageable);
   
	List<User> findByAccountNonLocked(Boolean accountNonLocked);
 
    @Modifying
    @Transactional
    @Query("UPDATE User u set u.accountNonLocked = :accountNonLocked , u.raw = :raw, u.failedLogin = :count WHERE u.userId = :userId")
    Integer failedLoginAttempt(@Param("userId")Long userId,@Param("raw")String raw,@Param("accountNonLocked") Boolean accountNonLocked, @Param("count") Long count);
    
}