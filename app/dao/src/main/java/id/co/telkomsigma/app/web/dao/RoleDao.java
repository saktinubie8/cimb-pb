package id.co.telkomsigma.app.web.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import id.co.telkomsigma.app.model.web.user.Role;

/**
 * Created by daniel on 4/16/15.
 */
public interface RoleDao extends JpaRepository<Role, String> {
    /**
     * Get all roles
     * @return list of Role object
     */       
    int countByAuthorityLikeIgnoreCase(String authority);
    

    //get Role by Role Id, return role
    Role findByRoleId(Long roleId);
    
    Page<Role> findByAuthorityLikeIgnoreCase(String authority, Pageable pageable);
    
    //get Role by Role Name, return role
    Role findByAuthority(String authority);
    
}