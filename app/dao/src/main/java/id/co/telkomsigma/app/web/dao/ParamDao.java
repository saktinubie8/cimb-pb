package id.co.telkomsigma.app.web.dao;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.telkomsigma.app.model.web.parameter.SystemParameter;

public interface ParamDao extends JpaRepository<SystemParameter, String>{
		    
	SystemParameter findByParameterId(Long paramId);
        
    int countByParamNameLikeOrParamDescriptionLikeAllIgnoreCase(String paramName,String paramDescription);
       
    @Query("SELECT COUNT(DISTINCT p) FROM SystemParameter p WHERE (LOWER(p.paramName) LIKE :keyword OR LOWER(p.paramDescription) LIKE :keyword) AND p.paramGroup = :paramGroup")
    int countParam(@Param("keyword") String keyword, @Param("paramGroup") String paramGroup);
    
    Page<SystemParameter> findByParamNameLikeOrParamDescriptionLikeAllIgnoreCase(String paramName,String paramDescription, Pageable pageable);
    
    @Query("SELECT DISTINCT p FROM SystemParameter p WHERE (LOWER(p.paramName) LIKE :keyword OR LOWER(p.paramDescription) LIKE :keyword) AND p.paramGroup = :paramGroup ")
    Page<SystemParameter> getParams(@Param("keyword") String keyword, @Param("paramGroup") String paramGroup, Pageable pageable);

	SystemParameter findByParamName(String paramName);
}
