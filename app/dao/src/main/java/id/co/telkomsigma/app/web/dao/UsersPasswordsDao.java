package id.co.telkomsigma.app.web.dao;


import java.util.List;

import id.co.telkomsigma.app.model.web.user.User;
import id.co.telkomsigma.app.model.web.user.UsersPasswords;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UsersPasswordsDao  extends JpaRepository<UsersPasswords, Long>{

	List<UsersPasswords> findTop4ByUserIdOrderByCreatedDateDesc (User user);
	
	List<UsersPasswords>  findByUserIdOrderByCreatedDateDesc(User user);
	
	/*@Query(value = "SELECT * from m_users_passwords up join m_users u on up.user_id = u.id where u.id :userId order by up.datetime desc", nativeQuery = true)
	List<UsersPasswords> findNUserId(@Param("userId") Long userId);
			//, @Param("record") Long record);
*/	
	
	List<UsersPasswords> findByUserId(User user);
	
}
