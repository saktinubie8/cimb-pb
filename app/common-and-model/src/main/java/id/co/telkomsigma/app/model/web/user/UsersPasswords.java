package id.co.telkomsigma.app.model.web.user;

import java.util.Date;

import id.co.telkomsigma.app.model.web.GenericModel;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Column;


	
@Entity
@Table(name = "m_users_passwords")	
public class UsersPasswords extends GenericModel  {
	private static final long serialVersionUID = -2442773369159964802L;
	/***************************** - Field - ****************************/
	private Long userPwdId;
	private User userId;
	private String password;
	private Date createdDate; 
	
	@Id
	@Column(name = "ID", nullable = false, unique = true, columnDefinition = "serial")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getUserPwdId() {
		return userPwdId;
	}
	public void setUserPwdId(Long userPwdId) {
		this.userPwdId = userPwdId;
	}
	
	@ManyToOne(targetEntity = User.class, fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", nullable = false)
	public User getUserId() {
		return userId;
	}
	public void setUserId(User userId) {
		this.userId = userId;
	}
	
	@Column(name = "password")
	public String getPassword() {
		return password;
	}
		public void setPassword(String password) {
		this.password = password;
	}
	@Column(name = "datetime")			
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	
}
