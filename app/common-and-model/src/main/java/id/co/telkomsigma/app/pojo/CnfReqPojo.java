package id.co.telkomsigma.app.pojo;

import java.util.Date;

public class CnfReqPojo {

	private String extRef; 
	private String relRef; 
	private String benAC;
	private String memID;
	private String memAC;
	private String curCod; 
	private String valDate;
	private String cashVal;
	private String notes;
	
	public String getExtRef() {
		return extRef;
	}
	public void setExtRef(String extRef) {
		this.extRef = extRef;
	}
	public String getRelRef() {
		return relRef;
	}
	public void setRelRef(String relRef) {
		this.relRef = relRef;
	}
	public String getBenAC() {
		return benAC;
	}
	public void setBenAC(String benAC) {
		this.benAC = benAC;
	}
	public String getMemID() {
		return memID;
	}
	public void setMemID(String memID) {
		this.memID = memID;
	}
	public String getMemAC() {
		return memAC;
	}
	public void setMemAC(String memAC) {
		this.memAC = memAC;
	}
	public String getCurCod() {
		return curCod;
	}
	public void setCurCod(String curCod) {
		this.curCod = curCod;
	}
	public String getValDate() {
		return valDate;
	}
	public void setValDate(String valDate) {
		this.valDate = valDate;
	}
	public String getCashVal() {
		return cashVal;
	}
	public void setCashVal(String cashVal) {
		this.cashVal = cashVal;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	@Override
	public String toString() {
		return "CnfReqPojo [extRef=" + extRef + ", relRef=" + relRef + ", benAC=" + benAC + ", memID=" + memID
				+ ", memAC=" + memAC + ", curCod=" + curCod + ", valDate=" + valDate + ", cashVal=" + cashVal
				+ ", notes=" + notes + "]";
	}
	
	
	
}
