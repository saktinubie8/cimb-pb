package id.co.telkomsigma.app.util;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(content = Include.NON_NULL)
public class ResponseCode {

	private String rc;
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	private List<PositionList> positionList;

	private String sessionKey;

	private String locationCode;
	private String locationName;
	private String locationAddress;
	private String locationType;
	private String locationPicture;
	private String url;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	private String maxGpsAccuracy;

	private String currentVersion;

	private List<String> allowedVersions;

	public String getCurrentVersion() {
		return currentVersion;
	}

	public List<String> getAllowedVersions() {
		return allowedVersions;
	}

	public void setCurrentVersion(String currentVersion) {
		this.currentVersion = currentVersion;
	}

	public void setAllowedVersions(List<String> allowedVersions) {
		this.allowedVersions = allowedVersions;
	}

	public String getMaxGpsAccuracy() {
		return maxGpsAccuracy;
	}

	public void setMaxGpsAccuracy(String maxGpsAccuracy) {
		this.maxGpsAccuracy = maxGpsAccuracy;
	}

	public String getLocationCode() {
		return locationCode;
	}

	public String getLocationName() {
		return locationName;
	}

	public String getLocationAddress() {
		return locationAddress;
	}

	public String getLocationType() {
		return locationType;
	}

	public String getLocationPicture() {
		return locationPicture;
	}

	public void setLocationCode(String locationCode) {
		this.locationCode = locationCode;
	}

	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	public void setLocationAddress(String locationAddress) {
		this.locationAddress = locationAddress;
	}

	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}

	public void setLocationPicture(String locationPicture) {
		this.locationPicture = locationPicture;
	}

	public String getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

	public String getRc() {
		return rc;
	}

	public void setRc(String rc) {
		this.rc = rc;
	}

	public List<PositionList> getPositionList() {
		return positionList;
	}

	public void setPositionList(List<PositionList> positionList) {
		this.positionList = positionList;
	}

}
