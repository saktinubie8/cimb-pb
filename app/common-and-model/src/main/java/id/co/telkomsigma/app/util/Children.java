package id.co.telkomsigma.app.util;

import java.util.ArrayList;
import java.util.List;

public class Children {
	private String initId;
	private String title;
	private String icon;
	private String address;

	private String circleColor;
	private String pathColor;

	private String imagePath;

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getCircleColor() {
		return circleColor;
	}

	public String getPathColor() {
		return pathColor;
	}

	public void setCircleColor(String circleColor) {
		this.circleColor = circleColor;
	}

	public void setPathColor(String pathColor) {
		this.pathColor = pathColor;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	private List<Children> children = new ArrayList<>();
	private Boolean isLazy;

	private String longitude;
	private String latitude;

	private String parentId;

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String lattitude) {
		this.latitude = lattitude;
	}

	public Boolean getIsLazy() {
		return isLazy;
	}

	public void setIsLazy(Boolean isLazy) {
		this.isLazy = isLazy;
	}

	public String getInitId() {
		return initId;
	}

	public void setInitId(String initId) {
		this.initId = initId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<Children> getChildren() {
		return children;
	}

	public void setChildren(List<Children> children) {
		this.children = children;
	}
}
