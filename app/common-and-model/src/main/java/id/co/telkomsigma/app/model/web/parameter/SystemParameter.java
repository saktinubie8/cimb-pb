package id.co.telkomsigma.app.model.web.parameter;

import id.co.telkomsigma.app.model.web.GenericModel;
import id.co.telkomsigma.app.model.web.user.User;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "m_parameter")
public class SystemParameter extends GenericModel {

	private Long parameterId;
	private String paramGroup;
	private String paramName;
	private String paramValue;
	private String paramDescription;

	private User createdBy;
	private Date createdDate;
	private User updatedBy;
	private Date updatedDate;
	
	private char paramType;
	

	@Id
	@Column(name = "ID", nullable = false, unique = true, columnDefinition = "serial")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getParameterId() {
		return parameterId;
	}

	@Column(name = "PARAM_GROUP", nullable = false, length = MAX_LENGTH_PARAM_GROUP)
	public String getParamGroup() {
		return paramGroup;
	}

	@Column(name = "PARAM_NAME", nullable = false, length = MAX_LENGTH_PARAM_NAME)
	public String getParamName() {
		return paramName;
	}

	@Column(name = "PARAM_VALUE", nullable = false, columnDefinition = "text")
	public String getParamValue() {
		return paramValue;
	}

	@Column(name = "PARAM_DESCRIPTION", nullable = false, columnDefinition = "text")
	public String getParamDescription() {
		return paramDescription;
	}

	@ManyToOne(targetEntity = User.class)
	@JoinColumn(name = "CREATED_BY", nullable = false)
	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "CREATED_DT", nullable = false)
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@ManyToOne(targetEntity = User.class)
	@JoinColumn(name = "UPDATE_BY", nullable = false)
	public User getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(User updateBy) {
		this.updatedBy = updateBy;
	}

	@Column(name = "UPDATE_DT", nullable = false)
	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updateDate) {
		this.updatedDate = updateDate;
	}

	public void setParameterId(Long parameterId) {
		this.parameterId = parameterId;
	}

	public void setParamGroup(String paramGroup) {
		this.paramGroup = paramGroup;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	public void setParamDescription(String paramDescription) {
		this.paramDescription = paramDescription;
	}

	@Column(name = "PARAM_TYPE")
	public char getParamType() {
		return paramType;
	}

	public void setParamType(char paramType) {
		this.paramType = paramType;
	}

	@Override
	public String toString() {
		return "SystemParameter [parameterId=" + parameterId + ", paramGroup=" + paramGroup + ", paramName=" + paramName
				+ ", paramValue=" + paramValue + ", paramDescription=" + paramDescription + ", createdBy=" + createdBy
				+ ", createdDate=" + createdDate + ", updatedBy=" + updatedBy + ", updatedDate=" + updatedDate
				+ ", paramType=" + paramType + "]";
	}
	
	

}
