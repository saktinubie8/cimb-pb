package id.co.telkomsigma.app.pojo;

import java.util.Date;

public class PayordReqPojo {

	private HeaderReqPojo header; 
	private String 	memAC;
	private String 	extRef;
	private String 	curCod;
	private String 	benAC;
	private String 	valDate;
	private String 	cashVal;
	private String 	notes;
	private String 	memID;
	
	public HeaderReqPojo getHeader() {
		return header;
	}
	public void setHeader(HeaderReqPojo header) {
		this.header = header;
	}
	public String getMemAC() {
		return memAC;
	}
	public void setMemAC(String memAC) {
		this.memAC = memAC;
	}
	public String getExtRef() {
		return extRef;
	}
	public void setExtRef(String extRef) {
		this.extRef = extRef;
	}
	public String getCurCod() {
		return curCod;
	}
	public void setCurCod(String curCod) {
		this.curCod = curCod;
	}
	public String getBenAC() {
		return benAC;
	}
	public void setBenAC(String benAC) {
		this.benAC = benAC;
	}
	public String getValDate() {
		return valDate;
	}
	public void setValDate(String valDate) {
		this.valDate = valDate;
	}
	public String getCashVal() {
		return cashVal;
	}
	public void setCashVal(String cashVal) {
		this.cashVal = cashVal;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getMemID() {
		return memID;
	}
	public void setMemID(String memID) {
		this.memID = memID;
	}
	@Override
	public String toString() {
		return "PayordReqPojo [memAC=" + memAC + ", extRef=" + extRef + ", curCod=" + curCod + ", benAC=" + benAC
				+ ", valDate=" + valDate + ", cashVal=" + cashVal + ", notes=" + notes + ", memID=" + memID + "]";
	}
	
	
	
	
}
