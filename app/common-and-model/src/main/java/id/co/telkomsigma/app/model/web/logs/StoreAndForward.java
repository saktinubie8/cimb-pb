package id.co.telkomsigma.app.model.web.logs;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "T_STORE_AND_FORWARD")
public class StoreAndForward {
	
	@Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(name = "ID", nullable = false, unique = true, columnDefinition = "BINARY(16)")	
	private UUID id;
	
	@Column(name = "CREATED_DATE", nullable = false)
	private Date createdDate;
	
	@Column(name = "DESTINATION", nullable = false)
	private String destination;
	
	@Column(name = "RAW_REQUEST", nullable = false)
	private String rawRequest;
	
	@Column(name = "LAST_RETRY", nullable = true)
	private Date lastRetry;
	
	@Column(name = "LAST_ERROR", nullable = true)
	private String lastError;
	
	@Column(name = "RETRY_COUNT", nullable = false)
	private Long retryCount;
	
	@Column(name = "STAN", nullable = true)
	private String stan;
	
	@Column(name = "TRANSACTION_ID", nullable = false, columnDefinition = "BINARY(16)")
	private UUID transactionId;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getRawRequest() {
		return rawRequest;
	}

	public void setRawRequest(String rawRequest) {
		this.rawRequest = rawRequest;
	}

	public Date getLastRetry() {
		return lastRetry;
	}

	public void setLastRetry(Date lastRetry) {
		this.lastRetry = lastRetry;
	}

	public String getLastError() {
		return lastError;
	}

	public void setLastError(String lastError) {
		this.lastError = lastError;
	}

	public Long getRetryCount() {
		return retryCount;
	}

	public void setRetryCount(Long retryCount) {
		this.retryCount = retryCount;
	}

	public String getStan() {
		return stan;
	}

	public void setStan(String stan) {
		this.stan = stan;
	}

	public UUID getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(UUID transactionId) {
		this.transactionId = transactionId;
	}
	
	
	
}