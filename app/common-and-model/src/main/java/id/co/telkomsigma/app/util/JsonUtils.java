package id.co.telkomsigma.app.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.type.TypeReference;



@Component
public class JsonUtils {
	ObjectMapper objectMapper;
	

	public JsonUtils() {
		this.objectMapper = new ObjectMapper();
	}

	public String toJson(Object obj) throws IOException, JsonMappingException, JsonGenerationException {
		return this.objectMapper.writeValueAsString(obj);
	}

	public <T> Object fromJson(String json, Class<T> cls) throws IOException, JsonParseException {
		return this.objectMapper.readValue(json, cls);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Map<String, Object> jsonToMapOfObject(String json) {
		Map<String, Object> mapto = new HashMap();
		try {
			mapto = objectMapper.readValue(json, new TypeReference<Map<String, Object>>() {
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mapto;
	}
}
