package id.co.telkomsigma.app.util;

import java.util.Date;

public class PositionList {

	private Date dateTime;
	private String longitude;
	private String latitude;
	private String gpsAccuracy;

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getGpsAccuracy() {
		return gpsAccuracy;
	}

	public void setGpsAccuracy(String gpsAccuracy) {
		this.gpsAccuracy = gpsAccuracy;
	}

}
