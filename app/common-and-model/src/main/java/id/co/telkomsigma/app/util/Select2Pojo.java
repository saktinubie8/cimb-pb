package id.co.telkomsigma.app.util;

public class Select2Pojo {
	private String id;
	private String text;

	public String getId() {
		return id;
	}

	public String getText() {
		return text;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setText(String text) {
		this.text = text;
	}

}
