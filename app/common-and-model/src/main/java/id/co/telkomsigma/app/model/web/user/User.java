package id.co.telkomsigma.app.model.web.user;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import id.co.telkomsigma.app.model.web.GenericModel;
import id.co.telkomsigma.app.util.PropertiesUtil;

import javax.persistence.*;

import java.util.*;

/**
 * Created by daniel on 3/31/15.
 */
@Entity
@Table(name = "m_users")
public class User extends GenericModel implements UserDetails {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2442773369159964802L;
	/***************************** - Field - ****************************/
	private Long userId;
	private String username;
	private String password;
	private boolean enabled = false;
	private boolean accountNonExpired = true;
	private boolean accountNonLocked = true;
	private boolean credentialsNonExpired = true;
	private Set<Role> roles = new HashSet<Role>();
	private List<String> strRoles = new ArrayList<String>();
	private String name;
	private String email;
	private String address;
	private String phone;
	private String phoneMobile;
	private String description;
	private String verificationCode;
	private Long createdBy;
	private Date createdDate;
	private Long updatedBy;
	private Date updatedDate;
	private String raw;
	private String jobTitle;
	private String extension;
	private String fax;
	private String level;
	private String city;
    private Date lastUpdatePassword;
	private Long failedLogin;
	/*****************************
	 * - Transient Field -
	 ****************************/
	private String passwordConfirm;
	private String passwordOld;
	private String captcha;
	private String enabledStr;
	private String accountNonLockedStr;
	private Properties prop;
	private boolean matchDefault;
	private boolean expiryDays;
	private Integer changePasswordCount;

	/***************************** - Constructor - ****************************/
	public User() {
	}

	public User(String username) {
		this.username = username;
	}

	public User(User user) {
		setUsername(user.getUsername());
		setPassword(user.getPassword());
		setEnabled(user.isEnabled());
		setAccountNonExpired(user.isAccountNonExpired());
		setAccountNonLocked(user.isAccountNonLocked());
		setCredentialsNonExpired(user.isCredentialsNonExpired());
		setRoles(user.getRoles());
		setName(user.getName());
		setEmail(user.getEmail());
		setAddress(user.getAddress());
		setPhone(user.getPhone());
		setPhoneMobile(user.getPhoneMobile());
		setDescription(user.getDescription());
		setVerificationCode(user.getVerificationCode());
		setCreatedBy(user.getCreatedBy());
		setCreatedDate(user.getCreatedDate());
		setUpdatedBy(user.getUpdatedBy());
		setUpdatedDate(user.getUpdatedDate());
		setLastUpdatePassword(user.getLastUpdatePassword());
	}

	/*****************************
	 * - Getter Method -
	 ****************************/
	@Id
	@Column(name = "ID", nullable = false, unique = true, columnDefinition = "serial")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getUserId() {
		return userId;
	}

	@Override
	@Column(name = "USER_LOGIN", length = MAX_LENGTH_USERNAME, nullable = false, unique = true)
	public String getUsername() {
		return username;
	}

	@Override
	@Column(name = "PASSWORD", nullable = false, unique = false)
	public String getPassword() {
		return password;
	}

	@Override
	@Column(name = "ACCOUNT_NON_EXPIRED", nullable = false, columnDefinition = "boolean")
	public boolean isAccountNonExpired() {
		return accountNonExpired;
	}

	@Override
	@Column(name = "ACCOUNT_NON_LOCKED", nullable = false, columnDefinition = "boolean")
	public boolean isAccountNonLocked() {
		return accountNonLocked;
	}

	@Override
	@Column(name = "CREDENTIAL_NON_EXPIRED", nullable = false, columnDefinition = "boolean")
	public boolean isCredentialsNonExpired() {
		return credentialsNonExpired;
	}

	@Override
	@Column(name = "ACCOUNT_ENABLED", nullable = false, columnDefinition = "boolean")
	public boolean isEnabled() {
		return enabled;
	}

	@ManyToMany(fetch = FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	@JoinTable(name = "m_user_roles", joinColumns = {
			@JoinColumn(name = "userId") }, inverseJoinColumns = @JoinColumn(name = "roleId"))
	public Set<Role> getRoles() {
		return roles;
	}

	@Column(name = "FULL_NAME", length = MAX_LENGTH_NAME, nullable = false)
	public String getName() {
		return name;
	}

	@Column(name = "EMAIL", length = MAX_LENGTH_EMAIL, nullable = true, unique = true)
	public String getEmail() {
		return email;
	}

	@Column(name = "ADDRESS", length = MAX_LENGTH_ADDRESS, nullable = false)
	public String getAddress() {
		return address;
	}

	@Column(name = "PHONE_NUMBER", length = MAX_LENGTH_PHONE, nullable = true)
	public String getPhone() {
		return phone;
	}

	@Column(name = "MOBILE_NUMBER", length = MAX_LENGTH_PHONE_MOBILE, nullable = true)
	public String getPhoneMobile() {
		return phoneMobile;
	}

	@Column(name = "DESCRIPTION", length = MAX_LENGTH_DESCRIPTION)
	public String getDescription() {
		return description;
	}

	@Column(name = "VERIFICATION_CODE", length = MAX_LENGTH_VERIFICATION_CODE)
	public String getVerificationCode() {
		return verificationCode;
	}

	@Column(name = "CREATED_BY")
	public Long getCreatedBy() {
		return createdBy;
	}

	@Column(name = "CREATED_DT")
	public Date getCreatedDate() {
		return createdDate;
	}

	@Column(name = "UPDATE_BY")
	public Long getUpdatedBy() {
		return updatedBy;
	}

	@Column(name = "UPDATE_DT")
	public Date getUpdatedDate() {
		return updatedDate;
	}

	/*****************************
	 * - Setter Method -
	 ****************************/
	public void setUserId(Long id) {
		this.userId = id;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public void setAccountNonExpired(boolean accountNonExpired) {
		this.accountNonExpired = accountNonExpired;
	}

	public void setAccountNonLocked(boolean accountNonLocked) {
		this.accountNonLocked = accountNonLocked;
	}

	public void setCredentialsNonExpired(boolean credentialsNonExpired) {
		this.credentialsNonExpired = credentialsNonExpired;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setPhoneMobile(String phoneMobile) {
		this.phoneMobile = phoneMobile;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	/*****************************
	 * - Other Method -
	 ****************************/
	@Override
	@Transient
	public Set<GrantedAuthority> getAuthorities() {
		Set<GrantedAuthority> authorities = new LinkedHashSet<GrantedAuthority>();
		authorities.addAll(roles);
		return authorities;
	}

	@Transient
	public String getPasswordConfirm() {
		return passwordConfirm;
	}

	public void setPasswordConfirm(String passwordConfirm) {
		this.passwordConfirm = passwordConfirm;
	}

	@Transient
	public String getCaptcha() {
		return captcha;
	}

	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}

	public void addRole(Role role) {
		if (roles == null) {
			roles = new HashSet<Role>();
		}
		roles.add(role);
	}

	public void setEnabled() {
		this.enabled = enabledStr.equalsIgnoreCase("1");
	}

	@Transient
	public String getEnabledStr() {
		return enabledStr;
	}

	public void setEnabledStr(String enabledStr) {
		this.enabledStr = enabledStr;
	}

	public void setEnabledStr() {
		if (enabled) {
			enabledStr = "1";
		} else {
			enabledStr = "0";
		}
	}

	public void setAccountNonLocked() {
		this.accountNonLocked = accountNonLockedStr.equalsIgnoreCase("1");
	}

	@Transient
	public String getAccountNonLockedStr() {
		return accountNonLockedStr;
	}

	public void setAccountNonLockedStr(String accountNonLockedStr) {
		this.accountNonLockedStr = accountNonLockedStr;
	}

	public void setAccountNonLockedStr() {
		if (accountNonLocked) {
			accountNonLockedStr = "1";
		} else {
			accountNonLockedStr = "0";
		}
	}

	@Transient
	public List<String> getStrRoles() {
		return strRoles;
	}

	public void setStrRoles(List<String> strRoles) {
		this.strRoles = strRoles;
	}

	@Column(name = "RAW", nullable = true, columnDefinition = "text")
	public String getRaw() {
		if (prop == null)
			prop = new Properties();
		raw = PropertiesUtil.toString(prop);
		return raw;
	}

	public void setRaw(String raw) {
		this.raw = raw;
		prop = PropertiesUtil.fromString(raw);
	}

	@Transient
	public Properties getProp() {
		return prop;
	}

	public void setProp(Properties prop) {
		this.prop = prop;
	}

	@Transient
	public String getCustomProp(String key) {
		if (prop == null)
			prop = new Properties();
		return (String) prop.get(key);
	}

	public void setCustomProp(String key, String value) {
		if (prop == null)
			prop = new Properties();
		prop.put(key, value);
		getRaw();
	}

	@Column(name = "JOB_TITLE", length = 100)
	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	@Column(name = "EXTENSION_NUMBER", length = 12)
	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	@Column(name = "FAX_NUMBER", length = 12)
	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	@Column(name = "LEVEL", length = 20)
	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	@Transient
	public String getPasswordOld() {
		return passwordOld;
	}

	public void setPasswordOld(String passwordOld) {
		this.passwordOld = passwordOld;
	}

	@Column(name = "CITY")
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Column(name = "UPDATE_DT_PASSWORD", columnDefinition = "timestamp without time zone")
	public Date getLastUpdatePassword() {
		return lastUpdatePassword;
	}

	public void setLastUpdatePassword(Date lastUpdatePassword) {
		this.lastUpdatePassword = lastUpdatePassword;
	}
	
	@Column(name = "failed_login")
	public Long getFailedLogin() {
		return failedLogin;
	}

	public void setFailedLogin(Long failedLogin) {
		this.failedLogin = failedLogin;
	}

	@Column(name = "change_password_history")
	public Integer getChangePasswordCount() {
		return changePasswordCount;
	}

	public void setChangePasswordCount(Integer changePasswordCount) {
		this.changePasswordCount = changePasswordCount;
	}

	@Transient
	public Boolean getMatchDefault() {
		return matchDefault;
	}

	public void setMatchDefault(Boolean matchDefault) {
		this.matchDefault = matchDefault;
	}
	
	@Transient
	public Boolean getExpiryDays() {
		return expiryDays;
	}

	public void setExpiryDays(Boolean expiryDays) {
		this.expiryDays = expiryDays;
	}
	
}