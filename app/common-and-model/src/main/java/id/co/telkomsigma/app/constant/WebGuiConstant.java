package id.co.telkomsigma.app.constant;

/**
 * Created by daniel on 4/17/15.
 */
public final class WebGuiConstant {
	/**
	 * URL CAPTCHA
	 */
	private static final String CAPTCHA_PATH = "/captcha";
	public static final String CAPTCHA_REGISTRATION_PATH_MAPPING = CAPTCHA_PATH + "-registration*";
	public static final String CAPTCHA_RESEND_ACTIVATION_LINK_PATH_MAPPING = CAPTCHA_PATH + "-resend-activation*";
	public static final String CAPTCHA_RESET_PASSWORD_PATH_MAPPING = CAPTCHA_PATH + "-reset-password*";
	public static final String CAPTCHA_PATH_SECURITY_MAPPING = CAPTCHA_PATH + "**";

	/**
	 * URL GLOBAL
	 */
	private static final String AFTER_LOGIN_PATH = "/home";
	public static final String AFTER_LOGIN_PATH_HOME_PAGE = "/home";
	public static final String AFTER_LOGIN_PATH_SECURITY_MAPPING = AFTER_LOGIN_PATH + "/**";
	public static final String AFTER_LOGIN_PATH_PROFILE_PAGE = AFTER_LOGIN_PATH + "/profile";
	public static final String AFTER_LOGIN_PATH_PARAM_PAGE = "/home/param";
	public static final String AFTER_LOGIN_PATH_HOME_GET_LIST_TERMINAL = AFTER_LOGIN_PATH + "/list*";

	/**
	 * URL DASHBOARD
	 */
	public static final String AFTER_LOGIN_PATH_MAP_DASHBOARD = AFTER_LOGIN_PATH + "/dashboard";
	public static final String AFTER_LOGIN_PATH_MAP_DASHBOARD_REQUEST = AFTER_LOGIN_PATH_MAP_DASHBOARD + "*";
	public static final String AFTER_LOGIN_PATH_MAP_DASHBOARD_BARCHART = AFTER_LOGIN_PATH_MAP_DASHBOARD + "/barchart*";

	/**
	 * URL AUDIT LOG
	 */
	public static final String AFTER_LOGIN_PATH_AUDIT_LOG = AFTER_LOGIN_PATH + "/auditLog";

	public static final String AFTER_LOGIN_PATH_AUDIT_LOG_MAPPING = AFTER_LOGIN_PATH_AUDIT_LOG + "*";
	public static final String AFTER_LOGIN_PATH_AUDIT_LOG_GET_LIST_MAPPING = AFTER_LOGIN_PATH_AUDIT_LOG + "/get-list*";
	public static final String AFTER_LOGIN_PATH_AUDIT_LOG_SEARCH_MAPPING = AFTER_LOGIN_PATH_AUDIT_LOG + "/search*";

	
	
	
	/**
	 * URL USER
	 */

	public static final String AFTER_LOGIN_PATH_MANAGE_USER = AFTER_LOGIN_PATH + "/manage/user";
	public static final String AFTER_LOGIN_PATH_MANAGE_USER_MAPPING = AFTER_LOGIN_PATH_MANAGE_USER + "*";
	public static final String AFTER_LOGIN_PATH_MANAGE_USER_GET_LIST_MAPPING = AFTER_LOGIN_PATH_MANAGE_USER
			+ "/get-list*";
	private static final String AFTER_LOGIN_PATH_MANAGE_USER_ADD = AFTER_LOGIN_PATH_MANAGE_USER + "/add";
	public static final String AFTER_LOGIN_PATH_MANAGE_USER_ADD_MAPPING = AFTER_LOGIN_PATH_MANAGE_USER_ADD + "*";
	public static final String AFTER_LOGIN_PATH_MANAGE_USER_ADD_REQUEST = AFTER_LOGIN_PATH_MANAGE_USER_ADD
			+ "?userId={userId}";
	private static final String AFTER_LOGIN_PATH_MANAGE_USER_DELETE = AFTER_LOGIN_PATH_MANAGE_USER + "/delete";
	public static final String AFTER_LOGIN_PATH_MANAGE_USER_DELETE_MAPPING = AFTER_LOGIN_PATH_MANAGE_USER_DELETE + "*";
	public static final String AFTER_LOGIN_PATH_MANAGE_USER_DELETE_REQUEST = AFTER_LOGIN_PATH_MANAGE_USER_DELETE
			+ "?userId={userId}";
	private static final String AFTER_LOGIN_PATH_MANAGE_USER_LOCK = AFTER_LOGIN_PATH_MANAGE_USER + "/lock";
	public static final String AFTER_LOGIN_PATH_MANAGE_USER_LOCK_MAPPING = AFTER_LOGIN_PATH_MANAGE_USER_LOCK + "*";
	public static final String AFTER_LOGIN_PATH_MANAGE_USER_LOCK_REQUEST = AFTER_LOGIN_PATH_MANAGE_USER_LOCK
			+ "?userId={userId}";
	private static final String AFTER_LOGIN_PATH_MANAGE_USER_UNLOCK = AFTER_LOGIN_PATH_MANAGE_USER + "/unlock";
	public static final String AFTER_LOGIN_PATH_MANAGE_USER_UNLOCK_MAPPING = AFTER_LOGIN_PATH_MANAGE_USER_UNLOCK + "*";
	public static final String AFTER_LOGIN_PATH_MANAGE_USER_UNLOCK_REQUEST = AFTER_LOGIN_PATH_MANAGE_USER_UNLOCK
			+ "?userId={userId}";
	private static final String AFTER_LOGIN_PATH_MANAGE_USER_ENABLE = AFTER_LOGIN_PATH_MANAGE_USER + "/enable";
	public static final String AFTER_LOGIN_PATH_MANAGE_USER_ENABLE_MAPPING = AFTER_LOGIN_PATH_MANAGE_USER_ENABLE + "*";
	public static final String AFTER_LOGIN_PATH_MANAGE_USER_ENABLE_REQUEST = AFTER_LOGIN_PATH_MANAGE_USER_ENABLE
			+ "?userId={userId}";
	private static final String AFTER_LOGIN_PATH_MANAGE_USER_DISABLE = AFTER_LOGIN_PATH_MANAGE_USER + "/disable";
	public static final String AFTER_LOGIN_PATH_MANAGE_USER_DISABLE_MAPPING = AFTER_LOGIN_PATH_MANAGE_USER_DISABLE
			+ "*";
	public static final String AFTER_LOGIN_PATH_MANAGE_USER_DISABLE_REQUEST = AFTER_LOGIN_PATH_MANAGE_USER_DISABLE
			+ "?userId={userId}";
	private static final String AFTER_LOGIN_PATH_MANAGE_USER_RESET = AFTER_LOGIN_PATH_MANAGE_USER + "/reset";
	public static final String AFTER_LOGIN_PATH_MANAGE_USER_RESET_MAPPING = AFTER_LOGIN_PATH_MANAGE_USER_RESET + "*";
	public static final String AFTER_LOGIN_PATH_MANAGE_USER_RESET_REQUEST = AFTER_LOGIN_PATH_MANAGE_USER_RESET
			+ "?userId={userId}";
	public static final String AFTER_LOGIN_PATH_MANAGE_USER_AJAX_SEARCH_LOCATION_MAPPING = AFTER_LOGIN_PATH_MANAGE_USER_ADD
			+ "/ajax/location*";

	public static final String AFTER_LOGIN_PATH_MANAGE_PARAM = AFTER_LOGIN_PATH + "/manage/parameter";
	public static final String AFTER_LOGIN_PATH_MANAGE_PARAM_MAPPING = AFTER_LOGIN_PATH_MANAGE_PARAM + "*";
	public static final String AFTER_LOGIN_PATH_MANAGE_PARAM_GET_LIST_MAPPING = AFTER_LOGIN_PATH_MANAGE_PARAM
			+ "/get-list*";
	private static final String AFTER_LOGIN_PATH_MANAGE_PARAM_EDIT = AFTER_LOGIN_PATH_MANAGE_PARAM + "/edit";
	public static final String AFTER_LOGIN_PATH_MANAGE_PARAM_EDIT_MAPPING = AFTER_LOGIN_PATH_MANAGE_PARAM_EDIT + "*";
	public static final String AFTER_LOGIN_PATH_MANAGE_PARAM_EDIT_REQUEST = AFTER_LOGIN_PATH_MANAGE_PARAM_EDIT
			+ "?paramId={paramId}";

	/**
	 * URL ROLE
	 */
	public static final String AFTER_LOGIN_PATH_MANAGE_ROLE = AFTER_LOGIN_PATH + "/manage/role";
	public static final String AFTER_LOGIN_PATH_MANAGE_ROLE_MAPPING = AFTER_LOGIN_PATH_MANAGE_ROLE + "*";
	public static final String AFTER_LOGIN_PATH_MANAGE_ROLE_GET_LIST_MAPPING = AFTER_LOGIN_PATH_MANAGE_ROLE
			+ "/get-list*";
	private static final String AFTER_LOGIN_PATH_MANAGE_ROLE_ADD = AFTER_LOGIN_PATH_MANAGE_ROLE + "/add";
	public static final String AFTER_LOGIN_PATH_MANAGE_ROLE_ADD_MAPPING = AFTER_LOGIN_PATH_MANAGE_ROLE_ADD + "*";
	public static final String AFTER_LOGIN_PATH_MANAGE_ROLE_ADD_REQUEST = AFTER_LOGIN_PATH_MANAGE_ROLE_ADD
			+ "?roleId={roleId}";
	private static final String AFTER_LOGIN_PATH_MANAGE_ROLE_EDIT = AFTER_LOGIN_PATH_MANAGE_ROLE + "/edit";
	public static final String AFTER_LOGIN_PATH_MANAGE_ROLE_EDIT_MAPPING = AFTER_LOGIN_PATH_MANAGE_ROLE_EDIT + "*";
	public static final String AFTER_LOGIN_PATH_MANAGE_ROLE_EDIT_REQUEST = AFTER_LOGIN_PATH_MANAGE_ROLE_EDIT
			+ "?u={rolename}";
	private static final String AFTER_LOGIN_PATH_MANAGE_ROLE_DELETE = AFTER_LOGIN_PATH_MANAGE_ROLE + "/add";
	public static final String AFTER_LOGIN_PATH_MANAGE_ROLE_DELETE_MAPPING = AFTER_LOGIN_PATH_MANAGE_ROLE_DELETE + "*";
	public static final String AFTER_LOGIN_PATH_MANAGE_ROLE_DELETE_REQUEST = AFTER_LOGIN_PATH_MANAGE_ROLE_DELETE
			+ "?roleId={roleId}";

	/**
	 * URL SIMUL CNF
	 */
	public static final String AFTER_LOGIN_PATH_CNF = AFTER_LOGIN_PATH + "/simulator/cnf";
	public static final String AFTER_LOGIN_PATH_CNF_MAPPING = AFTER_LOGIN_PATH_CNF + "*";
	public static final String AFTER_LOGIN_PATH_CNF_SEND_PAGE = AFTER_LOGIN_PATH_CNF+ "/send*";
	
	
	/**
	 * URL SIMUL ACKCNF
	 */
	public static final String AFTER_LOGIN_PATH_ACKCNF = AFTER_LOGIN_PATH + "/simulator/ackcnf";
	public static final String AFTER_LOGIN_PATH_ACKCNF_MAPPING = AFTER_LOGIN_PATH_ACKCNF + "*";
	public static final String AFTER_LOGIN_PATH_ACKCNF_SEND_PAGE = AFTER_LOGIN_PATH_ACKCNF+ "/send*";
	
	/**
	 * URL SIMUL PAYORD
	 */
	public static final String AFTER_LOGIN_PATH_PAYORD = AFTER_LOGIN_PATH + "/simulator/payord";
	public static final String AFTER_LOGIN_PATH_PAYORD_MAPPING = AFTER_LOGIN_PATH_PAYORD + "*";
	public static final String AFTER_LOGIN_PATH_PAYORD_SEND_PAGE = AFTER_LOGIN_PATH_PAYORD+ "/send*";
	
	/**
	 * URL SIMUL DCNF
	 */
	public static final String AFTER_LOGIN_PATH_DCNF = AFTER_LOGIN_PATH + "/simulator/dcnf";
	public static final String AFTER_LOGIN_PATH_DCNF_MAPPING = AFTER_LOGIN_PATH_DCNF + "*";
	public static final String AFTER_LOGIN_PATH_DCNF_SEND_PAGE = AFTER_LOGIN_PATH_DCNF + "/send*";
	
	/**
	 * URL SIMUL DREF
	 */
	public static final String AFTER_LOGIN_PATH_DREF = AFTER_LOGIN_PATH + "/simulator/dref";
	public static final String AFTER_LOGIN_PATH_DREF_MAPPING = AFTER_LOGIN_PATH_DREF + "*";
	public static final String AFTER_LOGIN_PATH_DREF_SEND_PAGE = AFTER_LOGIN_PATH_DREF+ "/send*";
	
	
	/**
	 * SESSION CONSTANT
	 */
	public static final String SESSION_REGISTERED_USER = "registeredUser";
	public static final String SESSION_RESEND_ACTIVATION_LINK_USER = "resendActivationLinkUser";
	public static final String SESSION_REQUEST_RESET_PASSWORD_USER = "requestResetPasswordUser";
	public static final String SESSION_RESET_PASSWORD_USER = "resetPasswordUser";

	public static final String SESSION_CAPTCHA_REGISTRATION = "registeredUser";
	public static final String SESSION_CAPTCHA_REQUEST_RESET_PASSWORD = "requestResetPassword";
	public static final String SESSION_CAPTCHA_RESEND_ACTIVATION_LINK = "resendActivationLink";

	public static final String SESSION_EDIT_USER_ = "editUser_";

	/**
	 * MENU
	 */
	public static final String HEADER_AFTER_LOGIN_PATH = "/header";

	/**
	 * OTHER CONSTANT
	 */
	public static final long DEFAULT_VALUE_COUNTRY_CODE = 94;

	public static final String PARAMETER_NAME_MAX_FAILED_ATTEMPT = "security.max.failedAttempt";

	public static final String PARAMETER_MAX_LENGTH_PASSWORD = "maxLengthPass";
	public static final String PARAMETER_MAX_CHANGE_PASSWORD = "maxHistChangePassword";
	public static final String PARAMETER_DEFAULT_PASSWORD = "DefaultPassword";
	public static final String PARAMETER_MAX_FAILED_ATTEMPT_DEFAULT_VALUE = "5";
	public static final String PARAMETER_GENERAL_PASSWORD = "generalPwd";

	public static final String PARAMETER_NAME_LOCK_DURATION = "security.lock.duration";
	public static final String PARAMETER_LOCK_DURATION_DEFAULT_VALUE = "30";

	// custom parameter ditabel user kolom raw
	public static final String PARAMETER_USER_CURRENT_FAILED_ATTEMPT = "current.failedAttempt";
	public static final String PARAMETER_USER_CURRENT_LOCK_UNTIL = "lock.until";
	
	public static final String PARAMETER_PASSWORD_REST = "passwordRest";

	/**
	 * URL Cache
	 */
	public static final String AFTER_LOGIN_PATH_CACHE = AFTER_LOGIN_PATH + "/manage/cache";
	public static final String DATE_FORMAT_ERROR = "field.error.invalid.date.format";

	public static final String AFTER_LOGIN_PATH_GENERATE_REPORT = AFTER_LOGIN_PATH + "/manage/report/generateReport";
	public static final String AFTER_LOGIN_PATH_GENERATE_REPORT_MAPPING = AFTER_LOGIN_PATH_GENERATE_REPORT + "*";
	public static final String AFTER_LOGIN_PATH_LIST_GENERATE_REPORT_MAPPING = AFTER_LOGIN_PATH
			+ "/manage/report/list*";
	public static final String AFTER_LOGIN_PATH_APK_GET_APK_REQUEST_OUTSIDE = AFTER_LOGIN_PATH_GENERATE_REPORT
			+ "?version={version:.+}";

	
	/**
	 * URL LOG
	 */
	public static final String AFTER_LOGIN_PATH_LOG = AFTER_LOGIN_PATH + "/log/messagelogs";

	public static final String AFTER_LOGIN_PATH_LOG_MAPPING = AFTER_LOGIN_PATH_LOG + "*";
	public static final String AFTER_LOGIN_PATH_LOG_GET_LIST_MAPPING = AFTER_LOGIN_PATH_LOG + "/get-list*";
	public static final String AFTER_LOGIN_PATH_LOG_SEARCH_MAPPING = AFTER_LOGIN_PATH_LOG + "/search*";

	public static final String AFTER_LOGIN_PATH_LOG_DETAIL = AFTER_LOGIN_PATH_LOG + "/details/get-details";
	public static final String AFTER_LOGIN_PATH_LOG_DETAIL_MAPPING = AFTER_LOGIN_PATH_LOG_DETAIL + "*";
	public static final String AFTER_LOGIN_PATH_LOG_DETAIL_REQUEST = AFTER_LOGIN_PATH_LOG_DETAIL + "?trxId={trxId}";
	
	public static final String AFTER_LOGIN_PATH_LOG_DETAILS_BY_GROUP_ID_MAPPING = AFTER_LOGIN_PATH_LOG + "*";
	public static final String AFTER_LOGIN_PATH_LOG_GET_LIST_DETAILS_BY_GROUP_ID_MAPPING = AFTER_LOGIN_PATH_LOG
			+ "/get-list-log-details*";
	
	private static final String AFTER_LOGIN_PATH_LOG_DETAIL_RESEND = AFTER_LOGIN_PATH_LOG + "/resend";
	public static final String AFTER_LOGIN_PATH_LOG_DETAIL_RESEND_MAPPING = AFTER_LOGIN_PATH_LOG_DETAIL_RESEND + "*";
	public static final String AFTER_LOGIN_PATH_LOG_DETAIL_RESEND_REQUEST = AFTER_LOGIN_PATH_LOG_DETAIL_RESEND
			+ "?messageId={messageId}";
		
	/**
	 * STORE AND FORWARD
	 */
	
	
	public static final String AFTER_LOGIN_PATH_LOG_STORE_AND_FORWARD = AFTER_LOGIN_PATH + "/log/storeandforward";

	public static final String AFTER_LOGIN_PATH_LOG_STORE_AND_FORWARD_MAPPING = AFTER_LOGIN_PATH_LOG_STORE_AND_FORWARD + "*";
	public static final String AFTER_LOGIN_PATH_LOG_STORE_AND_FORWARD_GET_LIST_MAPPING = AFTER_LOGIN_PATH_LOG_STORE_AND_FORWARD + "/get-list*";
	public static final String AFTER_LOGIN_PATH_LOG_STORE_AND_FORWARD_SEARCH_MAPPING = AFTER_LOGIN_PATH_LOG_STORE_AND_FORWARD + "/search*";
		
	private static final String AFTER_LOGIN_PATH_LOG_STORE_AND_FORWARD_DELETE = AFTER_LOGIN_PATH_LOG_STORE_AND_FORWARD + "/delete";
	public static final String AFTER_LOGIN_PATH_LOG_STORE_AND_FORWARD_DELETE_MAPPING = AFTER_LOGIN_PATH_LOG_STORE_AND_FORWARD_DELETE + "*";
	public static final String AFTER_LOGIN_PATH_LOG_STORE_AND_FORWARD_DELETE_REQUEST = AFTER_LOGIN_PATH_LOG_STORE_AND_FORWARD_DELETE
			+ "?id={id}";
	
	
	/**
	 * MONITORING
	 */
	
	public static final String AFTER_LOGIN_PATH_MANAGE_MONITORING = AFTER_LOGIN_PATH + "/manage/monitoring";
	public static final String AFTER_LOGIN_PATH_MANAGE_MONITORING_MAPPING = AFTER_LOGIN_PATH_MANAGE_MONITORING + "*";
	public static final String AFTER_LOGIN_PATH_MANAGE_MONITORING_GET_LIST_MAPPING = AFTER_LOGIN_PATH_MANAGE_MONITORING
			+ "/get-list*";
	
	private static final String AFTER_LOGIN_PATH_MANAGE_MONITORING_START_ITM = AFTER_LOGIN_PATH_MANAGE_MONITORING + "/startItm";
	public static final String AFTER_LOGIN_PATH_MANAGE_MONITORING_START_ITM_MAPPING = AFTER_LOGIN_PATH_MANAGE_MONITORING_START_ITM + "*";
	public static final String AFTER_LOGIN_PATH_MANAGE_MONITORING_START_ITM_REQUEST = AFTER_LOGIN_PATH_MANAGE_MONITORING_START_ITM + "?id={id}";
		
	private static final String AFTER_LOGIN_PATH_MANAGE_MONITORING_STOP_ITM = AFTER_LOGIN_PATH_MANAGE_MONITORING + "/stopItm";
	public static final String AFTER_LOGIN_PATH_MANAGE_MONITORING_STOP_ITM_MAPPING = AFTER_LOGIN_PATH_MANAGE_MONITORING_STOP_ITM + "*";
	public static final String AFTER_LOGIN_PATH_MANAGE_MONITORING_STOP_ITM_REQUEST = AFTER_LOGIN_PATH_MANAGE_MONITORING_STOP_ITM + "?id={id}";
	
	
	private static final String AFTER_LOGIN_PATH_MANAGE_MONITORING_SIGNON_ITM = AFTER_LOGIN_PATH_MANAGE_MONITORING + "/signonItm";
	public static final String AFTER_LOGIN_PATH_MANAGE_MONITORING_SIGNON_ITM_MAPPING = AFTER_LOGIN_PATH_MANAGE_MONITORING_SIGNON_ITM + "*";
	public static final String AFTER_LOGIN_PATH_MANAGE_MONITORING_SIGNON_ITM_REQUEST = AFTER_LOGIN_PATH_MANAGE_MONITORING_SIGNON_ITM + "?id={id}";
	
	
	private static final String AFTER_LOGIN_PATH_MANAGE_MONITORING_SIGNOFF_ITM = AFTER_LOGIN_PATH_MANAGE_MONITORING + "/signoffItm";
	public static final String AFTER_LOGIN_PATH_MANAGE_MONITORING_SIGNOFF_ITM_MAPPING = AFTER_LOGIN_PATH_MANAGE_MONITORING_SIGNOFF_ITM + "*";
	public static final String AFTER_LOGIN_PATH_MANAGE_MONITORING_SIGNOFF_ITM_REQUEST = AFTER_LOGIN_PATH_MANAGE_MONITORING_SIGNOFF_ITM + "?id={id}";
	
	
	private static final String AFTER_LOGIN_PATH_MANAGE_MONITORING_ECHO_ITM = AFTER_LOGIN_PATH_MANAGE_MONITORING + "/echoItm";
	public static final String AFTER_LOGIN_PATH_MANAGE_MONITORING_ECHO_ITM_MAPPING = AFTER_LOGIN_PATH_MANAGE_MONITORING_ECHO_ITM + "*";
	public static final String AFTER_LOGIN_PATH_MANAGE_MONITORING_ECHO_ITM_REQUEST = AFTER_LOGIN_PATH_MANAGE_MONITORING_ECHO_ITM + "?id={id}";
			
	private static final String AFTER_LOGIN_PATH_MANAGE_MONITORING_START_KSEI = AFTER_LOGIN_PATH_MANAGE_MONITORING + "/startKsei";
	public static final String AFTER_LOGIN_PATH_MANAGE_MONITORING_START_KSEI_MAPPING = AFTER_LOGIN_PATH_MANAGE_MONITORING_START_KSEI + "*";
	public static final String AFTER_LOGIN_PATH_MANAGE_MONITORING_START_KSEI_REQUEST = AFTER_LOGIN_PATH_MANAGE_MONITORING_START_KSEI + "?id={id}";

	
	private static final String AFTER_LOGIN_PATH_MANAGE_MONITORING_STOP_KSEI = AFTER_LOGIN_PATH_MANAGE_MONITORING + "/stopKsei";
	public static final String AFTER_LOGIN_PATH_MANAGE_MONITORING_STOP_KSEI_MAPPING = AFTER_LOGIN_PATH_MANAGE_MONITORING_STOP_KSEI + "*";
	public static final String AFTER_LOGIN_PATH_MANAGE_MONITORING_STOP_KSEI_REQUEST = AFTER_LOGIN_PATH_MANAGE_MONITORING_STOP_KSEI + "?id={id}";

	
}
