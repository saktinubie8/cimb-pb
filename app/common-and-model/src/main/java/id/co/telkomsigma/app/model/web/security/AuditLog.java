package id.co.telkomsigma.app.model.web.security;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import id.co.telkomsigma.app.model.web.GenericModel;

@Entity
@Table(name = "m_audit_log")
public class AuditLog extends GenericModel{

	private Long auditId;
	private Integer activityType;
	private String description;
    private String createdBy;
    private Date createdDate;
    private String reffNo;
    
    /***************************** - ACTIVITY TYPE - ****************************/
    public static final int LOGIN_LOGOUT = 1;
    public static final int VIEW_PAGE = 2;
    public static final int SAVE_OR_UPDATE = 3;
    public static final int DELETE_CACHE = 4;
    public static final int CHECK_CONNECTION = 5;
    public static final int STORE_AND_FORWARD = 6;
    public static final int MANUAL_INJECTOR = 7;
    
    
    @Column(name = "REFF_NO", nullable = false,length=MAX_LENGTH_REFF_NO)
    public String getReffNo() {
		return reffNo;
	}

	public void setReffNo(String reffNo) {
		this.reffNo = reffNo;
	}

	@Id
    @Column(name = "id", nullable = false,unique=true)
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public Long getAuditId() {
		return auditId;
	}
    
    @Column(name = "ACTIVITY_TYPE", nullable = false)
	public Integer getActivityType() {
		return activityType;
	}
    
    @Column(name = "DESCRIPTION", nullable = false, length=MAX_LENGTH_DESCRIPTION)
	public String getDescription() {
		return description;
	}
	public void setAuditId(Long auditId) {
		this.auditId = auditId;
	}
	public void setActivityType(Integer activity_type) {
		this.activityType = activity_type;
	}
	public void setDescription(String description) {
		this.description = description;
	}
    
	
	@Column(name = "CREATED_BY", nullable = false)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
    @Column(name = "CREATED_DT", nullable = false)
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
}
