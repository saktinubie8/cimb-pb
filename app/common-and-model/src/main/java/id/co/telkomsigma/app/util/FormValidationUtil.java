package id.co.telkomsigma.app.util;

import org.springframework.validation.BindingResult;
import org.springframework.web.multipart.MultipartFile;

import id.co.telkomsigma.app.model.web.user.User;
import id.co.telkomsigma.app.util.FieldValidationUtil;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by daniel on 4/16/15.
 */
public final class FormValidationUtil {
	public static final String FIELD_ERROR_CONTAINS_SPACE = "field.error.contains.space";
	public static final String FIELD_ERROR_EQUALS = "field.error.equals";
	public static final String FIELD_ERROR_PASSWORD_EQUALS = "field.error.password.equals";
	public static final String FIELD_ERROR_INVALID_FORMAT_EMAIL = "field.error.invalid.format.email";
	public static final String FIELD_ERROR_INVALID_VALUE = "field.error.invalid.value";
	public static final String FIELD_ERROR_INVALID_VALUE_LOCATION = "field.error.invalid.value.location";
	public static final String FIELD_ERROR_INVALID_VALUE_CAPTCHA = "field.error.invalid.value.captcha";
	public static final String FIELD_ERROR_INVALID_VALUE_CITY = "field.error.invalid.value.city";
	public static final String FIELD_ERROR_INVALID_VALUE_COMMIT_TYPE = "field.error.invalid.value.commit.type";
	public static final String FIELD_ERROR_INVALID_VALUE_COUNTRY = "field.error.invalid.value.country";
	public static final String FIELD_ERROR_INVALID_VALUE_ENDORSE_TYPE = "field.error.invalid.value.endorse.type";
	public static final String FIELD_ERROR_INVALID_VALUE_FARE_TYPE = "field.error.invalid.value.fare.type";
	public static final String FIELD_ERROR_INVALID_VALUE_LOSS_BEHAVIOUR_CODE = "field.error.invalid.value.loss.behaviour.code";
	public static final String FIELD_ERROR_INVALID_VALUE_MASTER_LEASING = "field.error.invalid.value.master.leasing";
	public static final String FIELD_ERROR_INVALID_VALUE_POLICY_OBJECT_TYPE = "field.error.invalid.value.policy.object.type";
	public static final String FIELD_ERROR_INVALID_VALUE_POLICY_STATUS = "field.error.invalid.value.policy.status";
	public static final String FIELD_ERROR_INVALID_VALUE_POLICY_TYPE = "field.error.invalid.value.policy.type";
	public static final String FIELD_ERROR_INVALID_VALUE_PROVINCE = "field.error.invalid.value.province";
	public static final String FIELD_ERROR_INVALID_VALUE_REPORTING_MEDIA = "field.error.invalid.value.reporting.media";
	public static final String FIELD_ERROR_INVALID_VALUE_ROLE = "field.error.invalid.value.role";
	public static final String FIELD_ERROR_INVALID_VALUE_SUB_DISTRICT = "field.error.invalid.value.sub.district";
	public static final String FIELD_ERROR_INVALID_VALUE_VEHICLE_TYPE = "field.error.invalid.value.vehicle.type";
	public static final String FIELD_ERROR_MAXLENGTH = "field.error.maxlength";
	public static final String FIELD_ERROR_MINLENGTH = "field.error.minlength";
	public static final String FIELD_ERROR_NOT_UNIQUE_EMAIL = "field.error.not.unique.email";
	public static final String FIELD_ERROR_NOT_UNIQUE_USERNAME = "field.error.not.unique.username";
	public static final String FIELD_ERROR_NUMBER_ONLY = "field.error.number.only";
	public static final String FIELD_ERROR_REQUIRED = "field.error.required";
	public static final String FIELD_ERROR_USER_NOT_FOUND = "field.error.user.not.found";
	public static final String FIELD_ERROR_USER_ALREADY_LOCK = "field.error.user.already.lock";
	public static final String FIELD_ERROR_USER_ALREADY_UNLOCK = "field.error.user.already.unlock";
	public static final String FIELD_ERROR_REQUIRED_DEPENDENCY = "field.error.required.dependency";
	public static final String FIELD_ERROR_FORMAT_DATE = "field.error.invalid.date.format";
	public static final String FIELD_ERROR_USER_ALREADY_ENABLE = "field.error.user.already.enable";
	public static final String FIELD_ERROR_USER_ALREADY_DISABLE = "field.error.user.already.disable";
	public static final String FIELD_ERROR_LOCATION_NOTFOUND = "field.error.location.notfound";
	public static final String FIELD_ERROR_LOCATION_NULL = "field.error.location.null";
	public static final String FIELD_ERROR_APK_NOT_FOUND = "field.error.apk.not.found";
	public static final String FIELD_ERROR_APK_ALREADY_NOT_ALLOWED = "field.error.apk.already.not.allowed";
	public static final String FIELD_ERROR_APK_ALREADY_ALLOWED = "field.error.apk.already.allowed";
	public static final String FIELD_ERROR_NO_KARTU_EXIST = "field.error.no.kartu.exist";
	public static final String FIELD_ERROR_IDXMW_ENDPOINT_EXISTS = "field.error.not.unique.idxmw.endpoint";
	public static final String FIELD_ERROR_IDXMW_ENDPOINTGROUP_EXISTS = "field.error.not.unique.idxmw.endpointGroup";
	public static final String FIELD_ERROR_CONFIRM_PASSWORD = "field.error.not.match.password";
	public static final String FIELD_ERROR_REST_USERNAME_ENDPOINT_EXISTS = "field.error.not.unique.rest.endpoint.username";
	public static final String FIELD_ERROR_INVALID_FORMAT_IP_ADDRESS = "field.error.invalid.format.ipAddress";
	public static final String FIELD_ERROR_REST_ENDPOINT_GROUP_EXISTS = "field.error.not.unique.rest.endpoint.group";
	public static final String FIELD_ERROR_REST_SERVICE_EXISTS = "field.error.not.unique.rest.service";
	public static final String FIELD_ERROR_INVALID_FORMAT_USERNAME = "field.error.invalid.format.username";
	public static final String FIELD_ERROR_INVALID_FORMAT_USERNAME_AND_PWD = "field.error.invalid.format.username.and.password";
	public static final String FIELD_ERROR_INVALID_FORMAT_PASSWORD = "field.error.invalid.format.password";
	public static final String FIELD_ERROR_UNKNOWN_ENDPOINT = "field.error.unknown.endpoint";
	public static final String FIELD_ERROR_UNKNOWN_ENDPOINT_GROUP = "field.error.unknown.enpoint.group";
	public static final String FIELD_ERROR_REUSE_PASSWORD = "field.error.reuse.password";
	public static final String FIELD_ERROR_FORTMAT_PASSWORD = "label.note.password";
	public static final String FIELD_ERROR_GENERAL_PASSWORD = "label.note.general.password";
	public static final String FIELD_ERROR_OVERLIMIT_CHANGE_PASSWORD = "field.error.overlimit.change.password";
	public static final String FIELD_ERROR_MIN_EXPIRY_DAYS = "field.error.min.expiry.days";
	public static final String FIELD_ERROR_MIN_HISTORY_PWD = "field.error.min.hist.change.pwd";
	public static final String FIELD_ERROR_MIN_LENGTH_PWD = "field.error.min.length.pwd";
	
	
	// Validasi add User
	public static void validatingUsername(BindingResult errors, String name, String value, String label,
			short maxLength, short minLength) {
		if (FieldValidationUtil.isEmpty(value)) {
			errors.rejectValue(name, FIELD_ERROR_REQUIRED, new Object[] { label }, "");
		} else if (FieldValidationUtil.containsSpace(value)) {
			errors.rejectValue(name, FIELD_ERROR_CONTAINS_SPACE, new Object[] { label }, "");
		} else if (FieldValidationUtil.isLengthLessThan(value, minLength)) {
			errors.rejectValue(name, FIELD_ERROR_MINLENGTH, new Object[] { label, minLength }, "");
		}
		// label = validatingRequiredMaxMinLength(errors, name, value, label,
		// maxLength, minLength);
		// label = validatingNoSpace(errors, name, value, label);
	}

	public static void validatingEmail(BindingResult errors, String name, String value, String label, short maxLength) {
		label = validatingRequiredMaxLength(errors, name, value, label, maxLength);
		if (FieldValidationUtil.isInvalidEmail(value)) {
			errors.rejectValue(name, FIELD_ERROR_INVALID_FORMAT_EMAIL, new Object[] { label }, "");
		}
	}

	public static void validatingPassword(BindingResult errors, String name, String value, String label,
			String nameConfirm, String valueConfirm, String labelConfirm, short maxLength, short minLength) {
		
		label = validatingRequiredMaxMinLength(errors, name, value, label, maxLength, minLength);
		labelConfirm = labelConfirm.toLowerCase();
		
		if (FieldValidationUtil.isBlank(valueConfirm)) {
			errors.rejectValue(nameConfirm, FIELD_ERROR_REQUIRED, new Object[] { labelConfirm }, "");
		}
		if (FieldValidationUtil.isNotEquals(value, valueConfirm)) {
			errors.rejectValue(nameConfirm, FIELD_ERROR_EQUALS, new Object[] { labelConfirm, label }, "");
		}
	}

	public static void validatingOldPassword(BindingResult errors, String name, String valueConfirm, String label,
			Boolean match, String labelConfirm, short maxLength, short minLength) {
		validatingRequiredMaxMinLength(errors, name, valueConfirm, label, maxLength, minLength);
		// validatingRequired(errors,name,valueConfirm,label);
		if (FieldValidationUtil.isBlank(valueConfirm)) {
			errors.rejectValue(name, FIELD_ERROR_REQUIRED, new Object[] { labelConfirm }, "");
		}
		if (!match) {
			errors.rejectValue(name, FIELD_ERROR_PASSWORD_EQUALS, new Object[] { labelConfirm, label }, "");
		}
	}

	public static void validatingName(BindingResult errors, String name, String value, String label, short maxLength) {
		validatingRequiredMaxLength(errors, name, value, label, maxLength);
		if (FieldValidationUtil.isEmpty(value)) {
			errors.rejectValue(name, FIELD_ERROR_REQUIRED, new Object[] { label }, "");
		}
	}

	public static void validatingAddress(BindingResult errors, String name, String value, String label,
			short maxLength) {
		validatingRequiredMaxLength(errors, name, value, label, maxLength);
		if (FieldValidationUtil.isEmpty(value)) {
			errors.rejectValue(name, FIELD_ERROR_REQUIRED, new Object[] { label }, "");
		}
	}

	public static void validatingCity(BindingResult errors, String name, String value, String label, short maxLength) {
		validatingRequiredMaxLength(errors, name, value, label, maxLength);
	}

	public static void validatingProvince(BindingResult errors, String name, String value, String label,
			short maxLength) {
		validatingRequiredMaxLength(errors, name, value, label, maxLength);
	}

	public static void validatingPhoneMobile(BindingResult errors, String name, String value, String label,
			short maxLength, short minLength) {
		if (!FieldValidationUtil.isBlank(value))
			validatingRequiredMaxMinLengthNumericOnly(errors, name, value, label, maxLength, minLength);
	}

	public static void validatingPhone(BindingResult errors, String name, String value, String label, short maxLength,
			short minLength) {
		validatingRequiredMaxMinLengthNumericOnly(errors, name, value, label, maxLength, minLength);
	}

	public static void validatingRole(BindingResult errors, String name, List<String> values, String label) {
		label = label.toLowerCase();
		if (values == null || values.size() == 0) {
			errors.rejectValue(name, FIELD_ERROR_REQUIRED, new Object[] { label }, "");
		}
	}

	public static void validatingNIK(BindingResult errors, String name, String values, String label) {
		label = label.toLowerCase();
		if (values.equals(null) || values.isEmpty()) {
			errors.rejectValue(name, FIELD_ERROR_REQUIRED, new Object[] { label }, "");
		}
	}

	public static void validatingAcceptanceFile(BindingResult errors, String name, MultipartFile acceptanceFile,
			String label) {
		label = label.toLowerCase();
		if (FieldValidationUtil.isEmpty(acceptanceFile)) {
			errors.rejectValue(name, FIELD_ERROR_REQUIRED, new Object[] { label }, "");
		}
	}

	public static void validatingCaptcha(BindingResult errors, String name, String value, String label,
			String valueCaptcha, short maxLength) {
		label = validatingRequiredMaxLength(errors, name, value, label, maxLength);
		if (FieldValidationUtil.isNotEqualsIgnoreCase(value, valueCaptcha)) {
			errors.rejectValue(name, FIELD_ERROR_INVALID_VALUE_CAPTCHA, new Object[] { label }, "");
		}
	}

	public static void validatingChronology(BindingResult errors, String name, String value, String label,
			short maxLength) {
		validatingRequiredMaxLength(errors, name, value, label, maxLength);
	}

	public static void validatingDescription(BindingResult errors, String name, String value, String label,
			short maxLength) {
		validatingMaxLength(errors, name, value, label, maxLength);
	}

	public static void validatingInstitutionName(BindingResult errors, String name, String value, String label,
			short maxLength) {
		validatingMaxLength(errors, name, value, label, maxLength);
	}

	public static void validatingIsPremiPaid(BindingResult errors, String name, String value, String label,
			String... valueArr) {
		label = label.toLowerCase();
		if (FieldValidationUtil.isBlank(value)) {
			errors.rejectValue(name, FIELD_ERROR_REQUIRED, new Object[] { label }, "");
		}
		List<String> values = Arrays.asList(valueArr);
		if (!values.contains(value)) {
			errors.rejectValue(name, FIELD_ERROR_INVALID_VALUE, new Object[] { label }, "");
		}
	}

	public static void validatingIsSurveyor(BindingResult errors, String name, String value, String label,
			String... valueArr) {
		label = label.toLowerCase();
		if (FieldValidationUtil.isBlank(value)) {
			errors.rejectValue(name, FIELD_ERROR_REQUIRED, new Object[] { label }, "");
		}
		List<String> values = Arrays.asList(valueArr);
		if (!values.contains(value)) {
			errors.rejectValue(name, FIELD_ERROR_INVALID_VALUE, new Object[] { label }, "");
		}
	}

	public static void validatingIsTelkom(BindingResult errors, String name, String value, String label,
			String... valueArr) {
		label = label.toLowerCase();
		if (FieldValidationUtil.isBlank(value)) {
			errors.rejectValue(name, FIELD_ERROR_REQUIRED, new Object[] { label }, "");
		}
		List<String> values = Arrays.asList(valueArr);
		if (!values.contains(value)) {
			errors.rejectValue(name, FIELD_ERROR_INVALID_VALUE, new Object[] { label }, "");
		}
	}

	public static void validatingLossDescription(BindingResult errors, String name, String value, String label,
			short maxLength) {
		validatingRequiredMaxLength(errors, name, value, label, maxLength);
	}

	public static void validatingLossScene(BindingResult errors, String name, String value, String label,
			short maxLength) {
		validatingRequiredMaxLength(errors, name, value, label, maxLength);
	}

	public static void validatingPostal(BindingResult errors, String name, String value, String label,
			short maxLength) {
		validatingRequiredMaxLengthNumericOnly(errors, name, value, label, maxLength);
	}

	public static void validatingPostalOptional(BindingResult errors, String name, String value, String label,
			short maxLength) {
		validatingMaxLengthNumericOnly(errors, name, value, label, maxLength);
	}

	public static void validatingReporterAddress(BindingResult errors, String name, String value, String label,
			short maxLength) {
		validatingRequiredMaxLength(errors, name, value, label, maxLength);
	}

	public static void validatingReporterName(BindingResult errors, String name, String value, String label,
			short maxLength) {
		validatingRequiredMaxLength(errors, name, value, label, maxLength);
	}

	public static void validatingReporterPhone(BindingResult errors, String name, String value, String label,
			short maxLength, short minLength) {
		validatingRequiredMaxMinLengthNumericOnly(errors, name, value, label, maxLength, minLength);
	}

	public static void validatingLockUser(BindingResult errors, String name, User values, String label) {
		if (values == null) {
			errors.rejectValue(name, FIELD_ERROR_USER_NOT_FOUND, new Object[] { values.getUsername() }, "");
		} else {
			if (!values.isAccountNonLocked()) {
				errors.rejectValue(name, FIELD_ERROR_USER_ALREADY_LOCK, new Object[] { values.getUsername() }, "");
			}
		}
	}

	public static void validatingUnLockUser(BindingResult errors, String name, User values, String label) {
		if (values == null) {
			errors.rejectValue(name, FIELD_ERROR_USER_NOT_FOUND, new Object[] { values.getUsername() }, "");
		} else {
			if (values.isAccountNonLocked()) {
				errors.rejectValue(name, FIELD_ERROR_USER_ALREADY_UNLOCK, new Object[] { values.getUsername() }, "");
			}
		}
	}

	public static void validatingStatus(BindingResult errors, String name, String value, String label,
			String[] valueArr) {
		label = label.toLowerCase();
		if (FieldValidationUtil.isBlank(value)) {
			errors.rejectValue(name, FIELD_ERROR_REQUIRED, new Object[] { label }, "");
		}
		List<String> values = Arrays.asList(valueArr);
		if (!values.contains(value)) {
			errors.rejectValue(name, FIELD_ERROR_INVALID_VALUE, new Object[] { label }, "");
		}
	}

	private static String validatingMaxLength(BindingResult errors, String name, String value, String label,
			short maxLength) {
		label = label.toLowerCase();
		System.out.println("Validate val length : " +value + " maxlenght : " + maxLength  );
		if (FieldValidationUtil.isLengthGreaterThan(value, maxLength)) {
			errors.rejectValue(name, FIELD_ERROR_MAXLENGTH, new Object[] { label, maxLength }, "");
		}
		return label;
	}

	private static String validatingMaxLengthNumericOnly(BindingResult errors, String name, String value, String label,
			short maxLength) {
		label = validatingMaxLength(errors, name, value, label, maxLength);
		if (FieldValidationUtil.isNotNumericOnly(value)) {
			errors.rejectValue(name, FIELD_ERROR_NUMBER_ONLY, new Object[] { label }, "");
		}
		return label;
	}

	private static String validatingNoSpace(BindingResult errors, String name, String value, String label) {
		label = label.toLowerCase();
		if (FieldValidationUtil.containsSpace(value)) {
			errors.rejectValue(name, FIELD_ERROR_CONTAINS_SPACE, new Object[] { label }, "");
		}
		return label;
	}

	private static String validatingRequiredMaxLength(BindingResult errors, String name, String value, String label,
			short maxLength) {
		label = label.toLowerCase();
		// if (FieldValidationUtil.isBlank(value)) {
		// errors.rejectValue(name, FIELD_ERROR_REQUIRED, new Object[] { label
		// }, "");
		// }
		if (FieldValidationUtil.isLengthGreaterThan(value, maxLength)) {
			errors.rejectValue(name, FIELD_ERROR_MAXLENGTH, new Object[] { label, maxLength }, "");
		}
		return label;
	}

	public static String validatingPasswordMaxLength(BindingResult errors, String name, String value, String label,
													  short maxLength) {
		label = label.toLowerCase();
		// if (FieldValidationUtil.isBlank(value)) {
		// errors.rejectValue(name, FIELD_ERROR_REQUIRED, new Object[] { label
		// }, "");
		// }
		if (FieldValidationUtil.isLengthGreaterThan(value, maxLength)) {
			errors.rejectValue(name, FIELD_ERROR_MAXLENGTH, new Object[] { label, maxLength }, "");
		}
		return label;
	}

	private static String validatingRequiredMaxLengthNumericOnly(BindingResult errors, String name, String value,
			String label, short maxLength) {
		label = validatingRequiredMaxLength(errors, name, value, label, maxLength);
		if (FieldValidationUtil.isNotNumericOnly(value)) {
			errors.rejectValue(name, FIELD_ERROR_NUMBER_ONLY, new Object[] { label }, "");
		}
		return label;
	}

	private static String validatingRequiredMaxMinLength(BindingResult errors, String name, String value, String label,
			short maxLength, short minLength) {
		
		label = validatingRequiredMaxLength(errors, name, value, label, maxLength);
		
		if (FieldValidationUtil.isLengthLessThan(value, minLength)) {
			errors.rejectValue(name, FIELD_ERROR_MINLENGTH, new Object[] { label, minLength }, "");
		}
		return label;
	}

	private static String validatingRequiredMaxMinLengthNumericOnly(BindingResult errors, String name, String value,
			String label, short maxLength, short minLength) {
		label = validatingRequiredMaxMinLength(errors, name, value, label, maxLength, minLength);
		if (FieldValidationUtil.isNotNumericOnly(value)) {
			errors.rejectValue(name, FIELD_ERROR_NUMBER_ONLY, new Object[] { label }, "");
		}
		return label;
	}

	private static String validatingDateFormat(BindingResult errors, String name, String value, String label) {
		label = label.toLowerCase();
		if (FieldValidationUtil.isInvalidDatePicker(value)) {
			errors.rejectValue(name, FIELD_ERROR_FORMAT_DATE, new Object[] { label }, "");
		}
		return label;
	}

	private static String validatingDateTimeFormat(BindingResult errors, String name, String value, String label) {
		label = label.toLowerCase();
		if (FieldValidationUtil.isInvalidDateTimePicker(value)) {
			errors.rejectValue(name, FIELD_ERROR_FORMAT_DATE, new Object[] { label }, "");
		}
		return label;
	}

	private static String[] validatingNullHierarchy(BindingResult errors, String name, Map<String, String> datas) {
		int size = datas.size();
		String[] labels = new String[datas.size()];
		for (String key : datas.keySet()) {
			if (FieldValidationUtil.isBlank(datas.get(key))) {
				labels[datas.size() - size] = key;
				size--;
			}
		}
		if (size == 0) {
			errors.rejectValue(name, FIELD_ERROR_REQUIRED_DEPENDENCY, labels, "");
		}

		return labels;
	}

	public static void validatingFourComponent(BindingResult errors, String name, Map<String, String> datas) {
		validatingNullHierarchy(errors, name, datas);
	}

	public static void validatingKotaKejadian(BindingResult errors, String name, String value, String label,
			short maxLength) {
		validatingRequiredMaxLength(errors, name, value, label, maxLength);
	}

	public static void validatingAssignDate(BindingResult errors, String name, String value, String label) {
		label = validatingDateTimeFormat(errors, name, value, label);
	}

	public static void validatingDateOfLoss(BindingResult errors, String name, String value, String label) {
		label = validatingDateTimeFormat(errors, name, value, label);
	}

	public static void validatingAuthority(BindingResult errors, String name, String value, String label,
			short maxLength, short minLength) {
		label = validatingRequiredMaxMinLength(errors, name, value, label, maxLength, minLength);
		label = validatingNoSpace(errors, name, value, label);
	}

	public static void validatingRoleDescription(BindingResult errors, String name, String value, String label,
			short maxLength) {
		label = validatingRequiredMaxLength(errors, name, value, label, maxLength);
	}

	public static void validatingMenus(BindingResult errors, String name, List<String> values, String label) {
		label = label.toLowerCase();
		if (values == null || values.size() == 0) {
			errors.rejectValue(name, FIELD_ERROR_REQUIRED, new Object[] { label }, "");
		}
	}

	public static void validatingResetPassword(BindingResult errors, String name, User values, String label) {
		if (values == null) {
			errors.rejectValue(name, FIELD_ERROR_USER_NOT_FOUND, new Object[] { values.getUsername() }, "");
		}
	}

	public static void validatingEnableUser(BindingResult errors, String name, User values, String label) {
		if (values == null) {
			errors.rejectValue(name, FIELD_ERROR_USER_NOT_FOUND, new Object[] { values.getUsername() }, "");
		} else {
			if (values.isEnabled()) {
				errors.rejectValue(name, FIELD_ERROR_USER_ALREADY_ENABLE, new Object[] { values.getUsername() }, "");
			}
		}
	}

	public static void validatingDisableUser(BindingResult errors, String name, User values, String label) {
		if (values == null) {
			errors.rejectValue(name, FIELD_ERROR_USER_NOT_FOUND, new Object[] { values.getUsername() }, "");
		} else {
			if (!values.isEnabled()) {
				errors.rejectValue(name, FIELD_ERROR_USER_ALREADY_DISABLE, new Object[] { values.getUsername() }, "");
			}
		}
	}

	public static void validatingNotNullLocation(BindingResult errors, String name, String strLocation, String label) {
		if (strLocation == null) {
			errors.rejectValue(name, FIELD_ERROR_LOCATION_NULL, "");
		} else if (strLocation.trim().length() <= 0)
			errors.rejectValue(name, FIELD_ERROR_LOCATION_NULL, "");
	}

		
	
	public static void validatingRequired(BindingResult errors, String name, String value, String label) {
		label = label.toLowerCase();
		if (FieldValidationUtil.isBlank(value)) {
			errors.rejectValue(name, FIELD_ERROR_REQUIRED, new Object[] { label }, "");
		}
	}
	
	public static void validatingLongRequired(BindingResult errors, String name, Long value, String label) {
		label = label.toLowerCase();
		if (FieldValidationUtil.isBlank(value)) {
			errors.rejectValue(name, FIELD_ERROR_REQUIRED, new Object[] { label }, "");
		}
	}
	
		
	public static void validatingListRequired(BindingResult errors, String name, List<Long> value, String label) {
		label = label.toLowerCase();
		if (FieldValidationUtil.isEmpty(value)) {
			errors.rejectValue(name, FIELD_ERROR_REQUIRED, new Object[] { label }, "");
		}
	}

	
	
	public static void validatingUsername(BindingResult errors, String name, String value, String label) {
		if (FieldValidationUtil.isInvalidUsername(value)) {
			errors.rejectValue(name, FIELD_ERROR_INVALID_FORMAT_USERNAME, new Object[] { label }, "");
		}
	}
	
	
	public static void validatingUsernameAndPassword(BindingResult errors, String name, User user, String label) {
		if (FieldValidationUtil.isInvalidUsernameAndPassword(user.getUsername(), user.getPassword())) {
			System.out.println("username : " + user.getUsername() + " password : " + user.getPassword());
			errors.rejectValue(name, FIELD_ERROR_INVALID_FORMAT_USERNAME_AND_PWD, new Object[] { label }, "");
		}
	}
	
	public static void validatingFormatPassword(BindingResult errors, String name, String value, String label) {
		if (!FieldValidationUtil.isValidPassword(value)) {
			errors.rejectValue(name, FIELD_ERROR_INVALID_FORMAT_USERNAME, new Object[] { label }, "");
		}
	}
	
	public static void validatingPasswordFormat(BindingResult errors, String name, String value, String label) {
		if (FieldValidationUtil.isInvalidPassword(value)) {
			errors.rejectValue(name, FIELD_ERROR_FORTMAT_PASSWORD, new Object[] { label }, "");
		}
	}	
	
	public static void validatingCfgEndpoint(BindingResult errors, Long value) {
		errors.rejectValue(FIELD_ERROR_UNKNOWN_ENDPOINT, "");
	}
	
	public static void validatingCfgEndpointGroup(BindingResult errors, Long value) {
		errors.rejectValue(FIELD_ERROR_UNKNOWN_ENDPOINT_GROUP, "");
	}
	
	public static void validatingDefaultPassword(BindingResult errors, String name, String value, String label,
			String nameConfirm, String valueConfirm, String labelConfirm, short maxLength, short minLength) {
		label = validatingRequiredMaxMinLength(errors, name, value, label, maxLength, minLength);
		labelConfirm = labelConfirm.toLowerCase();
		if (FieldValidationUtil.isBlank(valueConfirm)) {
			errors.rejectValue(nameConfirm, FIELD_ERROR_REQUIRED, new Object[] { labelConfirm }, "");
		}
		if (FieldValidationUtil.isNotEquals(value, valueConfirm)) {
			errors.rejectValue(nameConfirm, FIELD_ERROR_EQUALS, new Object[] { labelConfirm, label }, "");
		}
	}
	
/*	public static void validatingReusePassword(BindingResult errors, Long value) {
		errors.rejectValue(FIELD_ERROR_INVALID_FORMAT_EMAIL, "");
	}*/
	
	public static void validatingReusePassword(BindingResult errors, String pwd, String label) {
		//validatingRequiredMaxLength(errors, name, value, label, maxLength);
		//if (FieldValidationUtil.isEmpty(value)) {
			errors.rejectValue(pwd, FIELD_ERROR_REUSE_PASSWORD, new Object[] { label }, "");
		//}
	}
	
	private static String validatingRequiredNumericOnly(BindingResult errors, String name, String value,
			String label) {
		//label = validatingRequiredMaxMinLength(errors, name, value, label, maxLength, minLength);
		if (FieldValidationUtil.isNotNumericOnly(value)) {
			errors.rejectValue(name, FIELD_ERROR_NUMBER_ONLY, new Object[] { label }, "");
		}
		return label;
	}
	
	public static void validatingNumber(BindingResult errors, String name, String value, String label) {
		if (!FieldValidationUtil.isBlank(value))
			validatingRequiredNumericOnly(errors, name, value, label);
	}

	public static void changePassOverLimit(BindingResult errors, String user,  String label){
		errors.rejectValue(user, FIELD_ERROR_OVERLIMIT_CHANGE_PASSWORD, new Object[] { label }, "");
	}
	
	public static void validateMinExpiryDays(BindingResult errors, String paramValue, String label) {
			errors.rejectValue(paramValue, FIELD_ERROR_MIN_EXPIRY_DAYS, new Object[] { label }, "");
	}
	
	public static void validateMinHistPwd(BindingResult errors, String paramValue, String label) {
		errors.rejectValue(paramValue, FIELD_ERROR_MIN_HISTORY_PWD, new Object[] { label }, "");
	}
	
	public static void validateMinLengthPwd(BindingResult errors, String paramValue, String label) {
		
		errors.rejectValue(paramValue, FIELD_ERROR_MIN_LENGTH_PWD, new Object[] { label }, "");
	}
	
	public static String validatingPasswordMinLengthPass(BindingResult errors, String name, String value, String label,
			  short minLength) {
		label = label.toLowerCase();
		//minLength = minLength + new Short("1")
		if(FieldValidationUtil.isLengthLessThan(value,minLength)) {
		//if (FieldValidationUtil.isLengthLessThan(value, minLength)) {
			errors.rejectValue(name, FIELD_ERROR_MINLENGTH, new Object[] { label, minLength }, "");
		}
		return label;
	}
	
	
	
	public static void validatingGeneralPassword(BindingResult errors, String name, String password, String generalPwd,  String label) {
		if (FieldValidationUtil.isGeneralPwd(password, generalPwd)) {
			errors.rejectValue(name, FIELD_ERROR_GENERAL_PASSWORD, new Object[] { label }, "");
		}
	}	
}