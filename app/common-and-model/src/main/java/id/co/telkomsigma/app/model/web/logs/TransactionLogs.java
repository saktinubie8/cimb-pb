package id.co.telkomsigma.app.model.web.logs;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

import javax.persistence.*;

@Entity
@Table(name = "T_TRANSACTION_LOGS")
public class TransactionLogs {

	@Id
	@Column(name = "ID", nullable = false, unique = true, columnDefinition = "BINARY(16)")	
	private UUID id;
	
	@Column(name = "CREATED_DATE", nullable = false)
	private Date createdDate;
	
	// 0 = PENDING
	// 1 = COMPLETED
	@Column(name = "STATUS", nullable = false)
	private Integer status;
	
	@Column(name = "ORIGINATOR", nullable = false)
	private String originator;
	
	@Column(name = "DESTINATION", nullable = false)
	private String destination;
	
	@Column(name = "TRANSACTION_TYPE", nullable = false)
	private String transactionType;
		
	@Column(name = "CASH_VALUE", nullable = true)
	private BigDecimal cashValue;
	
	@Column(name = "EXT_REF", nullable = true)
	private String extRef;
	
	@Column(name = "BENEFICIARY_ACCOUNT", nullable = true)
	private String benAC;
	
	@Column(name = "MEMBER_ID", nullable = true)
	private String memID;
	
	@Column(name = "MEMBER_ACCOUNT", nullable = true)
	private String memAC;
	
	@Column(name = "CURRENCY_CODE", nullable = true)
	private String curCod; 
	
	@Column(name = "VALUE_DATE", nullable = true)
	private String valDate;
	
	@Column(name = "NOTES", nullable = true)
	private String notes;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getOriginator() {
		return originator;
	}

	public void setOriginator(String originator) {
		this.originator = originator;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public BigDecimal getCashValue() {
		return cashValue;
	}

	public void setCashValue(BigDecimal cashValue) {
		this.cashValue = cashValue;
	}

	public String getExtRef() {
		return extRef;
	}

	public void setExtRef(String extRef) {
		this.extRef = extRef;
	}

	public String getBenAC() {
		return benAC;
	}

	public void setBenAC(String benAC) {
		this.benAC = benAC;
	}

	public String getMemID() {
		return memID;
	}

	public void setMemID(String memID) {
		this.memID = memID;
	}

	public String getMemAC() {
		return memAC;
	}

	public void setMemAC(String memAC) {
		this.memAC = memAC;
	}

	public String getCurCod() {
		return curCod;
	}

	public void setCurCod(String curCod) {
		this.curCod = curCod;
	}

	public String getValDate() {
		return valDate;
	}

	public void setValDate(String valDate) {
		this.valDate = valDate;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}
	
	
}
