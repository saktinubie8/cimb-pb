package id.co.telkomsigma.app.model.web.logs;

import java.util.Date;
import java.util.UUID;

import javax.persistence.*;

import id.co.telkomsigma.app.model.web.GenericModel;

@Entity
@Table(name = "T_MESSAGE_LOGS")
public class MessageLogs extends GenericModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4494547109990679124L;

	@Id
	@Column(name = "ID", nullable = false, unique = true, columnDefinition = "BINARY(16)")	
	private UUID id;
	
	@Column(name = "CREATED_DATE", nullable = false)
	private Date createdDate;
	
	@Column(name = "ORIGINATOR", nullable = false)
	private String originator;
	
	@Column(name = "DESTINATION", nullable = false)
	private String destination;
	
	@Column(name = "RAW_DATA", nullable = true, columnDefinition = "LONGTEXT")
	private String rawData;
	
	@Column(name = "FORMATTED_DATA", nullable = true, columnDefinition = "JSON")
    private String formattedData;
	
	@Column(name = "MESSAGE_TYPE", nullable = false)
    private String messageType;
	
	@Column(name = "TRANSACTION_ID", nullable = false, unique = false, columnDefinition = "BINARY(16)")	
	private UUID transactionId;
	
	@Column(name = "STAN", nullable = true)
    private String stan;
	
	@Column(name = "IS_REQUEST", nullable = false)
	private Boolean isRequest = false;
	
	@Column(name = "TRX_TYPE", nullable = true)
	private String trxType;

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getOriginator() {
		return originator;
	}

	public void setOriginator(String originator) {
		this.originator = originator;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getRawData() {
		return rawData;
	}

	public void setRawData(String rawData) {
		this.rawData = rawData;
	}

	public String getFormattedData() {
		return formattedData;
	}

	public void setFormattedData(String formattedData) {
		this.formattedData = formattedData;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public UUID getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(UUID transactionId) {
		this.transactionId = transactionId;
	}

	public String getStan() {
		return stan;
	}

	public void setStan(String stan) {
		this.stan = stan;
	}

	public Boolean getIsRequest() {
		return isRequest;
	}

	public void setIsRequest(Boolean isRequest) {
		this.isRequest = isRequest;
	}

	public String getTrxType() {
		return trxType;
	}

	public void setTrxType(String trxType) {
		this.trxType = trxType;
	}	
	
	
	
	
	
}
