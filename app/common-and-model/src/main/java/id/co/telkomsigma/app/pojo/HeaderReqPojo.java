package id.co.telkomsigma.app.pojo;

public class HeaderReqPojo {

		private String name;
		private String type;
		private String structure;
		
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public String getStructure() {
			return structure;
		}
		public void setStructure(String structure) {
			this.structure = structure;
		}

		
		
}
