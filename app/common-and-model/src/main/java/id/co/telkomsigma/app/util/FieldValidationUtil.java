package id.co.telkomsigma.app.util;

import org.springframework.web.multipart.MultipartFile;

import id.co.telkomsigma.app.model.web.security.AuditLog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by daniel on 4/10/15.
 */
public final class FieldValidationUtil {
	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	private static Pattern emailPattern = Pattern.compile(EMAIL_PATTERN);

	private static final String DATE_PATTERN = "(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/((19|20)\\d\\d)";

	private static final String DATETIME_PATTERN = "(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/((19|20)\\d\\d) (2[0-3]|[0-1][0-9]):[0-5][0-9]";

	private static Pattern datePattern = Pattern.compile(DATE_PATTERN);

	private static Pattern datetimePattern = Pattern.compile(DATETIME_PATTERN);
	
	private static final String IP_ADDRESS_PATTERN = "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

	private static Pattern ipAddressPattern = Pattern.compile(IP_ADDRESS_PATTERN);
	
	private static final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,20})"; //number,lower,upper,min 8, max 20

	private static Pattern passwordPattern = Pattern.compile(PASSWORD_PATTERN);
	
	private static final String USERNAME_PATTERN = "((?=.*\\d)(?=.*[A-Z]).{8,20})";

	private static Pattern usernamePattern = Pattern.compile(USERNAME_PATTERN);
	
	public static boolean containsSpace(String str) {
		return str.contains(" ");
	}
	
	
	public static boolean isValidPassword(String passwordhere) {

	    Pattern specailCharPatten = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
	    Pattern UpperCasePatten = Pattern.compile("[A-Z ]");
	    Pattern lowerCasePatten = Pattern.compile("[a-z ]");
	    Pattern digitCasePatten = Pattern.compile("[0-9 ]");
	   // errorList.clear();

	    boolean flag=true;

	    if (!specailCharPatten.matcher(passwordhere).find()) {
	        //errorList.add("Password must have atleast one specail character !!");
	        flag=false;
	    }
	    if (!UpperCasePatten.matcher(passwordhere).find()) {
	       // errorList.add("Password must have atleast one uppercase character !!");
	        flag=false;
	    }
	    if (!lowerCasePatten.matcher(passwordhere).find()) {
	        //errorList.add("Password must have atleast one lowercase character !!");
	        flag=false;
	    }
	    if (!digitCasePatten.matcher(passwordhere).find()) {
	        //errorList.add("Password must have atleast one digit character !!");
	        flag=false;
	    }

	    System.out.println("Status FOrmat pasword : " + flag);
	    return flag;

	}

	public static boolean isBlank(String value) {
		return (value == null || value.trim().equalsIgnoreCase(""));
	}
	
	public static boolean isBlank(Long value) {
		return (value == null);
	}

	public static boolean isEmpty(String value) {
		return (value == null || value.equalsIgnoreCase(""));
	}

	@SuppressWarnings("rawtypes")
	public static boolean isEmpty(List list) {
		return (list == null || list.size() == 0);
	}

	public static boolean isEmpty(MultipartFile multipartFile) {
		if (multipartFile == null || multipartFile.isEmpty() || multipartFile.getSize() == 0) {
			return true;
		}
		return false;
	}

	public static boolean isInvalidDate(String date) {
		return isInvalidDate(date, "dd/MM/yyyy");
	}

	public static boolean isInvalidDate(String date, String dateFormat) {
		try {
			String[] dates = date.split("/");
			if (dates.length != 3) {
				return true;
			}

			if (dates[0].length() != 2) {
				return true;
			}

			if (dates[1].length() != 2) {
				return true;
			}

			if (dates[2].length() != 4) {
				return true;
			}

			SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
			sdf.setLenient(false);
			sdf.parse(date);
			return false;
		} catch (ParseException e) {
			return true;
		}
	}

	public static boolean isInvalidEmail(String email) {
		return !emailPattern.matcher(email).matches();
	}

	public static boolean isInvalidDatePicker(String date) {
		return !datePattern.matcher(date).matches();
	}

	public static boolean isInvalidDateTimePicker(String date) {
		return !datetimePattern.matcher(date).matches();
	}
	
	public static boolean isInvalidIpAddress(String ipAddress) {
		return !ipAddressPattern.matcher(ipAddress).matches();
	}
	
	public static boolean isInvalidUsername(String username) {
		return !usernamePattern.matcher(username).matches();
	}
	
	public static boolean isInvalidUsernameAndPassword(String username, String password) {
		Boolean status = false ;
		
		if(username.equalsIgnoreCase(password))
			status = true;
		
		System.out.println("status cek pwd & username : " + status);
		
		return status;
	}
	
	
	public static boolean isInvalidPassword(String password) {
		return !passwordPattern.matcher(password).matches();
	}


	public static boolean isInvalidNpwp(String npwp) {
		// 99.999.999.9-999.999
		return !Pattern.compile("^\\d{2}(\\.\\d{3}){2}\\.\\d\\-\\d{3}\\.\\d{3}").matcher(npwp).matches();
	}

	public static boolean isInvalidHHmm(String hhmm) {
		// 99.99
		return !Pattern.compile("^\\d{2}\\.\\d{2}").matcher(hhmm).matches();
	}

	public static boolean isInvalidMoneyValue(String money) {
		if (money.length() <= 3) {
			// 999
			return !Pattern.compile("^\\d{1,3}").matcher(money).matches();
		}
		return !Pattern.compile("^(\\d{1,3}\\.)(\\d{3}\\.)*\\d{3}").matcher(money).matches();
	}

	public static boolean isLengthGreaterThan(String value, int maxLenght) {
		return value.length() > maxLenght;
	}

	public static boolean isLengthLessThan(String value, int minLenght) {
		return value.length() < minLenght;
	}
	
	public static boolean isLessOrEqualTo(String value, int minLenght) {
		return value.length() <= minLenght;
	}

	public static boolean isNotEquals(String str1, String str2) {
		return !str1.equals(str2);
	}

	public static boolean isNotEqualsIgnoreCase(String str1, String str2) {
		return !str1.equalsIgnoreCase(str2);
	}

	public static boolean isNotAlphaOnly(String str) {
		return !Pattern.compile("^[a-zA-Z]+").matcher(str).matches();
	}

	public static boolean isNotAlphaNumericOnly(String str) {
		return !Pattern.compile("^[a-zA-Z0-9]+").matcher(str).matches();
	}

	public static boolean isNotAlphaSpaceOnly(String str) {
		return isNotAlphaOnly(str.replace(" ", ""));
	}

	public static boolean isNotNumericOnly(String str) {
		return !Pattern.compile("^\\d+").matcher(str).matches();
	}

	public static boolean isNotNumberDecimal(String str) {
		if (str.indexOf(",") != str.lastIndexOf(",")) {
			return false;
		}
		return !Pattern.compile("[0-9]+(\\,[0-9]+)?").matcher(str).matches();
	}

	public static boolean isNotNumberDecimalDot(String str) {
		if (str.indexOf(".") != str.lastIndexOf(".")) {
			return false;
		}
		return !Pattern.compile("[0-9]+(\\.[0-9]+)?").matcher(str).matches();
	}

	public static boolean isAlphaNumericSpace(String str) {
		return Pattern.compile("^[a-zA-Z0-9 ]+").matcher(str).matches();
	}

	public static boolean isZero(String str) {
		return str.equalsIgnoreCase("0");
	}

	public static boolean isNotPrice(String str) {
		return !Pattern.compile("[1-9]{1}\\d*").matcher(str).matches();
	}

	public static boolean isNull(Object obj) {
		return obj == null;
	}

	public static boolean isContainDot(String str) {
		return str.contains(".");
	}

	public static boolean isValueGreaterThan(String value, Number maxValue) {
		if (maxValue instanceof Byte) {
			return Byte.parseByte(value) > maxValue.byteValue();
		} else if (maxValue instanceof Short) {
			return Short.parseShort(value) > maxValue.shortValue();
		} else if (maxValue instanceof Integer) {
			return Integer.parseInt(value) > maxValue.intValue();
		} else if (maxValue instanceof Long) {
			return Long.parseLong(value) > maxValue.longValue();
		} else if (maxValue instanceof Float) {
			return Float.parseFloat(value) > maxValue.floatValue();
		} else {
			return Double.parseDouble(value) > maxValue.doubleValue();
		}
	}

	public static boolean isValueLessThan(String value, Number minValue) {
		if (minValue instanceof Byte) {
			return Byte.parseByte(value) < minValue.byteValue();
		} else if (minValue instanceof Short) {
			return Short.parseShort(value) < minValue.shortValue();
		} else if (minValue instanceof Integer) {
			return Integer.parseInt(value) < minValue.intValue();
		} else if (minValue instanceof Long) {
			return Long.parseLong(value) < minValue.longValue();
		} else if (minValue instanceof Float) {
			return Float.parseFloat(value) < minValue.floatValue();
		} else {
			return Double.parseDouble(value) < minValue.doubleValue();
		}
	}

	public static boolean isNotInSelections(String value, String... selections) {
		List<String> selectionList = new ArrayList<String>();
		Collections.addAll(selectionList, selections);
		return !(selectionList.contains(value));
	}

	public static boolean isInvalidTime(String timeStr) {
		try {
			String[] dates = timeStr.split(":");
			if (dates.length != 2) {
				return true;
			}

			if (dates[0].length() != 2) {
				return true;
			}

			if (dates[1].length() != 2) {
				return true;
			}

			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
			sdf.setLenient(false);
			sdf.parse(timeStr);
			return false;
		} catch (ParseException e) {
			return true;
		}
	}

	public static boolean isInvalidDateTime(String dateTimeStr) {
		String[] dateTimes = dateTimeStr.split(" ");
		if (dateTimes.length != 2) {
			return true;
		}

		if (dateTimes[0].length() != 10) {
			return true;
		}

		if (dateTimes[1].length() != 5) {
			return true;
		}

		if (isInvalidDate(dateTimes[0])) {
			return true;
		} else if (isInvalidTime(dateTimes[1])) {
			return true;
		}
		return false;
	}
	
	
	public static boolean isGeneralPwd(String password, String generalPwd) {
		boolean status = false;
		
		List<String> items = Arrays.asList(generalPwd.split("\\s*,\\s*"));
		for (String item : items) {
			System.out.println("general pwd : "+ item);
			if(password.equalsIgnoreCase(item)) {
				System.out.println("password : " + password + " same with general pwd : "+ item);
				status = true;
				
				break;
				
			}
		}
		
		System.out.println("status gen pwd  : " + status );
		return status;
	}
}
