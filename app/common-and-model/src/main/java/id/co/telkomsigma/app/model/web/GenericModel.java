package id.co.telkomsigma.app.model.web;

import java.io.Serializable;

/**
 * Created by daniel on 4/8/15.
 */
public class GenericModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3709853605619011777L;
	public static final short MAX_LENGTH_GENERIC_STRING = 100;
	public static final short MAX_LENGTH_ADDRESS = 100;
	public static final short MAX_LENGTH_AUTHORITY = 30;
	public static final short MAX_LENGTH_DEFAULT_CAPTCHA = 4;
	public static final short MAX_LENGTH_CITY = 25;
	public static final short MAX_LENGTH_CUST_CODE_CONVENTIONAL = 30;
	public static final short MAX_LENGTH_CUST_CODE_TAKAFUL = 30;
	public static final short MAX_LENGTH_DESCRIPTION = 100;
	public static final short MAX_LENGTH_EMAIL = 50;
	public static final short MAX_LENGTH_NAME = 100;
	public static final short MAX_LENGTH_PASSWORD = 20;
	public static final short MAX_LENGTH_POSTAL = 5;
	public static final short MAX_LENGTH_PHONE = 12;
	public static final short MAX_LENGTH_PHONE_MOBILE = 12;
	public static final short MAX_LENGTH_PROVINCE = 25;
	public static final short MAX_LENGTH_USERNAME = 20;
	public static final short MAX_LENGTH_AREAL_CODE = 15;
	public static final short MAX_LENGTH_VERIFICATION_CODE = 100;

	public static final short MIN_LENGTH_PASSWORD = 8;
	public static final short MIN_LENGTH_PHONE = 9;
	public static final short MIN_LENGTH_PHONE_MOBILE = 9;
	public static final short MIN_LENGTH_USERNAME = 5;
	public static final short MIN_LENGTH_AUTHORITY = 2;

	/***************************** - Menu - ****************************/
	public static final short MAX_LENGTH_MENU_PATH = 50;
	public static final short MAX_LENGTH_MENU_NAME = 30;
	public static final short MAX_LENGTH_MENU_CREATED_BY = 30;
	public static final short MAX_LENGTH_MENU_UPDATE_BY = 30;

	/***************************** - Fungtion - ****************************/
	public static final short MAX_LENGTH_FUNCTION_TYPE = 50;
	public static final short MAX_LENGTH_FUNCTION_NAME = 50;
	public static final short MAX_LENGTH_FUNCTION_DESC = 100;
	public static final short MAX_LENGTH_FUNCTION_LIST_MENU_ID = 2000;

	/*****************************
	 * - Constant Batch Process -
	 ****************************/
	public static final short MAX_LENGTH_BATCH_ID = 20;

	/***************************** - Report - ****************************/
	public static final short MAX_LENGTH_REPORT_NAME = 20;

	/***************************** - Member - ****************************/
	public static final short MAX_LENGTH_JENIS_KELAMIN = 2;
	public static final short MAX_LENGTH_PENDIDIKAN = 2;
	public static final short MAX_LENGTH_PENGHASILAN = 2;

	/***************************** - Member - ****************************/
	public static final short MAX_LENGTH_IMEI = 16;
	public static final short MAX_LENGTH_RAW = 8000;
	public static final short MAX_LENGTH_REFF_NO = 50;

	/***************************** - PARAMETER - ****************************/
	public static final short MAX_LENGTH_PARAM_GROUP = 20;
	public static final short MAX_LENGTH_PARAM_NAME = 50;
	public static final short MAX_LENGTH_PARAM_VALUE = 500;

	public static final short MAX_LENGTH_LONGITUDE = 25;
	public static final short MAX_LENGTH_LATITUDE = 25;
	public static final short MAX_LENGTH_GPS_ACCURACY = 25;

	/***************************** - DOWNLOAD - ****************************/
	public static final short MAX_LENGTH_FILE_NAME = 75;

	public static short getMaxLengthJenisKelamin() {
		return MAX_LENGTH_JENIS_KELAMIN;
	}

	public static short getMaxLengthAddress() {
		return MAX_LENGTH_ADDRESS;
	}

	public static short getMaxLengthAuthority() {
		return MAX_LENGTH_AUTHORITY;
	}

	public static short getMaxLengthDefaultCaptcha() {
		return MAX_LENGTH_DEFAULT_CAPTCHA;
	}

	public static short getMaxLengthCity() {
		return MAX_LENGTH_CITY;
	}

	public static short getMaxLengthReportName() {
		return MAX_LENGTH_REPORT_NAME;
	}

	public static short getMaxLengthCustCodeConventional() {
		return MAX_LENGTH_CUST_CODE_CONVENTIONAL;
	}

	public static short getMaxLengthCustCodeTakaful() {
		return MAX_LENGTH_CUST_CODE_TAKAFUL;
	}

	public static short getMaxLengthDescription() {
		return MAX_LENGTH_DESCRIPTION;
	}

	public static short getMaxLengthEmail() {
		return MAX_LENGTH_EMAIL;
	}

	public static short getMaxLengthName() {
		return MAX_LENGTH_NAME;
	}

	public static short getMaxLengthPassword() {
		return MAX_LENGTH_PASSWORD;
	}

	public static short getMaxLengthPostal() {
		return MAX_LENGTH_POSTAL;
	}

	public static short getMaxLengthPhone() {
		return MAX_LENGTH_PHONE;
	}

	public static short getMaxLengthPhoneMobile() {
		return MAX_LENGTH_PHONE_MOBILE;
	}

	public static short getMaxLengthProvince() {
		return MAX_LENGTH_PROVINCE;
	}

	public static short getMaxLengthUsername() {
		return MAX_LENGTH_USERNAME;
	}

	public static short getMaxLengthVerificationCode() {
		return MAX_LENGTH_VERIFICATION_CODE;
	}

	public static short getMinLengthPassword() {
		return MIN_LENGTH_PASSWORD;
	}

	public static short getMinLengthPhone() {
		return MIN_LENGTH_PHONE;
	}

	public static short getMinLengthPhoneMobile() {
		return MIN_LENGTH_PHONE_MOBILE;
	}

	public static short getMinLengthUsername() {
		return MIN_LENGTH_USERNAME;
	}

	public static short getMaxLengthMenuPath() {
		return MAX_LENGTH_MENU_PATH;
	}

	public static short getMaxLengthMenuName() {
		return MAX_LENGTH_MENU_NAME;
	}

	public static short getMaxLengthMenuCreatedBy() {
		return MAX_LENGTH_MENU_CREATED_BY;
	}

	public static short getMaxLengthMenuUpdateBy() {
		return MAX_LENGTH_MENU_UPDATE_BY;
	}
}