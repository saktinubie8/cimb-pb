package id.co.telkomsigma.app.pojo;

public class EndpointGroupChildPojo {
	private String endpointName;
	private String groupName;
	public String getEndpointName() {
		return endpointName;
	}
	public void setEndpointName(String endpointName) {
		this.endpointName = endpointName;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	
	
}
