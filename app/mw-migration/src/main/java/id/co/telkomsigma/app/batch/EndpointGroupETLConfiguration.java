package id.co.telkomsigma.app.batch;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.springframework.batch.core.Step;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.database.JpaItemWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.RowMapper;

import id.co.telkomsigma.app.model.web.soa.EndpointGroup;


@Configuration
public class EndpointGroupETLConfiguration extends BaseSpringBatchConfiguration{
	    @Bean
	    public ItemReader<EndpointGroup> endpointGroupReader() {	    	
	    	JdbcCursorItemReader<EndpointGroup> jdbc = new JdbcCursorItemReader<>();
	    	jdbc.setSql("select * from cfg_endpoint_group");
	    	jdbc.setDataSource(mssqlDatasource);
	    	jdbc.setRowMapper(new RowMapper<EndpointGroup>() {
	    		@Override
	    		public EndpointGroup mapRow(ResultSet rs, int arg1) throws SQLException {
	    			EndpointGroup old = new EndpointGroup();	
	    			old.setId(rs.getLong("id"));
	    			old.setGroupName(rs.getString("group_name"));
	    			old.setStatus(rs.getBoolean("status"));	    			
	    			return old;
	    		}
			});
	    	return jdbc;
	    }
	    
	    @Bean
	    public ItemProcessor endpointGroupProcessor(){
	      return new ItemProcessor<EndpointGroup,EndpointGroup>(){
	    	  
				@Override
				public EndpointGroup process(EndpointGroup old) throws Exception {
					EndpointGroup newObject = endpointGroupDao.findByGroupName(old.getGroupName());
					if(newObject==null)
						newObject = new EndpointGroup();  
					newObject.setGroupName(old.getGroupName());
					newObject.setStatus(old.getStatus());										
					return newObject;
				}
			};
	     }

	    
	    @Bean
	    public ItemWriter endpointGroupWriter(){ 
	        JpaItemWriter<EndpointGroup> writer = new JpaItemWriter<EndpointGroup>();	        
	        writer.setEntityManagerFactory(entityManager.getEntityManagerFactory());
	        return writer;
	    }
	
	    
	  @Bean
	  public Step migrateEndpointGroup() {
	    return stepBuilderFactory.get("migrateEndpointGroup")
	    	.<EndpointGroup,EndpointGroup>chunk(10000)
	    	.reader(endpointGroupReader())
	    	.processor(endpointGroupProcessor())
	    	.writer(endpointGroupWriter())
	        .build();
	  }
}
