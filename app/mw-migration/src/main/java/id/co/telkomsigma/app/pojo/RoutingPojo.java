package id.co.telkomsigma.app.pojo;

public class RoutingPojo {
	private Long id;
	private String method;
	private Boolean status;
	private Long endpointGroupId;
	private String endpointGroupName;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	public Long getEndpointGroupId() {
		return endpointGroupId;
	}
	public void setEndpointGroupId(Long endpointGroupId) {
		this.endpointGroupId = endpointGroupId;
	}
	public String getEndpointGroupName() {
		return endpointGroupName;
	}
	public void setEndpointGroupName(String endpointGroupName) {
		this.endpointGroupName = endpointGroupName;
	}
	
	
}
