package id.co.telkomsigma.app.configuration;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.jmx.export.MBeanExporter;

@Configuration
public class MwDatabaseConfig {
	protected Log log = LogFactory.getLog(this.getClass());
	
	@Autowired(required = false)
	private MBeanExporter mbeanExporter;
	
	
	@Bean(name="mssqlDataSource")
	@ConfigurationProperties(prefix="spring.mssqlDatasource")
	@ConditionalOnProperty(prefix = "spring.mssqlDatasource", name = { "url" })
	public DataSource mssqlDataSource()
	{
		return DataSourceBuilder.create().build();
	}	
	
/*	private void excludeMBeanIfNecessary(Object candidate, String beanName) {
		if ((this.mbeanExporter != null)
				&& (JmxUtils.isMBean(candidate.getClass())))
			this.mbeanExporter.addExcludedBean(beanName);
	}*/
}
