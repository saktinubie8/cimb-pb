package id.co.telkomsigma.app.batch;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.springframework.batch.core.Step;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.database.JpaItemWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.RowMapper;

import id.co.telkomsigma.app.model.web.soa.Endpoint;

@Configuration
public class EndpointETLConfiguration extends BaseSpringBatchConfiguration{
	    @Bean
	    public ItemReader<Endpoint> endpointReader() {	    	
	    	JdbcCursorItemReader<Endpoint> jdbc = new JdbcCursorItemReader<>();
	    	jdbc.setSql("select * from cfg_endpoint");
	    	jdbc.setDataSource(mssqlDatasource);
	    	jdbc.setRowMapper(new RowMapper<Endpoint>() {
	    		@Override
	    		public Endpoint mapRow(ResultSet rs, int arg1) throws SQLException {
	    			Endpoint old = new Endpoint();	
	    			old.setId(rs.getLong("id"));
	    			old.setDescription(rs.getString("description"));
	    			old.setJmsQueueOut(rs.getString("jms_queue_out"));
	    			old.setName(rs.getString("name"));
	    			old.setStatus(rs.getBoolean("status"));	    			
	    			return old;
	    		}
			});
	    	return jdbc;
	    }
	    
	    @Bean
	    public ItemProcessor endpointProcessor(){
	      return new ItemProcessor<Endpoint,Endpoint>(){
	    	  
				@Override
				public Endpoint process(Endpoint old) throws Exception {
					Endpoint newObject = endpointDao.findByName(old.getName());
					if(newObject==null)
						newObject = new Endpoint();  
					newObject.setName(old.getName());
					newObject.setDescription(old.getDescription());
					newObject.setJmsQueueOut(old.getJmsQueueOut());
					newObject.setJmsType("queue");
					newObject.setStatus(old.getStatus());										
					return newObject;
				}
			};
	     }

	    
	    @Bean
	    public ItemWriter endpointWriter(){ 
	        JpaItemWriter<Endpoint> writer = new JpaItemWriter<Endpoint>();	        
	        writer.setEntityManagerFactory(entityManager.getEntityManagerFactory());
	        return writer;
	    }
	
	    
	  @Bean
	  public Step migrateEndpoint() {
	    return stepBuilderFactory.get("migrateEndpoint")
	    	.<Endpoint,Endpoint>chunk(10000)
	    	.reader(endpointReader())
	    	.processor(endpointProcessor())
	    	.writer(endpointWriter())
	        .build();
	  }
}
