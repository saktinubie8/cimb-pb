package id.co.telkomsigma.app;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@ComponentScan
@Configuration
@EnableBatchProcessing
@EnableAutoConfiguration
@EnableScheduling
@EnableCaching
public class Application {
  
    public static void main(String[] args) {
       SpringApplication.run(Application.class, args);
    }
      
    
}