package id.co.telkomsigma.app.batch;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.springframework.batch.core.Step;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.database.JpaItemWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.RowMapper;

import id.co.telkomsigma.app.model.web.soa.Routing;
import id.co.telkomsigma.app.pojo.RoutingPojo;


@Configuration
public class RoutingETLConfiguration extends BaseSpringBatchConfiguration{
	    @Bean
	    public ItemReader<RoutingPojo> routingReader() {	    	
	    	JdbcCursorItemReader<RoutingPojo> jdbc = new JdbcCursorItemReader<>();
	    	jdbc.setSql("select r.id,r.method,r.status,r.endpoint_group_id,eg.group_name from cfg_routing r,cfg_endpoint_group eg where r.endpoint_group_id=eg.id");
	    	jdbc.setDataSource(mssqlDatasource);
	    	jdbc.setRowMapper(new RowMapper<RoutingPojo>() {
	    		@Override
	    		public RoutingPojo mapRow(ResultSet rs, int arg1) throws SQLException {
	    			RoutingPojo old = new RoutingPojo();	
	    			old.setId(rs.getLong("id"));
	    			old.setMethod(rs.getString("method"));
	    			old.setEndpointGroupId(rs.getLong("endpoint_group_id"));
	    			old.setEndpointGroupName(rs.getString("group_name"));	    			
	    			old.setStatus(rs.getBoolean("status"));	    			
	    			return old;
	    		}
			});
	    	return jdbc;
	    }
	    
	    @Bean
	    public ItemProcessor routingProcessor(){
	      return new ItemProcessor<RoutingPojo,Routing>(){
	    	  
				@Override
				public Routing process(RoutingPojo old) throws Exception {
					Routing newObject = routingDao.findByMethod(old.getMethod());
					if(newObject==null)
						newObject = new Routing();  
					newObject.setMethod(old.getMethod());
					newObject.setStatus(old.getStatus());
					newObject.setEndpointGroup(endpointGroupDao.findByGroupName(old.getEndpointGroupName()));
					return newObject;
				}
			};
	     }

	    
	    @Bean
	    public ItemWriter routingWriter(){ 
	        JpaItemWriter<Routing> writer = new JpaItemWriter<Routing>();	        
	        writer.setEntityManagerFactory(entityManager.getEntityManagerFactory());
	        return writer;
	    }
	
	    
	  @Bean
	  public Step migrateRouting() {
	    return stepBuilderFactory.get("migrateRouting")
	    	.<RoutingPojo,Routing>chunk(10000)
	    	.reader(routingReader())
	    	.processor(routingProcessor())
	    	.writer(routingWriter())
	        .build();
	  }
}
