package id.co.telkomsigma.app.batch;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import id.co.telkomsigma.app.web.dao.EndpointDao;
import id.co.telkomsigma.app.web.dao.EndpointGroupChildDao;
import id.co.telkomsigma.app.web.dao.EndpointGroupDao;
import id.co.telkomsigma.app.web.dao.RoutingDao;

public class BaseSpringBatchConfiguration {
	protected Log log = LogFactory.getLog(this.getClass());
	  @Autowired
	  protected EntityManager entityManager;	
	  
	  @Autowired
	  protected EntityManagerFactory entityManagerFactory;	  
	  
	  @Autowired
	  @Qualifier("mssqlDataSource")
	  protected DataSource mssqlDatasource;
	  
	  @Autowired
	  protected StepBuilderFactory stepBuilderFactory;
	  
	  @Autowired
	  protected ThreadPoolTaskExecutor threadPoolTaskExecutor;	
	  
	  @Autowired
	  protected EndpointDao endpointDao;
	  
	  @Autowired
	  protected EndpointGroupDao endpointGroupDao;
	  
	  @Autowired
	  protected RoutingDao routingDao;
	  
	  @Autowired
	  protected EndpointGroupChildDao endpointGroupChildDao;
}
