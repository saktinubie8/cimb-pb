package id.co.telkomsigma.app.batch;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.springframework.batch.core.Step;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.database.JpaItemWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.RowMapper;

import id.co.telkomsigma.app.model.web.soa.Endpoint;
import id.co.telkomsigma.app.model.web.soa.EndpointGroup;
import id.co.telkomsigma.app.model.web.soa.EndpointGroupChild;
import id.co.telkomsigma.app.pojo.EndpointGroupChildPojo;;


@Configuration
public class EndpointGroupChildETLConfiguration extends BaseSpringBatchConfiguration{
	    @Bean
	    public ItemReader<EndpointGroupChildPojo> endpointGroupChildReader() {	    	
	    	JdbcCursorItemReader<EndpointGroupChildPojo> jdbc = new JdbcCursorItemReader<>();
	    	jdbc.setSql("select e.name as endpoint_name,eg.group_name as group_name from cfg_endpoint_group_child egc,cfg_endpoint e,cfg_endpoint_group eg where egc.endpoint_id=e.id and egc.group_id=eg.id");
	    	jdbc.setDataSource(mssqlDatasource);
	    	jdbc.setRowMapper(new RowMapper<EndpointGroupChildPojo>() {
	    		@Override
	    		public EndpointGroupChildPojo mapRow(ResultSet rs, int arg1) throws SQLException {
	    			EndpointGroupChildPojo old = new EndpointGroupChildPojo();	
	    			old.setEndpointName(rs.getString("endpoint_name"));
	    			old.setGroupName(rs.getString("group_name"));			
	    			return old;
	    		}
			});
	    	return jdbc;
	    }
	    
	    @Bean
	    public ItemProcessor endpointGroupChildProcessor(){
	      return new ItemProcessor<EndpointGroupChildPojo,EndpointGroupChild>(){
	    	  
				@Override
				public EndpointGroupChild process(EndpointGroupChildPojo old) throws Exception {
					Endpoint endpoint = endpointDao.findByName(old.getEndpointName());
					EndpointGroup endpointGroup = endpointGroupDao.findByGroupName(old.getGroupName());					
					EndpointGroupChild newObject = endpointGroupChildDao.findByEndpointAndGroup(endpoint,endpointGroup); 
					if(newObject==null)
					{
						newObject = new EndpointGroupChild();
						newObject.setEndpoint(endpoint);
						newObject.setGroup(endpointGroup);
					}										
					return newObject;
				}
			};
	     }

	    
	    @Bean
	    public ItemWriter endpointGroupChildWriter(){ 
	        JpaItemWriter<EndpointGroupChild> writer = new JpaItemWriter<EndpointGroupChild>();	        
	        writer.setEntityManagerFactory(entityManager.getEntityManagerFactory());
	        return writer;
	    }
	
	    
	  @Bean
	  public Step migrateEndpointGroupChild() {
	    return stepBuilderFactory.get("migrateEndpointGroupChild")
	    	.<EndpointGroupChildPojo,EndpointGroupChild>chunk(10000)
	    	.reader(endpointGroupChildReader())
	    	.processor(endpointGroupChildProcessor())
	    	.writer(endpointGroupChildWriter())
	        .build();
	  }
}
