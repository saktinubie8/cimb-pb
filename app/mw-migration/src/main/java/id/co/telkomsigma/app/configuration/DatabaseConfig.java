package id.co.telkomsigma.app.configuration;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.jmx.export.MBeanExporter;

@Configuration
@EnableConfigurationProperties({ DataSourceProperties.class })
public class DatabaseConfig {
	protected Log log = LogFactory.getLog(this.getClass());
	
	@Autowired(required = false)
	private MBeanExporter mbeanExporter;
	
	@Bean
	@Primary
	@ConfigurationProperties(prefix="spring.datasource")
	@ConditionalOnProperty(prefix = "spring.datasource", name = { "url" })
	public DataSource primary()
	{
		return DataSourceBuilder.create().build();
	}	
}
