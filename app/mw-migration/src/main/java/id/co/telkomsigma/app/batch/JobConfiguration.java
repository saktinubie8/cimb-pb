package id.co.telkomsigma.app.batch;

import java.util.Date;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersIncrementer;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JobConfiguration {
	  @Autowired
	  protected JobBuilderFactory jobBuilderFactory;
	  
	  @Autowired
	  @Qualifier("migrateEndpoint")
	  private Step migrateEndpoint;
	  
	  @Autowired
	  @Qualifier("migrateEndpointGroup")
	  private Step migrateEndpointGroup;
	  
	  @Autowired
	  @Qualifier("migrateRouting")
	  private Step migrateRouting;
	  
	  @Autowired
	  @Qualifier("migrateEndpointGroupChild")
	  private Step migrateEndpointGroupChild;
	  
	  @Bean
	  public Job migrationJob() throws Exception {
	    return jobBuilderFactory.get("migrationJob")
	        .incrementer(new JobParametersIncrementer(){

				@Override
				public JobParameters getNext(JobParameters parameters) {
					JobParameters params = (parameters == null) ? new JobParameters() : parameters;

					long id = new Date().getTime();
					return new JobParametersBuilder(params).addLong("run.id", id).toJobParameters();
				}
	        	
	        })
	        .start(migrateEndpoint)
	        .next(migrateEndpointGroup)
	        .next(migrateRouting)
	        .next(migrateEndpointGroupChild)
	        .build();
	  }
}
