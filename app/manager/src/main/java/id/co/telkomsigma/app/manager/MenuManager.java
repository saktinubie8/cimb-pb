package id.co.telkomsigma.app.manager;

import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import id.co.telkomsigma.app.model.web.menu.Menu;

public interface MenuManager {
	
	List<Menu> getAll();
	
	List<Menu> getListMenuId(Long roleId);
	   /**
     * Searches menu by authority, 
     * @param authority
     * @return list of menu 
     */
	List<Menu> getMenuByRole(Long[] listMenuId);
	
	List<Menu> getAllChildMenus();

	Menu getMenuByMenuId(Long menuId);
}
