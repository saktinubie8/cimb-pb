package id.co.telkomsigma.app.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

/*import id.co.telkomsigma.app.manager.RestEndpointGroupChildManager;
import id.co.telkomsigma.app.manager.RestEndpointGroupManager;
import id.co.telkomsigma.app.manager.RestEndpointGroupServiceManager;
import id.co.telkomsigma.app.manager.RestEndpointManager;
import id.co.telkomsigma.app.manager.RestServiceCatalogManager;


import id.co.telkomsigma.app.model.web.rest.RestEndpoint;
import id.co.telkomsigma.app.model.web.rest.RestEndpointGroup;
import id.co.telkomsigma.app.model.web.rest.RestEndpointGroupChild;
import id.co.telkomsigma.app.model.web.rest.RestEndpointGroupService;
import id.co.telkomsigma.app.model.web.rest.RestServiceCatalog;*/

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class RestService extends GenericService{
	
	/*
	 * @Autowired private RestEndpointManager restEndpointManager;
	 * 
	 * @Autowired private RestEndpointGroupManager restEndpointGroupManager;
	 * 
	 * @Autowired private RestServiceCatalogManager restServiceCatalogManager;
	 * 
	 * @Autowired private RestEndpointGroupChildManager
	 * restEndpointGroupChildManager;
	 * 
	 * @Autowired private RestEndpointGroupServiceManager
	 * restEndpointGroupServiceManager;
	 */
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	/*
	 * public Page<RestEndpoint> searchRestEndpoint(String keyword,String status,
	 * int finalOffset, int finalLimit, String orderBy, String sortBy) {
	 * if(keyword==null) keyword="%%"; else keyword = "%"+keyword+"%";
	 * 
	 * Direction sortByDirection = Direction.DESC; if(sortBy != null &&
	 * sortBy.equals("asc")) sortByDirection = Direction.ASC;
	 * 
	 * return restEndpointManager.search(keyword,status, (finalOffset / finalLimit)
	 * , finalLimit,orderBy,sortByDirection); }
	 * 
	 * 
	 * public RestEndpoint findRestEndpointById(Long id) { return
	 * restEndpointManager.findById(id); }
	 * 
	 * public List<RestEndpointGroup> findAllEndpointGroups(){ return
	 * restEndpointGroupManager.findAllEndpointGroups(); }
	 * 
	 * public List<RestEndpointGroupChild> findChildByEndpoint(RestEndpoint
	 * endpoint){ return
	 * restEndpointGroupChildManager.findChildByEndpoint(endpoint); }
	 * 
	 * @Transactional public RestEndpoint saveRestEndpoint(RestEndpoint
	 * restEndpoint) {
	 * 
	 * List<RestEndpointGroup> endpointGroupListFromPage =
	 * restEndpointGroupManager.findById(restEndpoint.getStrGroupLists()); //save
	 * parents restEndpointManager.saveRestEndpoint(restEndpoint);
	 * 
	 * if(endpointGroupListFromPage!=null && endpointGroupListFromPage.size()>0) {
	 * List<RestEndpointGroupChild> childList = new ArrayList<>();
	 * for(RestEndpointGroup group : endpointGroupListFromPage ){
	 * RestEndpointGroupChild child = new RestEndpointGroupChild();
	 * child.setRestEndpoint(restEndpoint); child.setRestGroup(group);
	 * childList.add(child); restEndpoint.getStrGroupLists().add(group.getId()); }
	 * restEndpoint.setGroupChilds(childList); //encode password
	 * restEndpoint.setPassword(passwordEncoder.encode(restEndpoint.getPassword()));
	 * restEndpointGroupChildManager.save(childList); } return restEndpoint; }
	 * 
	 * 
	 * @Transactional public RestEndpoint updateRestEndpoint(RestEndpoint
	 * restEndpoint) { List<RestEndpointGroup> endpointGroupListFromPage =
	 * restEndpointGroupManager.findById(restEndpoint.getStrGroupLists());
	 * RestEndpoint fromDb = restEndpointManager.findById(restEndpoint.getId());
	 * if(fromDb!=null) { //delete groupchild by endpoint id
	 * List<RestEndpointGroupChild> childByEndpoint = fromDb.getGroupChilds();
	 * //--salah karena pake lazy, ga ketarik for (RestEndpointGroupChild child :
	 * childByEndpoint){ restEndpointGroupChildManager.delete(child.getId()); }
	 * //set satu2 dari from DB ke fromDb.setName(restEndpoint.getName());
	 * fromDb.setUsername(restEndpoint.getUsername()); if
	 * (!restEndpoint.getPassword().equalsIgnoreCase("")) {
	 * //fromDb.setPassword(restEndpoint.getPassword());
	 * fromDb.setPassword(passwordEncoder.encode(restEndpoint.getPassword())); }
	 * fromDb.setIpAddress(restEndpoint.getIpAddress());
	 * fromDb.setConfirmPassword(restEndpoint.getConfirmPassword());
	 * fromDb.setStaticKey(restEndpoint.getStaticKey());
	 * fromDb.setStatus(restEndpoint.getStatus());
	 * 
	 * 
	 * if(endpointGroupListFromPage!=null && endpointGroupListFromPage.size()>0) {
	 * List<RestEndpointGroupChild> childList = new ArrayList<>();
	 * for(RestEndpointGroup group : endpointGroupListFromPage ){
	 * RestEndpointGroupChild child = new RestEndpointGroupChild();
	 * child.setRestEndpoint(restEndpoint); child.setRestGroup(group);
	 * childList.add(child); fromDb.getStrGroupLists().add(group.getId()); }
	 * fromDb.setGroupChilds(childList);
	 * restEndpointGroupChildManager.save(childList); }
	 * restEndpointManager.saveRestEndpoint(fromDb); } else { throw new
	 * RuntimeException("No endpoint found for ID "+restEndpoint.getId()); } return
	 * fromDb; }
	 * 
	 * 
	 * @Transactional public void deleteRestEndpointAndChild(RestEndpoint fromDb) {
	 * List<RestEndpointGroupChild> childs = fromDb.getGroupChilds();
	 * for(RestEndpointGroupChild ec : childs) {
	 * restEndpointGroupChildManager.delete(ec.getId()); }
	 * restEndpointManager.delete(fromDb.getId()); }
	 * 
	 * @Transactional public void deleteRestEndpoint(Long endpointId) {
	 * restEndpointManager.delete(endpointId); }
	 * 
	 * 
	 * public Page<RestEndpointGroup> searchRestEndpointGroup(String keyword,String
	 * status, int finalOffset, int finalLimit, String orderBy,String sortBy) {
	 * if(keyword==null) keyword="%%"; else keyword = "%"+keyword+"%";
	 * 
	 * 
	 * Direction sortByDirection = Direction.DESC; if(sortBy !=null &&
	 * sortBy.equals("asc")) sortByDirection = Direction.ASC; return
	 * restEndpointGroupManager.search(keyword, status,(finalOffset / finalLimit) ,
	 * finalLimit,orderBy,sortByDirection); }
	 * 
	 * public List<RestEndpointGroupService> findServiceByGroup(RestEndpointGroup
	 * restEndpointGroup){ return
	 * restEndpointGroupServiceManager.findServiceByGroup(restEndpointGroup); }
	 * 
	 * public RestEndpointGroup findRestEndpointGroupById(Long id) { return
	 * restEndpointGroupManager.findById(id); }
	 * 
	 * 
	 * public List<RestServiceCatalog> findAllServices(){ return
	 * restServiceCatalogManager.findAllServices(); }
	 * 
	 * public Page<RestServiceCatalog> searchServiceCatalog(String keyword,String
	 * status, int finalOffset, int finalLimit,String orderBy,String sortBy) {
	 * if(keyword==null) keyword="%%"; else keyword = "%"+keyword+"%";
	 * 
	 * Direction sortByDirection = Direction.DESC; if(sortBy != null &&
	 * sortBy.equals("asc")) sortByDirection = Direction.ASC;
	 * 
	 * return restServiceCatalogManager.search(keyword,status, (finalOffset /
	 * finalLimit) , finalLimit,orderBy, sortByDirection); }
	 * 
	 * public RestServiceCatalog findRestServiceCatalogById(Long id) { return
	 * restServiceCatalogManager.findById(id); }
	 * 
	 * @Transactional public void saveRestServiceCatalog(RestServiceCatalog
	 * restServiceCatalog){
	 * restServiceCatalogManager.saveRestServiceCatalog(restServiceCatalog); }
	 * 
	 * @Transactional public void updateRestServiceCatalog(RestServiceCatalog
	 * restServiceCatalog){
	 * 
	 * RestServiceCatalog fromDb =
	 * restServiceCatalogManager.findById(restServiceCatalog.getId());
	 * if(fromDb!=null){
	 * 
	 * fromDb.setName(restServiceCatalog.getName());
	 * fromDb.setDescription(restServiceCatalog.getDescription());
	 * fromDb.setStatus(restServiceCatalog.getStatus());
	 * 
	 * restServiceCatalogManager.saveRestServiceCatalog(fromDb); }
	 * 
	 * else { throw new
	 * RuntimeException("No service found for ID "+restServiceCatalog.getId() +
	 * " and service name = " + restServiceCatalog.getName()); }
	 * 
	 * }
	 * 
	 * @Transactional public void deleteRestServiceCatalog(RestServiceCatalog
	 * fromDb){
	 * 
	 * //find rest group service yang idnya sama dengan rest service yang mau
	 * didelete List<RestEndpointGroupService> groupServiceList =
	 * restEndpointGroupServiceManager.findServiceByService(fromDb);
	 * 
	 * if(groupServiceList!=null && groupServiceList.size()>0){ for
	 * (RestEndpointGroupService groupService : groupServiceList){
	 * groupService.setRestService(null); groupService.setRestGroup(null);
	 * restEndpointGroupServiceManager.delete(groupService.getId()); } }
	 * 
	 * restServiceCatalogManager.deleteRestServiceCatalog(fromDb); }
	 * 
	 * 
	 * 
	 * @Transactional public RestEndpointGroup
	 * saveRestEndpointGroup(RestEndpointGroup restEndpointGroup) {
	 * 
	 * List<RestServiceCatalog> serviceListFromPage =
	 * restServiceCatalogManager.findById(restEndpointGroup.getStrGroupServices());
	 * //save parents
	 * restEndpointGroupManager.saveRestEndpointGroup(restEndpointGroup);
	 * 
	 * if(serviceListFromPage!=null && serviceListFromPage.size()>0) {
	 * List<RestEndpointGroupService> groupServiceList = new ArrayList<>();
	 * for(RestServiceCatalog service : serviceListFromPage ){
	 * RestEndpointGroupService groupService = new RestEndpointGroupService();
	 * groupService.setRestGroup(restEndpointGroup);
	 * groupService.setRestService(service);; groupServiceList.add(groupService);
	 * restEndpointGroup.getStrGroupServices().add(service.getId()); }
	 * restEndpointGroup.setGroupServices(groupServiceList);
	 * restEndpointGroupServiceManager.saveGroupService(groupServiceList); } return
	 * restEndpointGroup; }
	 * 
	 * 
	 * @Transactional public RestEndpointGroup
	 * updateRestEndpointGroup(RestEndpointGroup restEndpointGroup) { //cari
	 * servicenya based on list di halaman List<RestServiceCatalog>
	 * serviceListFromPage =
	 * restServiceCatalogManager.findById(restEndpointGroup.getStrGroupServices());
	 * //cari groupnya based on id RestEndpointGroup fromDb =
	 * restEndpointGroupManager.findById(restEndpointGroup.getId());
	 * if(fromDb!=null) { //delete groupService by group id
	 * List<RestEndpointGroupService> serviceByGroup = fromDb.getGroupServices();
	 * for (RestEndpointGroupService groupService : serviceByGroup){
	 * groupService.setRestGroup(null); groupService.setRestService(null);
	 * restEndpointGroupServiceManager.delete(groupService.getId()); }
	 * 
	 * //set satu2 dari from DB ke fromDb.setName(restEndpointGroup.getName());
	 * fromDb.setDescription(restEndpointGroup.getDescription());
	 * fromDb.setStatus(restEndpointGroup.getStatus());
	 * 
	 * //set servicenya if(serviceListFromPage!=null &&
	 * serviceListFromPage.size()>0) { List<RestEndpointGroupService>
	 * groupServiceList = new ArrayList<>(); for(RestServiceCatalog service :
	 * serviceListFromPage ){ RestEndpointGroupService groupService = new
	 * RestEndpointGroupService(); groupService.setRestGroup(restEndpointGroup);
	 * groupService.setRestService(service);; groupServiceList.add(groupService);
	 * restEndpointGroup.getStrGroupServices().add(service.getId()); }
	 * restEndpointGroup.setGroupServices(groupServiceList);
	 * restEndpointGroupServiceManager.saveGroupService(groupServiceList); }
	 * restEndpointGroupManager.saveRestEndpointGroup(fromDb); } else { throw new
	 * RuntimeException("No rest endpoint group found for ID "+restEndpointGroup.
	 * getId() + " and name : " + restEndpointGroup.getName()); } return fromDb; }
	 * 
	 * @Transactional public void deleteRestEndpointGroupChild(RestEndpointGroup
	 * fromDb){ List<RestEndpointGroupChild> groupChildList =
	 * findGroupChildByGroup(fromDb); if(groupChildList!=null &&
	 * groupChildList.size()>0){ for (RestEndpointGroupChild groupChild :
	 * groupChildList){ groupChild.setRestEndpoint(null);
	 * groupChild.setRestGroup(null);
	 * restEndpointGroupChildManager.delete(groupChild.getId()); } } }
	 * 
	 * @Transactional public void deleteRestEndpointGroupService(RestEndpointGroup
	 * fromDb){ List<RestEndpointGroupService> groupServiceList =
	 * findServiceByGroup(fromDb); if(groupServiceList!=null &&
	 * groupServiceList.size()>0){ for (RestEndpointGroupService groupService :
	 * groupServiceList){ //set null dulu untuk restGroup dan restService biar bisa
	 * didelete groupService.setRestGroup(null); groupService.setRestService(null);
	 * restEndpointGroupServiceManager.delete(groupService.getId()); } } }
	 * 
	 * @Transactional public void deleteRestEndpointGroup(RestEndpointGroup fromDb){
	 * restEndpointGroupManager.delete(fromDb.getId()); }
	 * 
	 * public List<RestEndpointGroupChild> findGroupChildByGroup(RestEndpointGroup
	 * restEndpointGroup){ return
	 * restEndpointGroupChildManager.findGroupChildByGroup(restEndpointGroup); }
	 * 
	 * public RestEndpoint findEndpointByUsername(String username){ return
	 * restEndpointManager.findEndpointByUsername(username); }
	 * 
	 * public RestEndpointGroup findRestGroupByName(String name){ return
	 * restEndpointGroupManager.findRestGroupByName(name); }
	 * 
	 * public RestServiceCatalog findServiceByName(String name){ return
	 * restServiceCatalogManager.findServiceByName(name); } public void
	 * resetPassword(Long restEndpointId) {
	 * restEndpointManager.resetPassword(restEndpointId); }
	 */
	
	
}
