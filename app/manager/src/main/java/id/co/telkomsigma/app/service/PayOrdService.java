package id.co.telkomsigma.app.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Service;

import id.co.telkomsigma.app.model.web.user.Role;

@Service("payOrdService")
public class PayOrdService extends GenericService {

	Long inc = 1L;
	
	public String generateExtRef()  {
		
		String exref = "";
		Date dt = new Date();           
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddhhmmss");  
        String strDate = dateFormat.format(dt);  
        inc++;
        exref =  strDate.concat(inc.toString());  
		
        return exref;
    }
	
}
