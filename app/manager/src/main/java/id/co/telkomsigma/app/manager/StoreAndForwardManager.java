package id.co.telkomsigma.app.manager;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort.Direction;

import id.co.telkomsigma.app.model.web.logs.StoreAndForward;

public interface StoreAndForwardManager {

	List<StoreAndForward> getStoreAndForward();
	
	Page<StoreAndForward> searchStoreAndForward(Map<String, Object> parameterList, int offset, int limit, String orderBy, Direction sortBy);
	
	List<StoreAndForward> getStoreAndForward(int first, int max);
	
	void deleteSnf(StoreAndForward snf);
	
	StoreAndForward findById(String id);

}