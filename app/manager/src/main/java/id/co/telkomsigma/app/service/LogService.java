package id.co.telkomsigma.app.service;

import java.util.List;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.app.manager.LogManager;
import id.co.telkomsigma.app.model.web.logs.MessageLogs;

@Service("logService")
public class LogService extends GenericService {

	@Autowired
	private LogManager logManager;
	
	
	public List<MessageLogs> getMessageLogs() { 
		return  logManager.getMessageLogs(); 
	}
	
	public Page<MessageLogs> searchMessageLogs(Map<String,Object> parameterList,
			 int finalOffset, int finalLimit) { 
		return
			 logManager.searchMessageLogs(parameterList, finalOffset, finalLimit); 
		}
	
	
	public List<MessageLogs> getMessageLogs(int first, int max) {
		return logManager.getMessageLogs(first, max);
	}
	
	public Page<MessageLogs> getMessageLogsById(String messageId, int finalOffset,
			int finalLimit){
		return logManager.getMessageLogsById(messageId,finalOffset,finalLimit);
	}
	
	public Page<MessageLogs> getMessageLogsByTrxId(String trxId, int finalOffset,
			int finalLimit){
		return logManager.getMessageLogsByTrxId(trxId,finalOffset,finalLimit);
	}
		
	
}
