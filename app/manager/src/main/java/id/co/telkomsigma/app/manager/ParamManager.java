package id.co.telkomsigma.app.manager;

import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort.Direction;

import id.co.telkomsigma.app.model.web.parameter.SystemParameter;

/**
 * Created by Dzulfiqar on 11/12/15.
 */
public interface ParamManager {
    
    List<SystemParameter> getAll();
	
	Page<SystemParameter> searchParam(String keyword, String paramGroup, LinkedHashMap<String, String> orderMap, int offset, int limit,String orderBy, Direction sortBy);
	
	SystemParameter updateParam (SystemParameter param, String paramValue);
	
    SystemParameter getParamByParamId(Long paramId);

	SystemParameter getParamByParamName(String paramName);	
	
	
}
