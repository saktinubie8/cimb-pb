package id.co.telkomsigma.app.manager.impl;

import id.co.telkomsigma.app.manager.ParamManager;
import id.co.telkomsigma.app.model.web.parameter.SystemParameter;
import id.co.telkomsigma.app.web.dao.ParamDao;

import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service("paramManager")
public class ParamManagerImpl implements ParamManager {

	@Autowired
	private ParamDao paramDao;
	
	@Override
	public List<SystemParameter> getAll() {
	   return paramDao.findAll();
	}
	
    @Override
    public Page<SystemParameter> searchParam(String keyword, String paramGroup, LinkedHashMap<String, String> orderMap, int offset, int limit,String orderBy, Direction sortBy) {
        if (keyword == null) {
            keyword = "";
        }
        keyword = "%" + keyword + "%";
      
         
            PageRequest pageRequest = new PageRequest((offset / limit), limit,sortBy,orderBy);
            if (paramGroup != null && !paramGroup.equalsIgnoreCase("")) {
            	return paramDao.getParams(keyword, paramGroup, pageRequest);
            } else {
            	return paramDao.findByParamNameLikeOrParamDescriptionLikeAllIgnoreCase(keyword,keyword, pageRequest);
            }
    }

	@Override
	public SystemParameter updateParam(SystemParameter  param, String paramValue) {
		// TODO Auto-generated method stub
        SystemParameter result = paramDao.findByParameterId(param.getParameterId());
		result.setParamValue(paramValue);
		paramDao.save(result);
        return result;
	}

    @Transactional
    @Override
    public SystemParameter getParamByParamId(Long paramId) {
		SystemParameter param = paramDao.findByParameterId(paramId);
        return param;
    }

	@Override
	public SystemParameter getParamByParamName(String paramName) {
		SystemParameter param = paramDao.findByParamName(paramName);
		return param;
	}
}
