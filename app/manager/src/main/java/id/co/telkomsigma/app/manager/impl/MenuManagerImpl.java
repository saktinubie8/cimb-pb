package id.co.telkomsigma.app.manager.impl;

import id.co.telkomsigma.app.manager.MenuManager;
import id.co.telkomsigma.app.model.web.menu.Menu;
import id.co.telkomsigma.app.web.dao.MenuDao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("menuManager")
public class MenuManagerImpl implements MenuManager{

	 @Autowired
	    private MenuDao menuDao;
	
	@Override
	public List<Menu> getAll() {
		return menuDao.findAll();
	}


	@Override
	public List<Menu> getListMenuId(Long roleId) {
		return menuDao.getListMenu(roleId);
	}

	@Override
	public List<Menu> getMenuByRole(Long[] listMenuId) {
		return menuDao.findByMenuIdIn(listMenuId);
	}


	@Override
	public List<Menu> getAllChildMenus() {
		// TODO Auto-generated method stub
		return menuDao.findByMenuParentIdNotNullOrderByMenuName();
	}


	@Override
	public Menu getMenuByMenuId(Long menuId) {
		// TODO Auto-generated method stub
		return menuDao.findByMenuId(menuId);
	}
}
