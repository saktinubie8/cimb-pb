package id.co.telkomsigma.app.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.app.manager.TransactionLogsManager;
import id.co.telkomsigma.app.model.web.logs.TransactionLogs;
import org.springframework.data.domain.Sort.Direction;

@Service("trxLogService")
public class TransactionLogsService extends GenericService{

	@Autowired
	private TransactionLogsManager trxLogManager;
	
	
	public List<TransactionLogs> getTransactionLogs() { 
		return  trxLogManager.getTransactionLogs(); 
	}
	
	public Page<TransactionLogs> searchTransactionLogs(Map<String,Object> parameterList,
			 int finalOffset, int finalLimit) { 
		return
			 trxLogManager.searchTransactionLogs(parameterList, finalOffset, finalLimit); 
		}
	
	
	public List<TransactionLogs> getTransactionLogs(int first, int max) {
		return trxLogManager.getTransactionLogs(first, max);
	}
	
	public Page<TransactionLogs> getTransactionLogsById(String trxId, int finalOffset,
			int finalLimit){
		return trxLogManager.getTransactionLogsById(trxId,finalOffset,finalLimit);
	}
	
	
	
	public Page<TransactionLogs> searchTransactionLogs(Map<String,Object> parameterList, 
			LinkedHashMap<String, String> sbOrder, int finalOffset, int finalLimit, String orderBy, String sortBy) {

		
		//System.out.println("before order by : " + orderBy + " sort by : " + sortBy);
		
		if(orderBy == null || orderBy.equals(""))		
			orderBy = "createdDate";
		
		Direction sortByDirection = Direction.DESC;
		if(sortBy.equals("")) 
				sortByDirection = Direction.DESC;	
		if(sortBy.equals("asc")) 
				sortByDirection = Direction.ASC;
		
		//System.out.println("after order by : " + orderBy + " sort by : " + sortBy);
		
		return trxLogManager.searchTransactionLogs(parameterList, sbOrder, finalOffset, finalLimit, orderBy, sortByDirection);
		
		  
		
	}
}
