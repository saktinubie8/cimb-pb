package id.co.telkomsigma.app.manager.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import id.co.telkomsigma.app.model.web.logs.MessageLogs;
import id.co.telkomsigma.app.model.web.user.UsersPasswords;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.app.manager.LogManager;
import id.co.telkomsigma.app.web.dao.LogDao;

//manager message logs
@Service("logManager")
public class LogManagerImpl implements LogManager {

	@Autowired
	LogDao logDao;
	
	@PersistenceContext(unitName = "webEntityManager")
	@Qualifier(value = "webEntityManager")
	public EntityManager em;


	@Override
	public List<MessageLogs> getMessageLogs() {
		// TODO Auto-generated method stub
		return logDao.findAll();
	}

	public void setParameter(Query q, Map<String, Object> parameterList) {
		for (String key : parameterList.keySet()) {
			q.setParameter(key, parameterList.get(key));
		}
	}
	
	@Override
	public Page<MessageLogs> searchMessageLogs(Map<String, Object> parameterList, int offset, int limit) {
		// TODO Auto-generated method stub
		
		String sqlQuery = "select a from MessageLogs a where 1=1";
		String whereClause = "";

		if (parameterList.get("messageType") != null) {
			whereClause = whereClause + " and upper(a.messageType) like upper(:messageType) ";
		}
		
		if (parameterList.get("flowType") != null) {
			if(parameterList.get("flowType").equals("req"))
				whereClause = whereClause + " and a.isRequest is true ";
			else 
				whereClause = whereClause + " and a.isRequest is false ";
		}
		
		if (parameterList.get("std") != null && parameterList.get("ed") != null) {
			whereClause = whereClause + " and a.createdDate between :std and :ed ";
		}
		
		if (parameterList.get("keyword") != null) {
			whereClause = whereClause
					+ "and ( upper(a.rawData) like upper(:keyword)  or upper(a.stan) like upper(:keyword))";
		}
		
		if (parameterList.get("originator") != null) {
			whereClause = whereClause
					+ "and ( upper(a.originator) like upper(:originator))";
		}
		
		if (parameterList.get("destination") != null) {
			whereClause = whereClause
					+ "and ( upper(a.destination) like upper(:destination))";
		}

		Query q = em.createQuery(sqlQuery + whereClause + " order by a.createdDate desc");

		System.out.println("--Query Message Logs : " + sqlQuery + whereClause); 
		
		setParameter(q, parameterList);

		q.setFirstResult(offset);
		q.setMaxResults(limit);

		List<MessageLogs> retVal = q.getResultList();

		

		String sqlCount = "select count(*) from MessageLogs a where 1=1 ";
		Query q3 = em.createQuery(sqlCount + whereClause);
		setParameter(q3, parameterList);
		Long rowCount = (long) q3.getSingleResult();

		Long totalRecord = (long) retVal.size();
		
		System.out.println("==total Record : " + totalRecord);
		System.out.println("==rowCount : " + rowCount);
	
		Page page = new PageImpl<>(retVal, new PageRequest((offset / limit), limit), rowCount);

		return page;

	}
	
	@Override
	public List<MessageLogs> getMessageLogs(int first, int max) {
		// TODO Auto-generated method stub
		
		//List<MessageLogsForUI> messageLogsForUI = new ArrayList<MessageLogsForUI>();
		
		Query q = em.createQuery(" select a from MessageLogs a order by a.createdDate desc");

		//System.out.println("--Query Message Logs : " + sqlQuery + whereClause);

		//setParameter(q, parameterList);

		q.setFirstResult(first);
		q.setMaxResults(max);

		List<MessageLogs> retVal = q.getResultList();
		
		return retVal;
	}

	@Override
	public Page<MessageLogs> getMessageLogsById(String messageId, int offset, int limit) {
		// TODO Auto-generated method stub
			PageRequest pageRequest = new PageRequest((offset / limit), limit);
			return logDao.findById(UUID.fromString(messageId), pageRequest);
	}

	@Override
	public Page<MessageLogs> getMessageLogsByTrxId(String trxId, int offset, int limit) {
		// TODO Auto-generated method stub
		PageRequest pageRequest = new PageRequest((offset / limit), limit);
		return logDao.findByTransactionIdOrderByCreatedDateDesc(UUID.fromString(trxId), pageRequest);
	}

	

}
