package id.co.telkomsigma.app.service;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.sql.DataSource;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRTextExporter;
import net.sf.jasperreports.engine.export.JRTextExporterParameter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

@Service("reportService")
public class ReportService {
	protected Log log = LogFactory.getLog(this.getClass());
	
	@Autowired
	private DataSource coreDatasource;
    
    @Autowired
    private ServletContext servletContext;
	
	 public JasperPrint generateJasperPrint(String name, Map<String, Object> parameters) throws JRException, SQLException, Exception {
		 JasperDesign design =  JRXmlLoader.load(servletContext.getClassLoader().getResource("jrxml/"+name).getPath());
		 JasperReport report =  JasperCompileManager.compileReport(design);
	     return JasperFillManager.fillReport(report, parameters, coreDatasource.getConnection());
	}
	
    public void generateCsv(String name, Map<String, Object> parameters,
            OutputStream outputStream) throws Exception {
        try {
            JasperPrint jasperPrint = generateJasperPrint(name, parameters);

            JRCsvExporter exporter = new JRCsvExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream);
            exporter.exportReport();
        } catch (JRException e) {
            log.error("fail generate csv", e);
        }
    }
    
    public void generateText(String name, Map<String, Object> parameters,
            OutputStream outputStream) throws Exception {
        try {
            JasperPrint jasperPrint = generateJasperPrint(name, parameters);

            JRTextExporter exporter = new JRTextExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream);
            exporter.setParameter(JRTextExporterParameter.PAGE_WIDTH, 800);
            exporter.setParameter(JRTextExporterParameter.PAGE_HEIGHT, 400);
            exporter.exportReport();
        } catch (JRException e) {
            log.error("fail generate csv", e);
        }
    }
}
