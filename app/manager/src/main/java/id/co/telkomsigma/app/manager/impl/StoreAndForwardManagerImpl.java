package id.co.telkomsigma.app.manager.impl;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.app.manager.StoreAndForwardManager;
import id.co.telkomsigma.app.model.web.logs.MessageLogs;
import id.co.telkomsigma.app.model.web.logs.StoreAndForward;
import id.co.telkomsigma.app.web.dao.StoreAndForwardDao;

@Service("storeAndForwardManager")
public class StoreAndForwardManagerImpl implements StoreAndForwardManager {

    @Autowired
    StoreAndForwardDao snfDao;

    @PersistenceContext(unitName = "webEntityManager")
    @Qualifier(value = "webEntityManager")
    public EntityManager em;

    @Override
    public List<StoreAndForward> getStoreAndForward() {
        // TODO Auto-generated method stub
        return snfDao.findAll();
    }

    @Override
    public Page<StoreAndForward> searchStoreAndForward(Map<String, Object> parameterList, int offset, int limit, String orderBy, Direction sortBy ) {
        // TODO Auto-generated method stub
        String sqlQuery = "select a from StoreAndForward a where 1=1";
        String whereClause = "";

        if (parameterList.get("std") != null && parameterList.get("ed") != null) {
            whereClause = whereClause + " and a.createdDate between :std and :ed ";
        }

        if (parameterList.get("keyword") != null) {
            whereClause = whereClause
                    + "and upper(a.lastError) like upper(:keyword)) ";
        }

        if (parameterList.get("destination") != null) {
            whereClause = whereClause
                    + "and ( upper(a.destination) like upper(:destination))";
        }

        Query q = em.createQuery(sqlQuery + whereClause + " order by "+orderBy+" "+sortBy);

        System.out.println("--Query Store and Forward : " + sqlQuery + whereClause);

        setParameter(q, parameterList);

        q.setFirstResult(offset);
        q.setMaxResults(limit);

        List<MessageLogs> retVal = q.getResultList();

        String sqlCount = "select count(*) from StoreAndForward a where 1=1 ";
        Query q3 = em.createQuery(sqlCount + whereClause);
        setParameter(q3, parameterList);
        Long rowCount = (long) q3.getSingleResult();

        Long totalRecord = (long) retVal.size();

        System.out.println("==total Record : " + totalRecord);
        System.out.println("==rowCount : " + rowCount);

        Page page = new PageImpl<>(retVal, new PageRequest((offset / limit), limit), rowCount);

        return page;
    }

    @Override
    public List<StoreAndForward> getStoreAndForward(int first, int max) {
        // TODO Auto-generated method stub

        Query q = em.createQuery(" select a from StoreAndForward a order by a.createdDate desc");

        q.setFirstResult(first);
        q.setMaxResults(max);

        List<StoreAndForward> retVal = q.getResultList();

        return retVal;
    }

    public void setParameter(Query q, Map<String, Object> parameterList) {
        for (String key : parameterList.keySet()) {
            q.setParameter(key, parameterList.get(key));
        }
    }

    @Override
    public void deleteSnf(StoreAndForward snf) {
        // TODO Auto-generated method stub
        snfDao.delete(snf);
    }

    @Override
    public StoreAndForward findById(String id) {
        // TODO Auto-generated method stub
        return snfDao.findById(UUID.fromString(id));
    }

}