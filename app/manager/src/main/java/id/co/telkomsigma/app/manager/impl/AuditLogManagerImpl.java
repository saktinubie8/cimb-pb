package id.co.telkomsigma.app.manager.impl;

import id.co.telkomsigma.app.manager.AuditLogManager;
import id.co.telkomsigma.app.model.web.logs.MessageLogs;
import id.co.telkomsigma.app.model.web.security.AuditLog;
import id.co.telkomsigma.app.model.web.user.User;
import id.co.telkomsigma.app.util.DateUtil;
import id.co.telkomsigma.app.web.dao.AuditLogDao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service("auditLogManager")
public class AuditLogManagerImpl implements AuditLogManager {

    @Autowired
    private AuditLogDao auditLogDao;

    @PersistenceContext(unitName = "webEntityManager")
    @Qualifier(value = "webEntityManager")
    public EntityManager em;

    @Override
    public List<AuditLog> getAll() {
        return auditLogDao.findAll();
    }

    @Override
    public AuditLog insertAuditLog(AuditLog auditLog) {
        auditLog = auditLogDao.save(auditLog);
        return auditLog;
    }
/*
    @Override
    public Page<AuditLog> searchAuditLog(String keyword, String startDate, String endDate, String activityType, LinkedHashMap<String, 
    		String> orderMap, int offset, int limit, String orderBy, Direction sortBy) {
        if (keyword == null) {
            keyword = "";
        }
        keyword = "%" + keyword + "%";
        
        Date sDate = new Date();
        Date eDate = new Date();
 
		SimpleDateFormat formatter2 = new SimpleDateFormat("dd/MM/yyyy");

        PageRequest pageRequest = new PageRequest((offset / limit), limit,sortBy, orderBy);
        
        //semua tidak diisi, find all
        if(keyword.isEmpty() && startDate.isEmpty() && endDate.isEmpty() && activityType.isEmpty()){
    		System.out.println("Pencarian all tanpa kriteria, semua tidak diisi  : "  );
    		return (auditLogDao.findAll(pageRequest));
    	}
        
       //keyword diisi , date kosong dan activity kosong, tidak diinclude
        if(!keyword.isEmpty() && startDate.isEmpty() && endDate.isEmpty() && activityType.isEmpty()){
    		System.out.println("Pencarian based on keyword only   : "  );
    		return (auditLogDao.findByDescriptionLikeOrCreatedByLikeIgnoreCaseOrderByCreatedDateDesc(keyword, keyword, pageRequest));
    	}
        
        //keyword diisi, date diisi, activity type kosong
        if (!keyword.isEmpty() && !startDate.isEmpty() && !endDate.isEmpty() && activityType.isEmpty()) {
        	System.out.println("Pencarian based on keyword and date    : "  );
        	try {
				sDate = formatter2.parse(startDate);
				eDate = DateUtil.addDays(formatter2.parse(endDate), 1);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		return (auditLogDao.findByDescriptionLikeOrCreatedByLikeIgnoreCaseAndCreatedDateBetweenOrderByCreatedDateDesc(keyword, keyword, sDate, eDate, pageRequest));
        }
        
        //keyword diisi, date diisi, activity type diisi 
        if (!keyword.isEmpty() && !startDate.isEmpty() && !endDate.isEmpty() && !activityType.isEmpty()) {
        	System.out.println("Pencarian based on keyword ,date and activity type  : "  );
        	try {
				sDate = formatter2.parse(startDate);
				eDate = DateUtil.addDays(formatter2.parse(endDate), 1);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		return (auditLogDao.findByDescriptionLikeOrCreatedByLikeIgnoreCaseAndActivityTypeAndCreatedDateBetweenOrderByCreatedDateDesc(keyword,keyword,Integer.parseInt(activityType),sDate,eDate,pageRequest));
        }
        
        
      //keyword diisi, date kosong, activity type diisi 
        if (!keyword.isEmpty() && startDate.isEmpty() && endDate.isEmpty() && !activityType.isEmpty()) {
        	System.out.println("Pencarian based on keyword  and activity type  : "  );
    		return (auditLogDao.findByDescriptionLikeOrCreatedByLikeIgnoreCaseAndActivityTypeOrderByCreatedDateDesc(keyword,keyword,Integer.parseInt(activityType),pageRequest));
        }
        
        //keyword kosong, date diisi, activity type kosong 
        if (keyword.isEmpty() && !startDate.isEmpty() && !endDate.isEmpty() && activityType.isEmpty()) {
        	System.out.println("Pencarian based on date only   : "  );
        	try {
				sDate = formatter2.parse(startDate);
				eDate = DateUtil.addDays(formatter2.parse(endDate), 1);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		return (auditLogDao.findByCreatedDateBetweenOrderByCreatedDateDesc(sDate,eDate,pageRequest));
        }
        

        //keyword kosong, date diisi, activity type isi 
        if (keyword.isEmpty() && !startDate.isEmpty() && !endDate.isEmpty() && !activityType.isEmpty()) {
        	System.out.println("Pencarian based on date and acticity type   : "  );
        	try {
				sDate = formatter2.parse(startDate);
				eDate = DateUtil.addDays(formatter2.parse(endDate), 1);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		return (auditLogDao.findByCreatedDateBetweenAndActivityTypeOrderByCreatedDateDesc(sDate,eDate,Integer.parseInt(activityType),pageRequest));
        }
        
        //keyword kosong, date diisi, activity type diisi 
       
        
        
        
        
        /*
        if(activityType.isEmpty()){
        	//jika activity type kosong, 
        	//pencarian based on 
        	
        	 
        	
        	//
        	
             	if(startDate.isEmpty() && endDate.isEmpty()){
             		//cari based on keyword saja
             		return (auditLogDao.findByDescriptionLikeOrCreatedByLikeIgnoreCaseOrderByCreatedDateDesc(keyword, keyword, pageRequest));
             	}
             	else if (!startDate.isEmpty() && !endDate.isEmpty()){
             		System.out.println("Pencarian based on start date and end date  : "  );
             		//cari based on keyword dan date
					try {
						sDate = formatter2.parse(startDate);
						eDate = DateUtil.addDays(formatter2.parse(endDate), 1);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
             		return (auditLogDao.findByDescriptionLikeOrCreatedByLikeIgnoreCaseAndCreatedDateBetweenOrderByCreatedDateDesc(keyword, keyword, sDate, eDate, pageRequest));
             	}
             }
               
       

        if(startDate.isEmpty() && endDate.isEmpty()){
        	if(activityType.isEmpty()){
        		//cari based on keyword saja
        		return (auditLogDao.findByDescriptionLikeOrCreatedByLikeIgnoreCaseOrderByCreatedDateDesc(keyword,keyword,pageRequest));
        	}
        	else{
        		/*
				try {
					sDate = formatter2.parse(startDate);
					eDate = DateUtil.addDays(formatter2.parse(endDate), 1);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				*/
    //cari based on keyword dan activity type
        	/*	
        		System.out.println("Pencarian based on activity type : " + activityType );
        		return (auditLogDao.findByActivityTypeOrderByCreatedDateDesc(Integer.parseInt(activityType),pageRequest));
        	}
        }
        /*
        if((!activityType.isEmpty()) && (!startDate.isEmpty() && !endDate.isEmpty())){
        	//kondisi semua parameter terpenuhi
			try {
				sDate = formatter2.parse(startDate);
				eDate = DateUtil.addDays(formatter2.parse(endDate), 1);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	return (auditLogDao.findByDescriptionLikeOrCreatedByLikeIgnoreCaseAndActivityTypeAndCreatedDateBetweenOrderByCreatedDateDesc(keyword,keyword,Integer.parseInt(activityType),sDate,eDate,pageRequest));
        }
       
        return null;
       // return auditLogDao.findAll(pageRequest);
    }
*/

    public void setParameter(Query q, Map<String, Object> parameterList) {
        for (String key : parameterList.keySet()) {
            q.setParameter(key, parameterList.get(key));
        }
    }


    @Override
    public List<AuditLog> getAuditLog(int first, int max) {


        Query q = em.createQuery(" select a from AuditLog a order by a.createdDate desc");


        q.setFirstResult(first);
        q.setMaxResults(max);

        List<AuditLog> retVal = q.getResultList();

        return retVal;
    }


    @Override
    public Page<AuditLog> searchAuditLog(String keyword, String startDate, String endDate, String activityType,
                                         LinkedHashMap<String, String> orderMap, int offset, int limit, String orderBy, Direction sortBy) {
        return null;
    }

    @Override
    public Page<AuditLog> getAuditLogById(String messageId, int offset, int limit) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Page<AuditLog> getAuditLogByTrxId(String trxId, int offset, int limit) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Page<AuditLog> searchAuditLogs(Map<String, Object> parameterList,  LinkedHashMap<String, String> orderMap, int offset, int limit, String orderBy, Direction sortBy) {
        // TODO Auto-generated method stub
        String sqlQuery = "select a from AuditLog a where 1=1";
        String whereClause = "";

        if (parameterList.get("std") != null && parameterList.get("ed") != null) {
            whereClause = whereClause + " and a.createdDate between :std and :ed ";
        }

        if (parameterList.get("keyword") != null) {
            whereClause = whereClause
                    + "and ( upper(a.description) like upper(:keyword)  or upper(a.createdBy) like upper(:keyword))";
        }

        if (parameterList.get("activityType") != null) {
            whereClause = whereClause
                    + "and a.activityType = :activityType";
        }

        Query q = em.createQuery(sqlQuery + whereClause+" order by "+orderBy+" "+sortBy);

        System.out.println("--Query Audit Logs : " + sqlQuery + whereClause);

        setParameter(q, parameterList);

        q.setFirstResult(offset);
        q.setMaxResults(limit);

        List<MessageLogs> retVal = q.getResultList();

        String sqlCount = "select count(*) from AuditLog a where 1=1 ";
        Query q3 = em.createQuery(sqlCount + whereClause);
        System.out.println("--Query Audit Logs count : " + sqlCount + whereClause);
        setParameter(q3, parameterList);
        Long rowCount = (long) q3.getSingleResult();

        Long totalRecord = (long) retVal.size();

        System.out.println("==total Record : " + totalRecord);
        System.out.println("==rowCount : " + rowCount);
        System.out.println("sortBy : "+sortBy);
        System.out.println("orderBy : "+orderBy);

        Page page = new PageImpl<>(retVal, new PageRequest((offset / limit), limit, sortBy, orderBy), rowCount);
        return page;
    }

}