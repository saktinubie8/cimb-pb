package id.co.telkomsigma.app.manager;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.springframework.data.domain.Sort.Direction;

import org.springframework.data.domain.Page;

import id.co.telkomsigma.app.model.web.logs.TransactionLogs;

public interface TransactionLogsManager {

	List<TransactionLogs> getTransactionLogs();
	
	Page<TransactionLogs> searchTransactionLogs(Map<String, Object> parameterList, int offset, int limit);

	List<TransactionLogs> getTransactionLogs(int first, int max);
	
	Page<TransactionLogs> getTransactionLogsById(String trxId, int offset, int limit);
	
	Page<TransactionLogs> searchTransactionLogs(Map<String,Object> parameterList, 
			LinkedHashMap<String, String> sbOrder, int finalOffset, int finalLimit, String orderBy, Direction sortBy);
}
