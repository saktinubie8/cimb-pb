package id.co.telkomsigma.app.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import id.co.telkomsigma.app.model.web.logs.TransactionLogs;
import id.co.telkomsigma.app.pojo.CnfReqPojo;
import id.co.telkomsigma.app.pojo.DcnfReqPojo;
import id.co.telkomsigma.app.pojo.DrefReqPojo;
import id.co.telkomsigma.app.util.RestUtil;

@Service
public class KseiWebService extends GenericService{

	private String mainUrl = "localhost:8081";
	
	protected final Log log = LogFactory.getLog(getClass());
	
	public ResponseEntity<String> sendDcnf(DcnfReqPojo requestBody){
			RestTemplate currentRestTemplate = RestUtil.getSecureRestTemplate();
			String url = "https://"+mainUrl+"/injector/dcnf";
		    log.debug("Sending webservice to {}" + url);
			return currentRestTemplate.postForEntity(url, requestBody, String.class);
	}
		
	
	public ResponseEntity<String> sendCnf(CnfReqPojo requestBody){
			RestTemplate currentRestTemplate = RestUtil.getSecureRestTemplate();
			String url = "https://"+mainUrl+"/injector/cnf";
			log.trace("Sending webservice to {}" + url);
			return currentRestTemplate.postForEntity(url, requestBody, String.class);
	}
		
	public ResponseEntity<String> sendDref(DrefReqPojo requestBody){
			RestTemplate currentRestTemplate = RestUtil.getSecureRestTemplate();
			String url = "https://"+mainUrl+"/injector/dref";
			log.trace("Sending webservice to {}" + url);
			return currentRestTemplate.postForEntity(url, requestBody, String.class);
	}
		
	public void prosesSuccessWebserviceFromKsei(TransactionLogs trxLog,ResponseEntity response) {
			log.debug("Received webservice response from KSEI with HTTP 200 (Success) ");
			log.debug("Change transaction status to success for {}"  + trxLog);
	}
	
	
}


