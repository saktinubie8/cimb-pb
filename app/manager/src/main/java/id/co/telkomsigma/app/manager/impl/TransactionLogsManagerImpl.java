package id.co.telkomsigma.app.manager.impl;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.app.manager.TransactionLogsManager;
import id.co.telkomsigma.app.model.web.logs.MessageLogs;
import id.co.telkomsigma.app.model.web.logs.TransactionLogs;
import id.co.telkomsigma.app.web.dao.TransactionLogsDao;
import org.springframework.data.domain.Sort.Direction;

@Service("transactionLogManager")
public class TransactionLogsManagerImpl implements TransactionLogsManager{

	@Autowired
	TransactionLogsDao trxLogsDao;
	
	@PersistenceContext(unitName = "webEntityManager")
	@Qualifier(value = "webEntityManager")
	public EntityManager em;
	
	@Override
	public List<TransactionLogs> getTransactionLogs() {
		// TODO Auto-generated method stub
		return trxLogsDao.findAll();
	}


	public void setParameter(Query q, Map<String, Object> parameterList) {
		for (String key : parameterList.keySet()) {
				q.setParameter(key, parameterList.get(key));
		}
	}
	
	@Override
	public Page<TransactionLogs> searchTransactionLogs(Map<String, Object> parameterList, int offset, int limit) {
		// TODO Auto-generated method stub
		String sqlQuery = "select a from TransactionLogs a where 1=1";
		String whereClause = "";

		if (parameterList.get("keyword") != null) {
			whereClause = whereClause
				//	+ "and ( upper(a.extref) like upper(:keyword)  or upper(a.stan) like upper(:keyword))";
					+ "and ( upper(a.extRef) like upper(:keyword))";
		}
		
		if (parameterList.get("trxName") != null) {
			System.out.println("trxName:" + parameterList.get("trxName").toString().trim());
			//String trxType =  parameterList.get("trxName").toString().trim();
			whereClause = whereClause + " and a.transactionType = :trxName ";
		}
		
		
		if (parameterList.get("sender") != null) {
			whereClause = whereClause
					+ "and  a.originator = :sender";
		}
		
		if (parameterList.get("destination") != null) {
			whereClause = whereClause
					+ "and  a.destination =:destination";
		}
				
		if (parameterList.get("std") != null && parameterList.get("ed") != null) {
			whereClause = whereClause + " and cast(a.createdDate as date)  between :std and :ed ";
		}
		
		Query q = em.createQuery(sqlQuery + whereClause + " order by a.createdDate desc");

		System.out.println("--Query Message Logs : " + sqlQuery + whereClause); 
		
		setParameter(q, parameterList);

		q.setFirstResult(offset);
		q.setMaxResults(limit);

		List<MessageLogs> retVal = q.getResultList();

		

		String sqlCount = "select count(*) from TransactionLogs a where 1=1 ";
		Query q3 = em.createQuery(sqlCount + whereClause);
		setParameter(q3, parameterList);
		Long rowCount = (long) q3.getSingleResult();
		
		System.out.println("--Query Message Logs : " + sqlCount + whereClause); 
		Long totalRecord = (long) retVal.size();
		
		System.out.println("==total Record : " + totalRecord);
		System.out.println("==rowCount : " + rowCount);
	
		Page page = new PageImpl<>(retVal, new PageRequest((offset / limit), limit), rowCount);

		return page;
	}

	@Override
	public List<TransactionLogs> getTransactionLogs(int first, int max) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		
				//List<MessageLogsForUI> messageLogsForUI = new ArrayList<MessageLogsForUI>();
				
				Query q = em.createQuery(" select a from TransactionLogs a order by a.createdDate desc");


				q.setFirstResult(first);
				q.setMaxResults(max);

				List<TransactionLogs> retVal = q.getResultList();
				
				return retVal;
	
	}

	@Override
	public Page<TransactionLogs> getTransactionLogsById(String trxId, int offset, int limit) {
		// TODO Auto-generated method stub
		PageRequest pageRequest = new PageRequest((offset / limit), limit);
		return trxLogsDao.findById(UUID.fromString(trxId), pageRequest);
	}


	@Override
	public Page<TransactionLogs> searchTransactionLogs(Map<String, Object> parameterList,
			LinkedHashMap<String, String> sbOrder, int finalOffset, int finalLimit, String orderBy, Direction sortBy) {
		// TODO Auto-generated method stub
		String sqlQuery = "select a from TransactionLogs a where 1=1";
		String whereClause = "";

		if (parameterList.get("keyword") != null) {
			whereClause = whereClause
				//	+ "and ( upper(a.extref) like upper(:keyword)  or upper(a.stan) like upper(:keyword))";
					+ "and ( upper(a.extRef) like upper(:keyword))";
		}
		
		if (parameterList.get("trxName") != null) {
			System.out.println("trxName:" + parameterList.get("trxName").toString().trim());
			//String trxType =  parameterList.get("trxName").toString().trim();
			whereClause = whereClause + " and a.transactionType = :trxName ";
		}
		
		
		if (parameterList.get("sender") != null) {
			whereClause = whereClause
					+ "and  a.originator = :sender";
		}
		
		if (parameterList.get("destination") != null) {
			whereClause = whereClause
					+ "and  a.destination =:destination";
		}
				
		if (parameterList.get("std") != null && parameterList.get("ed") != null) {
			whereClause = whereClause + " and cast(a.createdDate as date)  between :std and :ed ";
		}
		
		Query q = em.createQuery(sqlQuery + whereClause + " order by " + orderBy  +" "+ sortBy );

		System.out.println("--Query Message Logs : " + sqlQuery + whereClause + " order by " + orderBy  +" "+ sortBy); 
		
		setParameter(q, parameterList);

		q.setFirstResult(finalOffset);
		q.setMaxResults(finalLimit);

		List<MessageLogs> retVal = q.getResultList();

		

		String sqlCount = "select count(*) from TransactionLogs a where 1=1 ";
		Query q3 = em.createQuery(sqlCount + whereClause);
		setParameter(q3, parameterList);
		Long rowCount = (long) q3.getSingleResult();
		
		System.out.println("--Query Message Logs count: " + sqlCount + whereClause); 
		Long totalRecord = (long) retVal.size();
		
		System.out.println("==total Record : " + totalRecord);
		System.out.println("==rowCount : " + rowCount);
	
		Page page = new PageImpl<>(retVal, new PageRequest((finalOffset / finalLimit), finalLimit), rowCount);

		return page;
	}
	
	

}
