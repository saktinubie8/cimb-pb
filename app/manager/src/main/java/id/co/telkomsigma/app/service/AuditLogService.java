package id.co.telkomsigma.app.service;

import id.co.telkomsigma.app.manager.AuditLogManager;
import id.co.telkomsigma.app.model.web.logs.MessageLogs;
import id.co.telkomsigma.app.model.web.security.AuditLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Dzulfiqar on 11/12/15.
 */
@Service("auditLogService")
public class AuditLogService extends GenericService {
    @Autowired
    private AuditLogManager auditLogManager;

    /**
     * Get all parameters
     *
     * @return list of Locations object
     */
    public List<AuditLog> getLogs() {
        return auditLogManager.getAll();
    }

    public AuditLog insertAuditLog(AuditLog auditLog) {
        return auditLogManager.insertAuditLog(auditLog);
    }

    public void clearAuditLogCache() {
    }

    public Page<AuditLog> searchAuditLog(String keyword, String startDate,
                                         String endDate, String activityType, LinkedHashMap<String, String> orderMap,
                                         int finalOffset, int finalLimit, String orderBy, String sortBy) {

        if (orderBy == null || orderBy.equals(""))
            orderBy = "auditId";

        Direction sortByDirection = Direction.DESC;
        if (sortBy != null && sortBy.equals("asc"))
            sortByDirection = Direction.ASC;
        return auditLogManager.searchAuditLog(keyword, startDate, endDate, activityType, orderMap, finalOffset, finalLimit, orderBy, sortByDirection);
    }

    public Page<AuditLog> searchAuditLogs(Map<String, Object> parameterList,
                                          LinkedHashMap<String, String> sbOrder, int finalOffset, int finalLimit, String orderBy, String sortBy) {
        if (orderBy == null || orderBy.equals(""))
            orderBy = "auditId";
        Direction sortByDirection = Direction.DESC;
        if (sortBy != null && sortBy.equals("asc"))
            sortByDirection = Direction.ASC;
        return
                auditLogManager.searchAuditLogs(parameterList, sbOrder, finalOffset, finalLimit, orderBy, sortByDirection);
    }


}