package id.co.telkomsigma.app.service;

import id.co.telkomsigma.app.manager.RoleManager;
import id.co.telkomsigma.app.model.web.user.Role;

import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

@Service("roleService")
public class RoleService extends GenericService {
    @Autowired
    private RoleManager roleManager;
    
	
    public Role getRoleById(Long roleId) {
        return roleManager.getRoleById(roleId);
    }
    

    
    public List<Role> getRoles() {
        return roleManager.getAll();
    }
    

	public Role createRole(Role role)
	{
		return roleManager.insertRole(role);
	}
	
	public Role loadUserByRoleId(final Long roleId)  {
        return roleManager.getRoleById(roleId);
    }
    

	public Role updateRole(Role role)
	{
		return roleManager.updateRole(role);
	}

	public Role loadUserByRoleName(final String authority) throws Exception  {
        return roleManager.getRoleByAuthority(authority);
    }

	public Page<Role> search(String authority,
			LinkedHashMap<String, String> columnMap, int finalOffset,
			int finalLimit,String orderBy, String sortBy) {
		// TODO Auto-generated method stub

		if(orderBy == null || orderBy.equals(""))		
			orderBy = "roleId";
		
		Direction sortByDirection = Direction.DESC;
		if(sortBy.equals("") || sortBy.equals("asc")) 
		sortByDirection = Direction.ASC;
		
		return roleManager.searchRoles(authority, columnMap, finalOffset, finalLimit, orderBy, sortByDirection);
	}
}
