package id.co.telkomsigma.app.service;

import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import id.co.telkomsigma.app.constant.WebGuiConstant;
import id.co.telkomsigma.app.exception.UserProfileException;
import id.co.telkomsigma.app.manager.UserManager;
import id.co.telkomsigma.app.model.web.user.Role;
import id.co.telkomsigma.app.model.web.user.User;
import id.co.telkomsigma.app.util.DateUtil;
import id.co.telkomsigma.app.util.ResponseCode;

import org.apache.commons.lang.math.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

/**
 * Created by daniel on 3/31/15.
 */
@Service("userService")
public class UserService extends GenericService implements UserDetailsService {
    @Autowired
    private UserManager userManager;	   
    
    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        User user = userManager.getUserByUsername(username);
        return user;
    }
    
    public User doResetPassword(User user)
    {    	
    	return userManager.resetPassword(user);
    }    

    public User loadUserByUserId(final Long userId) throws UsernameNotFoundException {
        return userManager.getUserByUserId(userId);
    }

    
  	public void lockUser(User user, final Long lockDuration) throws Exception {
		userManager.lockUser(user, lockDuration);		
    }

	
	public void unlockUser(User user) {
        userManager.unlockUser(user);
    }

	
	public User createUser(User user)
	{
		try {
			return userManager.insertUser(user);
		} catch (UserProfileException e) {
			log.error(e.getMessage(), e);
			byte[] responseBody = e.getMessage().getBytes();
			throw new HttpClientErrorException(HttpStatus.CONFLICT, "CONFLICT", responseBody, Charset.forName("UTF-8"));
		}
		//return null;
	}
	
	
	public User updateUser(User user)
	{
		return userManager.updateUser(user);
	}

	public User updateProfile(User user)
	{
		return userManager.updateProfileAdmin(user);
	}
  
    public List<User> getAllLockedUser(){
        return  userManager.getAllLockedUser();
    }

    
	public Integer failedAttempt(User user, Integer count) {
        return  userManager.failedLoginAttempt(user.getUserId(), user.getRaw(), user.isAccountNonLocked(), count.longValue() );
	}
	

    
	public User loadUserByUsernameFromUpload(final String username) throws UsernameNotFoundException {
        return  userManager.getUserByUsernameFromUpload(username);
    }
	
	
	public User createUserFromUpload(User user)
	{
		return userManager.insertUserFromUpload(user);
	}
	
	
	public User updateUserFromUpload(User user)
	{
		return userManager.updateUserFromUpload(user);
	}

	
	public void resetPassword(User user) {
		userManager.resetPassword(user);
    }

	
	public void enableUser(User user) {
		userManager.enableUser(user);
    }
	
	
	public void disableUser(User user) {
		userManager.disableUser(user);
	}
	
	

	public Page<User> searchUser(String keyword, String authority,
			LinkedHashMap<String, String> sbOrder, int finalOffset, int limit,String orderBy, String sortBy) {

		if(orderBy == null || orderBy.equals(""))		
			orderBy = "userId";
		
		Direction sortByDirection = Direction.DESC;
		if(sortBy.equals("") || sortBy.equals("asc")) 
		sortByDirection = Direction.ASC;
		return userManager.searchUsers(keyword, authority, sbOrder, finalOffset, limit, orderBy, sortByDirection);
	}
	
	public void deleteUser(User user) {
		userManager.deleteUser(user);
	}
	
	public void clearUserCache(){
		userManager.clearCache();
	}
	
	public Boolean checkDefaultPassword(User user)
	{
		return userManager.checkDefaultPassword(user);
	}
	
	public Long checkExpiryDays(User user)
	{
		return userManager.checkExpiryDays(user);
	}
	
	public Boolean checkReusePassword(User user){
		return userManager.checkReusePassword(user);
	}
}
