package id.co.telkomsigma.app.manager;


import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort.Direction;

import id.co.telkomsigma.app.model.web.user.Role;


/**
 * Created by daniel on 4/16/15.
 */
public interface RoleManager {
    /**
     * Get all roles
     * @return list of Role object
     */
    List<Role> getAll();
    

    //get role by Id, return Role
    Role getRoleById(Long roleId);
        
    Page<Role> searchRoles(String authority, LinkedHashMap<String, String> orderMap, int offset, int limit,String orderBy, Direction sortBy);
    
    Role insertRole(Role role);
    
    Role updateRole(Role role);
    
    Role getRoleByAuthority(String authority);  
}