package id.co.telkomsigma.app.manager;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort.Direction;

import id.co.telkomsigma.app.model.web.security.AuditLog;

public interface AuditLogManager {

    List<AuditLog> getAll();

    AuditLog insertAuditLog(AuditLog auditLog);

    Page<AuditLog> searchAuditLog(String keyword, String startDate, String endDate,
                                  String activityType, LinkedHashMap<String, String> orderMap, int offset, int limit, String orderBy, Direction sortBy);

    List<AuditLog> getAuditLog(int first, int max);

    Page<AuditLog> getAuditLogById(String messageId, int offset, int limit);

    Page<AuditLog> getAuditLogByTrxId(String trxId, int offset, int limit);

    Page<AuditLog> searchAuditLogs(Map<String, Object> parameterList,  LinkedHashMap<String, String> orderMap, int offset, int limit, String orderBy, Direction sortBy);

}