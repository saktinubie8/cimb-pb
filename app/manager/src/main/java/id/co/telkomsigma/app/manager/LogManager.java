package id.co.telkomsigma.app.manager;


import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;

import id.co.telkomsigma.app.model.web.logs.MessageLogs;

//message logs manager
public interface LogManager {
	
	List<MessageLogs> getMessageLogs();
	
	Page<MessageLogs> searchMessageLogs(Map<String, Object> parameterList, int offset, int limit);

	List<MessageLogs> getMessageLogs(int first, int max);
	
	Page<MessageLogs> getMessageLogsById(String messageId, int offset, int limit);
	
	Page<MessageLogs> getMessageLogsByTrxId(String trxId, int offset, int limit);

	/*
	 * List<MessageLogs> getMessageLogs();
	 * 
	 * List<MessageLogsForUI> getMessageLogsForUI(int first, int max);
	 * 
	 * Page<MessageLogs> searchMessageLogs(Map<String, Object> parameterList, int
	 * offset, int limit);
	 * 
	 * Page<MessageLogsForUI> searchMessageLogsForUI(Map<String, Object>
	 * parameterList, int offset, int limit);
	 * 
	 * Page<MessageLogs> getMessageLogsByGroupId(String keyword, String groupId, int
	 * offset, int limit);
	 * 
	 * Page<MessageLogs> getMessageLogsById(String messageId, int offset, int
	 * limit);
	 * 
	 * Page<MessageLogsForUI> getMessageLogsForUIById(String messageId, int offset,
	 * int limit);
	 * 
	 * MessageLogsAttachment getAttachmentById(Long attachmentId);
	 * 
	 * MessageLogs getMessageLogsById(Long messageId);
	 * 
	 * List<MessageLogs> findByEndpoint(Endpoint endpoint);
	 * 
	 * void updateMessageLog(MessageLogs log);
	 * 
	 * List<MessageLogs> findByEndpointGroup(EndpointGroup group);
	 * 
	 * void saveAttachment(MessageLogsAttachment attach);
	 */
}
