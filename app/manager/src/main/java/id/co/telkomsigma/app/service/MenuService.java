package id.co.telkomsigma.app.service;

import id.co.telkomsigma.app.manager.MenuManager;
import id.co.telkomsigma.app.model.web.menu.Menu;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service("menuService")
public class MenuService extends GenericService{
    @Autowired
	private MenuManager menuMgr;
    
    public List<Menu> getMenuByRole(final Long roleId) {												
		return menuMgr.getListMenuId(roleId);	
    }

    public List<Menu> getAllMenus() {
		List<Menu> retVal = new ArrayList<>();
		for(Menu menu :menuMgr.getAll())
		{
			if(!menu.getMenuPath().contains("LABEL"))
				retVal.add(menu);
		}
        return retVal;
    }	

	public Menu getMenuByMenuId(Long menuId)
	{
		return menuMgr.getMenuByMenuId(menuId);
	}
}
