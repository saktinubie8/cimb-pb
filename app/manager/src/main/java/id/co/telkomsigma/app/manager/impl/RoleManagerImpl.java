package id.co.telkomsigma.app.manager.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.co.telkomsigma.app.manager.RoleManager;
import id.co.telkomsigma.app.model.web.menu.Menu;
import id.co.telkomsigma.app.model.web.user.Role;
import id.co.telkomsigma.app.web.dao.MenuDao;
import id.co.telkomsigma.app.web.dao.RoleDao;
import id.co.telkomsigma.app.web.dao.UserDao;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

/**
 * Created by daniel on 4/16/15.
 */
@Service("roleManager")
public class RoleManagerImpl implements RoleManager {

    @Autowired
    private RoleDao roleDao;
    
    @Autowired
    private UserDao userDao;
    
    @Autowired
    private MenuDao menuDao;

    @Override
    public List<Role> getAll() {
        return roleDao.findAll();
    }


	@Override
	public Role getRoleById(Long roleId) {
		return roleDao.findByRoleId(roleId);
	}


	@Override
	public Page<Role> searchRoles(String authority,
			LinkedHashMap<String, String> orderMap, int offset, int limit, String orderBy, Direction sortBy) {
		if (authority == null) {
			authority = "";
        }
		authority = "%" + authority + "%";
        return roleDao.findByAuthorityLikeIgnoreCase(authority, new PageRequest((offset / limit), limit, sortBy, orderBy));
	}


	@Transactional
	@Override
	public Role insertRole(Role role) {
	    role = roleDao.save(role);
        return role;
	}


	@Transactional
	@Override
	public Role updateRole(Role role) {	
		Role roleFromDb = roleDao.findByRoleId(role.getRoleId());
		roleFromDb.setDescription(role.getDescription());
		roleFromDb.setUpdatedBy(role.getUpdatedBy());

		if(null!=role.getMenus()){
			roleFromDb.setMenus(null);
			roleFromDb.setMenus(role.getMenus());
		}
        roleDao.save(roleFromDb);
        return role;
	}


	@Override
	public Role getRoleByAuthority(String authority) {
		return roleDao.findByAuthority(authority);
	}	
}
