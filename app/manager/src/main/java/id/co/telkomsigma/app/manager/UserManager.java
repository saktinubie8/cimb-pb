package id.co.telkomsigma.app.manager;

import id.co.telkomsigma.app.exception.UserProfileException;
import id.co.telkomsigma.app.model.web.user.User;

import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort.Direction;


//import org.neo4j.cypher.internal.compiler.v2_1.ast.rewriters.isolateAggregation;

/**
 * Created by daniel on 3/31/15.
 */
public interface UserManager {
    /**
     * Gets single user by email
     *
     * @param email
     * @return user or null
     */
    User getUserByEmail(String email);

    /**
     * Gets single user by username
     *
     * @param username
     * @return user or null
     */
    User getUserByUsername(String username);
    
    User getUserByUserId(Long userId);

    /**
     * Saves new user (admin / CS)
     *
     * @param user
     * @return user or null
     */
    User insertUser(User user) throws UserProfileException;

    /**
     * Resets current password
     *
     * @param user
     * @return user or null
     */
    User resetPassword(User user);

    /**
     * Searches users by username or email as keyword, with/without given authority order by orderMap from offset limited by limit
     *
     * @param keyword
     * @param authority
     * @param orderMap
     * @param offset
     * @param limit
     * @return list of user found limited by limit from offset and total count of found user wrapped in UserListPojo object
     */
    Page<User> searchUsers(String keyword, String authority, LinkedHashMap<String, String> orderMap, int offset, int limit, String orderBy, Direction sortBy);


    /**
     * Updates a user
     *
     * @param user
     * @return user or null
     */
    User updateUser(User user);
    
	User unlockUser(User user);

	List<User> getAllLockedUser();	
	
	Integer failedLoginAttempt(Long userId,String raw,boolean accountNonLocked, Long count);
	
	User insertUserFromUpload(User user);
	
	User getUserByUsernameFromUpload(String username);
	
	User updateUserFromUpload(User user);

	User resetPasswordToDefault(User user);

	User enableUser(User user);

	User disableUser(User user);

	void deleteUser(User user);
	
	void clearCache();

	User updateProfileAdmin(User user);

	User lockUser(User user, Long lockDuration);
	
	boolean checkDefaultPassword(User user);
	
	Long checkExpiryDays(User user);
	
	boolean checkReusePassword(User user);
}
