package id.co.telkomsigma.app.service;

import id.co.telkomsigma.app.manager.ParamManager;
import id.co.telkomsigma.app.model.web.parameter.SystemParameter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Dzulfiqar on 11/12/15.
 */
@Service("parameterService")
public class ParameterService extends GenericService {
	@Autowired
    private ParamManager paramManager;
	/**
	 * Get all parameters
	 * 
	 * @return list of Locations object
	 */

	public List<SystemParameter> getParameter() {
		return paramManager.getAll();
	}
	

	public void updateParam(SystemParameter param, final String paramValue) throws Exception {
		paramManager.updateParam(param, paramValue);
	}


	public SystemParameter loadParamByParamId(final Long paramId) throws UsernameNotFoundException {
		return paramManager.getParamByParamId(paramId);
	}


	public String loadParamByParamName(final String paramName, final String defaultValue) {
		SystemParameter param = paramManager.getParamByParamName(paramName);
		if (param != null)
			return param.getParamValue();
		return  defaultValue;
	}
	
	public Page<SystemParameter> searchParam(String keyword, String paramGroup,
			LinkedHashMap<String, String> columnMap, int finalOffset,
			int finalLimit,String orderBy,String sortBy) {
		// TODO Auto-generated method stub
		if(orderBy == null || orderBy.equals(""))		
			orderBy = "paramName";
		
		Direction sortByDirection = Direction.DESC;
		if(sortBy.equals("") || sortBy.equals("asc")) 
		sortByDirection = Direction.ASC;
		
		return paramManager.searchParam(keyword, paramGroup, columnMap, finalOffset, finalLimit, orderBy, sortByDirection);
	}
	
	public SystemParameter getParamByParamName(String name){
		return paramManager.getParamByParamName(name);
	}
}