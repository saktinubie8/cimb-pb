package id.co.telkomsigma.app.manager.impl;

import id.co.telkomsigma.app.constant.WebGuiConstant;
import id.co.telkomsigma.app.engine.MailEngine;
import id.co.telkomsigma.app.exception.UserProfileException;
import id.co.telkomsigma.app.manager.UserManager;
import id.co.telkomsigma.app.model.web.parameter.SystemParameter;
import id.co.telkomsigma.app.model.web.user.Role;
import id.co.telkomsigma.app.model.web.user.User;
import id.co.telkomsigma.app.model.web.user.UsersPasswords;
import id.co.telkomsigma.app.web.dao.ParamDao;
import id.co.telkomsigma.app.web.dao.RoleDao;
import id.co.telkomsigma.app.web.dao.UserDao;
import id.co.telkomsigma.app.web.dao.UsersPasswordsDao;

import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * Created by daniel on 3/31/15.
 */
@Service("userManager")
@Primary
public class UserManagerImpl implements UserManager {
    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private UserDao userDao;

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private ParamDao paramDao;

    @Autowired
	private PasswordEncoder passwordEncoder;

    @Autowired
    private UsersPasswordsDao usersPasswordsDao;


    @Autowired
    private MailEngine mailEngine;

    @Autowired
    private Locale timezone;
    
	@PersistenceContext(unitName = "webEntityManager")
	@Qualifier(value = "webEntityManager")
	public EntityManager em;
	
	protected final Log log = LogFactory.getLog(getClass());

	//private final String MIN_HIST_PWD = "2";
	
	private final String MAX_HIST_PWD = "maxHistChangePassword";
	
	private final String DEFAULT_PWD = "DefaultPassword";
	
    @Override
    public User getUserByEmail(String email) {
        return userDao.findByEmailIgnoreCase(email.toLowerCase());
    }

    @Override
    public User getUserByUsername(String username) {
     	return userDao.findByUsername(username.toLowerCase());
    }
    
    @Override
    public User getUserByUserId(Long userId) {
    	User user = userDao.findByUserId(userId);
        return user;
    }



    @Override
    public User insertUser(User user) throws UserProfileException {
        user.setUsername(user.getUsername().toUpperCase());
        user.setCreatedDate(new Date());
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setEnabled(true);
        user.setAccountNonLocked(true);
        user.setAccountNonExpired(true);
        user.setCredentialsNonExpired(true);
        user.setLastUpdatePassword(new Date());
        user.setChangePasswordCount(0);

        // do search user by username / email address
        User temp0 = userDao.findByUsername(user.getUsername());
        User temp1 = userDao.findByEmailIgnoreCase(user.getEmail());
        if (temp0 != null && temp1 != null) {
        	System.out.println("temp0 != null && temp1 != null ");
            throw new UserProfileException(true, true);
            
        } else if (temp0 != null) {
        	System.out.println("temp0 != null ");
            throw new UserProfileException(true, false);
        } else if (temp1 != null) {
        	System.out.println("ttemp1 != null ");
            throw new UserProfileException(false, true);
        }
        
        
        user = userDao.save(user);
        
        /*
        //save di userpassword
        UsersPasswords uspw = new UsersPasswords();
        uspw.setUserId(user);
        uspw.setPassword(passwordEncoder.encode(user.getPassword()));
        uspw.setCreatedDate(new Date());
        usersPasswordsDao.save(uspw);
        */
        
        return user;
    }


    @Override
    public User resetPassword(User user) {
        User result = userDao.findByUsername(user.getUsername());
        SystemParameter param = paramDao.findByParamName(DEFAULT_PWD);
        
        if (result != null) {
            result.setPassword(passwordEncoder.encode(param.getParamValue()));
            result.setUpdatedBy(user.getUserId());
            result.setUpdatedDate(new Timestamp(System.currentTimeMillis()));
            result.setVerificationCode("");
            userDao.save(result);
        }
        return result;
    }

    @Override
    public Page<User> searchUsers(String keyword, String authority, LinkedHashMap<String, String> orderMap, int offset, int limit, String orderBy, Direction sortBy) {
        if (keyword == null) {
            keyword = "";
        }
        keyword = "%" + keyword + "%";
            PageRequest pageRequest = new PageRequest((offset / limit), limit, sortBy, orderBy);
            if (authority != null && !authority.equalsIgnoreCase("")) {
                return userDao.getUsers(keyword, authority, pageRequest);
            } else {
                return userDao.findByUsernameLikeOrEmailLikeOrNameLikeAllIgnoreCase(keyword,keyword,keyword, pageRequest);
            }
    }

    @Override
    public User updateUser(User result) {
    	User fromDb = userDao.findByUserId(result.getUserId());
        if (result.getPassword() != null && !result.getPassword().equalsIgnoreCase("")) {
        	if(!fromDb.getPassword().equals(result.getPassword()))
        		fromDb.setPassword(passwordEncoder.encode(result.getPassword()));
        		fromDb.setLastUpdatePassword(new Date());
        		
        		
        		int countExisting = fromDb.getChangePasswordCount();
        		fromDb.setChangePasswordCount(countExisting+1);
        		
			
        	  System.out.println("Update user password from update user: " + result.getPassword());
			  //UsersPasswords uspw = new UsersPasswords(); uspw.setUserId(fromDb);
			  //uspw.setPassword(passwordEncoder.encode(result.getPassword()));
			  //uspw.setCreatedDate(new Date()); 
			  //usersPasswordsDao.save(uspw);
			 
        }
        fromDb.setEmail(result.getEmail());
        fromDb.setName(result.getName());
        fromDb.setAddress(result.getAddress());
        fromDb.setPhone(result.getPhone());
        fromDb.setCity(result.getCity());
        fromDb.setPhoneMobile(result.getPhoneMobile());
        fromDb.setRoles(result.getRoles());
        fromDb.setRaw(result.getRaw());
        fromDb.setExtension(result.getExtension());
        fromDb.setFax(result.getFax());
        fromDb.setJobTitle(result.getJobTitle());
        fromDb.setLevel(result.getLevel());
        userDao.save(fromDb);
        

        
        return result;
    }
    
    @Transactional
    @Override
    public User updateProfileAdmin(User user) {
        User result = userDao.findByUsername(user.getUsername());
        UsersPasswords uspw = new UsersPasswords();
        if (result != null) {
        	if(user.getMatchDefault()){
        		if (user.getPassword() != null && !user.getPassword().equalsIgnoreCase("")) {
                	if(!result.getPassword().equals(user.getPassword()))
                		result.setPassword(passwordEncoder.encode(user.getPassword()));
                		result.setLastUpdatePassword(new Date());
                		result.setChangePasswordCount(result.getChangePasswordCount()+1);
                        uspw.setUserId(result);
                        uspw.setPassword(result.getPassword());
                        uspw.setCreatedDate(new Date());
                        log.debug("uspw default " + result );
                        System.out.println("Update user password from updateProfileAdmin get match default: " + result.getPassword());
                        usersPasswordsDao.save(uspw);
                }
        		userDao.save(result);
        	}
        	else if(user.getExpiryDays()){
        		if (user.getPassword() != null && !user.getPassword().equalsIgnoreCase("")) {
                	if(!result.getPassword().equals(user.getPassword()))
                		result.setPassword(passwordEncoder.encode(user.getPassword()));
                		result.setLastUpdatePassword(new Date());
					    result.setChangePasswordCount(result.getChangePasswordCount()+1);
                		uspw.setUserId(result);
                		uspw.setPassword(result.getPassword());
                        uspw.setCreatedDate(new Date());
                        
                        log.debug("uspw expiry" + result );
                        System.out.println("Update user password from updateProfileAdmin expiry days: " + result.getPassword());
                        usersPasswordsDao.save(uspw);
                }
        		userDao.save(result);
        	}
        	else {
        		if (user.getPassword() != null && !user.getPassword().equalsIgnoreCase("")) {
                	if(!result.getPassword().equals(user.getPassword()))
                		result.setPassword(passwordEncoder.encode(user.getPassword()));
                		result.setLastUpdatePassword(new Date());
					    result.setChangePasswordCount(result.getChangePasswordCount()+1);
                		uspw.setUserId(result);
                		uspw.setPassword(result.getPassword());
                        uspw.setCreatedDate(new Date());
                        
                        log.debug("uspw  " + result );
                        System.out.println("Update user password from updateProfileAdmin else : " + result.getPassword());
                        usersPasswordsDao.save(uspw);
                }
                result.setName(user.getName());
                result.setAddress(user.getAddress());
                result.setPhoneMobile(user.getPhoneMobile());
                result.setPhone(user.getPhone());
                result.setUpdatedBy(result.getUserId());
                result.setUpdatedDate(new Timestamp(System.currentTimeMillis()));
                result.setExtension(user.getExtension());
                result.setFax(user.getFax());
                result.setJobTitle(user.getJobTitle());
                userDao.save(result);
        	}
        }
        return result;
    }


	@Override
	public User lockUser(User user, Long lockDuration) {
		User fromDb = userDao.findByUserId(user.getUserId());
		user.setAccountNonLocked(false);
		
		fromDb.setAccountNonLocked(false);
		Date lockUntil = DateUtils.addMinutes(new Date(), lockDuration.intValue());     			
		fromDb.setCustomProp(WebGuiConstant.PARAMETER_USER_CURRENT_LOCK_UNTIL, ""+lockUntil.getTime());
		userDao.save(fromDb);
		return user;
	}

	@Override
	public User unlockUser(User user) {
		User fromDb = userDao.findByUserId(user.getUserId());
		fromDb.setAccountNonLocked(true);
		fromDb.setFailedLogin(0L);
		userDao.save(fromDb);
		return user;
	}

	@Override
	public List<User> getAllLockedUser() {
		List<User> lockedUserList = userDao.findByAccountNonLocked(false);
		return lockedUserList;
	}

	@Override
	public Integer failedLoginAttempt(Long userId, String raw,boolean accountNonLocked, Long count) {
		// TODO Auto-generated method stub
		return userDao.failedLoginAttempt(userId, raw,accountNonLocked, count);
	}
	
	
    @Override
    public User insertUserFromUpload(User user) {
        user.setUsername(user.getUsername().toLowerCase());
        user.setCreatedDate(new Timestamp(System.currentTimeMillis()));
        user.setUpdatedDate(new Timestamp(System.currentTimeMillis()));
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setEnabled(true);
        user.setAccountNonLocked(true);
        user.setAccountNonExpired(true);
        user.setCredentialsNonExpired(true);
        user.setLastUpdatePassword(new Date());

        user = userDao.save(user);

        return user;
    }

	@Override
	public User getUserByUsernameFromUpload(String username) {
		User user = new User();
		user = userDao.findByUsername(username.toLowerCase());
		
		if(user!=null){
			user.setRoles(null);
		}
		return user;
	}

	@Override
	public User updateUserFromUpload(User user) {
        if (user.getPassword() != null && !user.getPassword().equalsIgnoreCase("")) {
        	user.setPassword(passwordEncoder.encode(user.getPassword()));
        	user.setLastUpdatePassword(new Date());
        }
        
        user.setUpdatedDate(new Timestamp(System.currentTimeMillis()));
        userDao.save(user);
        return user;
	}

	@Override
	public User resetPasswordToDefault(User user) {
		User fromDb = userDao.findByUserId(user.getUserId());
		fromDb.setPassword(passwordEncoder.encode("000000"));
		fromDb.setLastUpdatePassword(new Date());
		userDao.save(fromDb);
		return user;
	}

	@Override
	public User enableUser(User user) {
		User fromDb = userDao.findByUserId(user.getUserId());
		fromDb.setEnabled(true);
		userDao.save(fromDb);
		return user;
	}

	@Override
	public User disableUser(User user) {
		User fromDb = userDao.findByUserId(user.getUserId());
		fromDb.setEnabled(false);
		userDao.save(fromDb);
		return user;
	}

	@Override
	public void deleteUser(User user) {
		List<UsersPasswords> listChild = new ArrayList<UsersPasswords>();
		listChild = usersPasswordsDao.findByUserId(user);
		
		for(UsersPasswords userPwd : listChild){
			usersPasswordsDao.delete(userPwd);
		}
		
		userDao.delete(user);
	}	
	
	@Override
	public void clearCache(){
		
	}
	
	@Override
	public boolean checkDefaultPassword(User user) {
		SystemParameter param = paramDao.findByParamName(DEFAULT_PWD);
		
		boolean match = true ;
		if(passwordEncoder.matches(param.getParamValue(), user.getPassword()))
			match = true; 
		else 
			match = false;
		return match;
	}
	
	@Override
	public Long checkExpiryDays(User user) {
		Date now = new Date();
		Date lastUpdate = user.getLastUpdatePassword();
		if(lastUpdate == null){
			return 0L;
		}else{
			Long diff = TimeUnit.DAYS.convert(now.getTime() - lastUpdate.getTime(), TimeUnit.MILLISECONDS);
			return diff;
		}
	}
	
	@Override
	public boolean checkReusePassword(User user) {
		boolean find = false ;
		

		SystemParameter param = new SystemParameter();
		param = paramDao.findByParamName(MAX_HIST_PWD);
		Long record = Long.parseLong(param.getParamValue());
		Long userId = user.getUserId();
		
		List<UsersPasswords> usersPwd = new ArrayList<UsersPasswords>();
		
		Query q = em.createNativeQuery("SELECT up.* from m_users_passwords up join m_users u on up.user_id = u.id where u.id = ?1 order by up.datetime desc", UsersPasswords.class);
		q.setParameter(1, userId);
		q.setFirstResult(0);
		q.setMaxResults(record.intValue());
		usersPwd =  q.getResultList();
		
		
		log.debug("===== size usersPwd : " + usersPwd.size());
		
		for (UsersPasswords userPwd : usersPwd) {
			if(passwordEncoder.matches(user.getPassword(), userPwd.getPassword())){
				find = true;
				System.out.println("find true pwd " + user.getPassword());
				break;
			}
			System.out.println("pwd raw new " + user.getPassword());
			System.out.println("pwd encode inputan " + passwordEncoder.encode(user.getPassword()));
			System.out.println("pwd encode " + userPwd.getPassword());
			System.out.println("find " + find);
		}
		System.out.println("find final " + find);
		return find;
	}
	
}
