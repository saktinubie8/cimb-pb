package id.co.telkomsigma.app.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.app.manager.StoreAndForwardManager;
import id.co.telkomsigma.app.model.web.logs.StoreAndForward;


@Service("storeAndForwardService")
public class StoreAndForwardService extends GenericService{

	@Autowired
	private StoreAndForwardManager snfManager;
	
	
	public List<StoreAndForward> getStoreAndForward() { 
		return  snfManager.getStoreAndForward(); 
	}
	
	public Page<StoreAndForward> searchStoreAndForward(Map<String,Object> parameterList,
			 int finalOffset, int finalLimit, String orderBy, String sortBy) {
		if (orderBy == null || orderBy.equals(""))
			orderBy = "auditId";
		Sort.Direction sortByDirection = Sort.Direction.DESC;
		if (sortBy != null && sortBy.equals("asc"))
			sortByDirection = Sort.Direction.ASC;
		return
			 snfManager.searchStoreAndForward(parameterList, finalOffset, finalLimit, orderBy, sortByDirection);
		}
	
	
	public List<StoreAndForward> getStoreAndForward(int first, int max) {
		return snfManager.getStoreAndForward(first, max);
	}
	
	public void deleteSnf(StoreAndForward snf) {
		System.out.println(" service do delete snf : " + snf.getId().toString());
		snfManager.deleteSnf(snf);
	}
	
	public StoreAndForward findSnfById(String id) {
		return snfManager.findById(id);
	}

}