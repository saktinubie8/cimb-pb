package id.co.telkomsigma.app.scheduler;

import java.util.Date;
import java.util.List;

import id.co.telkomsigma.app.constant.WebGuiConstant;
import id.co.telkomsigma.app.manager.UserManager;
import id.co.telkomsigma.app.model.web.user.User;
import id.co.telkomsigma.app.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class OpenLockScheduler {

	
    @Autowired
    private UserManager userManager;
	
	@Scheduled(fixedRate=60000)
	public void checkUnlock()
	{
		List<User> listLockedUser = userManager.getAllLockedUser();
		if(listLockedUser!=null)
			if(listLockedUser.size()>0)
			{
				for(User user:listLockedUser)
				{
					String lockUntil = user.getCustomProp(WebGuiConstant.PARAMETER_USER_CURRENT_LOCK_UNTIL);
					if(lockUntil!=null)
					{
						Long unlockTime = Long.parseLong(lockUntil);
						Long currentTime = new Date().getTime();
						if(currentTime>unlockTime)
						{
							user.setAccountNonLocked(true);
							user.setCustomProp(WebGuiConstant.PARAMETER_USER_CURRENT_FAILED_ATTEMPT, "0");
							userManager.unlockUser(user);
						}
					}
				}
			}
	}
}
