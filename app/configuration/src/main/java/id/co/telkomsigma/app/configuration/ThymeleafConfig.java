package id.co.telkomsigma.app.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.thymeleaf.extras.springsecurity4.dialect.SpringSecurityDialect;

/**
 * Created by daniel on 4/9/15.
 */
@Configuration
public class ThymeleafConfig {

    @Bean(name = "springSecurityDialect")
    public SpringSecurityDialect getSpringSecurityDialect() {
        return new SpringSecurityDialect();
    }    
}