package id.co.telkomsigma.app.configuration;

import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import id.co.telkomsigma.app.authhandler.AuthenticationFailureHandler;
import id.co.telkomsigma.app.authhandler.AuthenticationSuccessHandler;
import id.co.telkomsigma.app.constant.WebGuiConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

/**
 * Created by daniel on 3/30/15.
 */
@Configuration
@EnableWebMvcSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

    private PasswordEncoder passwordEncoder;
	
	@Autowired
	private UserDetailsService userService;

	@Autowired
	private AuthenticationSuccessHandler authenticationSuccessHandler;

	@Autowired
	private AuthenticationFailureHandler authenticationFailureHandler;

	@Autowired
	private SessionRegistry sessionRegistry;

	@Value("${ldap.urls}")
	private String ldapUrls;

	@Value("${ldap.base.dn}")
	private String ldapBaseDn;

	@Value("${ldap.username}")
	private String ldapSecurityPrincipal;

	@Value("${ldap.password}")
	private String ldapPrincipalPassword;

	@Value("${ldap.user.dn.pattern}")
	private String ldapUserDnPattern;

	@Value("${ldap.enabled}")
	private String ldapEnabled;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.authorizeRequests()
				.antMatchers("/", "/index.html", WebGuiConstant.CAPTCHA_PATH_SECURITY_MAPPING, "/css/**", "/fonts/**",
						"/img/**", "/js/**", "/apk/**", "/error**", "/error/**", "/webservice/**")
						.permitAll()
				.antMatchers("/login**","/webservice/**")
					.anonymous()
				.antMatchers("/home/**")
					.fullyAuthenticated()
				.anyRequest().fullyAuthenticated()
			.and()
			.formLogin().loginPage("/")
				// .successHandler(new AuthenticationSuccessHandler())
				.successHandler(authenticationSuccessHandler).failureHandler(authenticationFailureHandler)
				// .defaultSuccessUrl("/app/home")
				// .failureUrl("/login?error")
				.permitAll().and().logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
				.logoutSuccessUrl("/index.html").invalidateHttpSession(true).permitAll()
			.and()
			.csrf().requireCsrfProtectionMatcher(new RequestMatcher() {
				private final Pattern allowedMethods = Pattern.compile("^(GET|HEAD|TRACE|OPTIONS)$");
	
					@Override
					public boolean matches(HttpServletRequest req) {
						if (allowedMethods.matcher(req.getMethod()).matches())
							return false;
						if (req.getServletPath().startsWith("/webservice/") || req.getPathInfo().startsWith("/webservice/"))
						//if (req.getServletPath().startsWith("/webservice/"))
							return false;
						return true;
					}
			})
			.and()
			.sessionManagement()
			    .maximumSessions(1) // How many session the same user can have? This can be any number you pick
			    .expiredUrl("/login?error=expired")
			    .sessionRegistry(sessionRegistry);
	}

	@Bean(name = "passwordEncoder")
	public PasswordEncoder getPasswordEncoder() {
		if (passwordEncoder == null) {
			passwordEncoder = new BCryptPasswordEncoder();
		}
		return passwordEncoder;
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		if(Boolean.parseBoolean(ldapEnabled)){
			auth
					.ldapAuthentication()
					.contextSource()
					.url(ldapUrls + ldapBaseDn)
					.managerDn(ldapSecurityPrincipal)
					.managerPassword(ldapPrincipalPassword)
					.and()
					.userDnPatterns(ldapUserDnPattern);
		}else{
			auth.userDetailsService(userService).passwordEncoder(getPasswordEncoder());
		}
	}
	
	@Bean
	public SessionRegistry sessionRegistry() {
		return new SessionRegistryImpl();
	}
}