package id.co.telkomsigma.app.configuration;

import java.util.Properties;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.code.kaptcha.impl.DefaultKaptcha;
import com.google.code.kaptcha.util.Config;

import id.co.telkomsigma.app.model.web.GenericModel;

/**
 * Created by daniel on 4/15/15.
 */
@Configuration
public class KaptchaConfig {

    @Bean(name = "defaultCaptchaProducer")
    public DefaultKaptcha defaultCaptchaProducer() {
        Properties properties = new Properties();
        properties.setProperty("kaptcha.image.width", "100");
        properties.setProperty("kaptcha.image.height", "40");
        properties.setProperty("kaptcha.textproducer.char.string", "123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ");
        properties.setProperty("kaptcha.textproducer.char.length", "" + GenericModel.MAX_LENGTH_DEFAULT_CAPTCHA);
        properties.setProperty("kaptcha.textproducer.font.size", "36");

        DefaultKaptcha defaultKaptcha = new DefaultKaptcha();
        defaultKaptcha.setConfig(new Config(properties));
        return defaultKaptcha;
    }
}
