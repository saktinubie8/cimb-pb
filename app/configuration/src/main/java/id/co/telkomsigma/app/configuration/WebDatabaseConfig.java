package id.co.telkomsigma.app.configuration;

import java.util.HashMap;

import javax.sql.DataSource;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
@EnableConfigurationProperties({ DataSourceProperties.class })
@EnableJpaRepositories(
    basePackages = "id.co.telkomsigma.app.web.dao", 
    entityManagerFactoryRef = "webEntityManager", 
    transactionManagerRef = "webTransactionManager"
)
public class WebDatabaseConfig {
    @Autowired
    private Environment env;
    
    
   @Bean
   @Primary
   public LocalContainerEntityManagerFactoryBean webEntityManager() {
       LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
       em.setDataSource(webDataSource());
       em.setPackagesToScan(new String[] { "id.co.telkomsigma.app.model.web" });

       HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
       em.setJpaVendorAdapter(vendorAdapter);
       HashMap<String, Object> properties = new HashMap<String, Object>();
       properties.put("hibernate.hbm2ddl.auto", env.getProperty("spring.jpa.hibernate.ddl-auto"));
       properties.put("hibernate.dialect", env.getProperty("spring.jpa.properties.hibernate.dialect"));
       properties.put("hibernate.show_sql", env.getProperty("spring.jpa.properties.hibernate.show_sql"));
       
       properties.put("hibernate.c3p0.max_size", env.getProperty("spring.jpa.properties.hibernate.c3p0.max_size"));
       properties.put("hibernate.c3p0.min_size", env.getProperty("spring.jpa.properties.hibernate.c3p0.min_size"));
       properties.put("hibernate.c3p0.timeouts", env.getProperty("pring.jpa.properties.hibernate.c3p0.timeouts"));
       properties.put("hibernate.c3p0.max_statements", env.getProperty("spring.jpa.properties.hibernate.c3p0.max_statements"));
       properties.put("hibernate.c3p0.idle_test_period", env.getProperty("spring.jpa.properties.hibernate.c3p0.idle_test_period"));
       properties.put("hibernate.c3p0.acquire_increment", env.getProperty("spring.jpa.properties.hibernate.c3p0.acquire_increment"));
       properties.put("hibernate.c3p0.validate", env.getProperty("spring.jpa.properties.hibernate.c3p0.validate"));
//       properties.put("hibernate.connection.provider_class", env.getProperty("spring.jpa.properties.hibernate.connection.provider_class"));      
       
       
       properties.put("hibernate.connection.driver_class", env.getProperty("spring.datasource.driverClassName"));
       properties.put("hibernate.connection.url", env.getProperty("spring.datasource.url"));
       properties.put("hibernate.connection.username", env.getProperty("spring.datasource.username"));
       properties.put("hibernate.connection.password", env.getProperty("spring.datasource.password"));
       
       em.setJpaPropertyMap(properties);

       return em;
   }

   @Primary
   @Bean
   @ConfigurationProperties(prefix="spring.datasource")
   public DataSource webDataSource() {
	   return DataSourceBuilder.create().build();
   }
   
   @Autowired
   protected BeanFactory beanFactory;

   @Primary
   @Bean
   public PlatformTransactionManager webTransactionManager() {
       JpaTransactionManager transactionManager = new JpaTransactionManager();
       transactionManager.setEntityManagerFactory(webEntityManager().getObject());
       transactionManager.setBeanFactory(beanFactory);
       transactionManager.setNestedTransactionAllowed(true);
       transactionManager.setRollbackOnCommitFailure(true);
       return transactionManager;
   }
}
