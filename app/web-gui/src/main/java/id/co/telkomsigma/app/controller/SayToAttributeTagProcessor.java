package id.co.telkomsigma.app.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.thymeleaf.context.ITemplateContext;
import org.thymeleaf.engine.AttributeName;
import org.thymeleaf.engine.TemplateData;
import org.thymeleaf.model.AttributeValueQuotes;
import org.thymeleaf.model.IModel;
import org.thymeleaf.model.IModelFactory;
import org.thymeleaf.model.IOpenElementTag;
import org.thymeleaf.model.IProcessableElementTag;
import org.thymeleaf.processor.element.AbstractAttributeTagProcessor;
import org.thymeleaf.processor.element.AbstractElementTagProcessor;
import org.thymeleaf.processor.element.IElementTagStructureHandler;
import org.thymeleaf.templatemode.TemplateMode;
import org.unbescape.html.HtmlEscape;

@Component
public class SayToAttributeTagProcessor extends AbstractElementTagProcessor {

	public SayToAttributeTagProcessor()
	{
		super(TemplateMode.HTML, "hello", "sayTo", true, null, false, 0);
	}

	public SayToAttributeTagProcessor(TemplateMode templateMode, String dialectPrefix, String elementName,
			boolean prefixElementName, String attributeName, boolean prefixAttributeName, int precedence) {
		super(templateMode, dialectPrefix, elementName, prefixElementName, attributeName, prefixAttributeName, precedence);
		// TODO Auto-generated constructor stub
	}

	
	@Override
	protected void doProcess(ITemplateContext paramITemplateContext, IProcessableElementTag paramIProcessableElementTag,
			IElementTagStructureHandler paramIElementTagStructureHandler) {
		IModelFactory modelFactory = paramITemplateContext.getModelFactory();
		IModel model = modelFactory.createModel();
		IOpenElementTag jav = modelFactory.createOpenElementTag("script","type","text/javascript");
		model.add(jav);
		model.add(modelFactory.createText("$(document).ready(function(){ alert('yoyoy'); });"));
		model.add(modelFactory.createCloseElementTag("script"));
		
		String message = paramITemplateContext.getMessage(this.getClass(), "label.add.lahan.kodePetakAreal", new Object[]{}, true);
		
//		IModel model = modelFactory.parse(paramITemplateContext.getTemplateData(), "<div class='headlines' th:id='hihi'>Some headlines</div>");
		Map<String,String> attributes = new HashMap<String, String>();
		attributes.put("id", "huhu");
		attributes.put("class", "headlines");
		IOpenElementTag mainDiv = modelFactory.createOpenElementTag("div", attributes, AttributeValueQuotes.SINGLE, false);
////		mainDiv = modelFactory.setAttribute(mainDiv, "id", "huhu");
		model.add(mainDiv);
		model.add(modelFactory.createText(HtmlEscape.escapeHtml5("HREREORER:"+paramIProcessableElementTag.getAttribute("order").getValue()+message)));
		model.add(modelFactory.createCloseElementTag("div"));
//		TemplateData data = new 		
		
		paramIElementTagStructureHandler.replaceWith(model, false);
	}

}
