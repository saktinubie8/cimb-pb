package id.co.telkomsigma.app.controller;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.thymeleaf.dialect.AbstractProcessorDialect;
import org.thymeleaf.processor.IProcessor;

@Configuration
public class HelloDialect extends AbstractProcessorDialect {

	@Autowired
	private IProcessor sayToAttributeTagProcessor;
	
	public HelloDialect()
	{
	       super(
	                "Hello Dialect",    // Dialect name
	                "hello",            // Dialect prefix (hello:*)
	                1000);  
	}
	
	public HelloDialect(String name, String prefix, int processorPrecedence) {
		super("Hello Dialect", "hello", processorPrecedence);
	}

	@Override
	public Set<IProcessor> getProcessors(String arg0) {
        final Set<IProcessor> processors = new HashSet<IProcessor>();
        processors.add(sayToAttributeTagProcessor);
        return processors;
	}

}
