package id.co.telkomsigma.app;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;


@EnableAutoConfiguration
@SpringBootApplication(scanBasePackages={"id.co.telkomsigma"})
//@SpringBootApplication(exclude = {JmxAutoConfiguration.class})
@EnableCaching
@EnableScheduling
@ComponentScan
@Configuration
@EnableTransactionManagement
@EntityScan
@EnableEncryptableProperties
public class Application extends SpringBootServletInitializer {
    private final Log log = LogFactory.getLog(this.getClass());
    

    public static void main(String[] args) {
    	System.setProperty("jasypt.encryptor.password", "cimbPB123"); 
        SpringApplication microService = new SpringApplication(Application.class);
        microService.run(args);
      
    }
}